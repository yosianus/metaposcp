

$(document).ready(function() {

	// start of carousel section
	$('.product-carousel').slick({
		dots: true,
		arrows: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		infinite: true,
		adaptiveHeight: true,
		responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 540,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		]
	});
	$('.testi-carousel').slick({
		dots: true,
		arrows: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		infinite: true,
		adaptiveHeight: true,
		responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		      }
		    },
		    {
		      breakpoint: 540,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		]
	});
	$('.news-carousel').slick({
		dots: true,
		arrows: false,
		slidesToShow: 2,
		slidesToScroll: 1,
		infinite: true,
		adaptiveHeight: true,
		responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		]
	});
	// end of carousel section

	// product slider
	$('.slider-for-product').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.slider-product-nav',
		adaptiveHeight: true
	});
	$('.slider-product-nav').slick({
		slidesToShow:3,
		slidesToScroll: 1,
		asNavFor: '.slider-for-product',
		dots: false,
		arrows: false,
		focusOnSelect: true,
		vertical: true
	});

	$(document).on('click','#toggle-mmenu', function(e){
		$(".mmenu-wrap").addClass("active");
		e.preventDefault();
	});
	$(document).on('click','.close-mmenu-toggle', function(e){
		$(".mmenu-wrap").removeClass("active");
		e.preventDefault();
	});

	$(document).on('click','.search-toggle', function(e){
		$(".search-area-wrap").addClass("active");
		e.preventDefault();
	});
	$(document).on('click','.close-search-toggle', function(e){
		$(".search-area-wrap").removeClass("active");
		e.preventDefault();
	});

	$(document).on('click','.mmenu-nav .has-sub > a', function(e){
		if( $(this).hasClass("active") ) {
			$(this).removeClass("active");
			$(this).next(".sub-nav").slideUp("fast");
		} else {
			$(this).addClass("active");
			$(this).next(".sub-nav").slideDown("fast");
		}

		e.preventDefault();
	});

	$(document).on('click','.lang-toggle', function(e){
		if( $(this).hasClass("active") ) {
			$(this).removeClass("active");
		} else {
			$(this).addClass("active");
		}

		e.preventDefault();
	});

	// GENERAL PAGE SIDEBAR MENU TOGGLE
	// Important HACK for iOS 8 and above so the on click function works on non-link/button
	if( /iPhone|iPad|iPod|Opera Mini/i.test(navigator.userAgent) ) {
		// run your code here
		$('.ngc-pages-aside').css('cursor','pointer');
	}
	$(document).on('click','.ngc-pages-aside', function(e){
		if($(".ngc-pages-aside").hasClass("opened")) {
		  // Check if click was triggered on or within .aside-content
		  if( $(e.target).closest(".aside-content").length > 0 ) {

		  }
		  // Otherwise
		  // trigger your click function
		  else {
		    $(".ngc-pages-aside").removeClass("opened");
		  }
		} else {

		}
		// jangan dikasih e.preventdefault supaya link bisa tetep jalan
	});
	$(document).on('click','.close-pages-menu', function(e){
		$(".ngc-pages-aside").removeClass("opened");
		e.preventDefault();
	});
	$(document).on('click','.pages-menu-toggle', function(e){
		$(".ngc-pages-aside").addClass("opened");
		e.preventDefault();
	});

	// toggle accordion
	$(".fc-toggle.opened + .fc-content").show();
	$(document).on('click','.fc-toggle', function(e){
		if( $(this).hasClass("opened") ) {
			$(this).removeClass("opened");
			$(this).next(".fc-content").slideUp("fast");
		} else {
			$(this).addClass("opened");
			$(this).next(".fc-content").slideDown("fast");
		}
		e.preventDefault();
	});
});

$(window).load(function() {

	// when page has been loaded

});
