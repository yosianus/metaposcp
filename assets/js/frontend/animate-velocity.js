$(document).ready(function() { // execute when window open
  var $fadeIn = $(".fadeIn"),
      $slideRightIn = $(".slideRightIn"),
      $slideLeftIn = $(".slideLeftIn"),
      $slideUpIn = $(".slideUpIn"),
      $slideDownIn = $(".slideDownIn"),
      $fadeInFlex = $(".fadeInFlex"),
      $slideRightInFlex = $(".slideRightInFlex"),
      $slideLeftInFlex = $(".slideLeftInFlex"),
      $slideUpInFlex = $(".slideUpInFlex"),
      $slideDownInFlex = $(".slideDownInFlex");

  $fadeIn.one('inview', function(event, isInView) {
    if (isInView) {
      // element is now visible in the viewport
      $(this).velocity("transition.fadeIn", 1000);
    }
  });

  $slideRightIn.one('inview', function(event, isInView) {
    if (isInView) {
      // element is now visible in the viewport
      $(this).velocity("transition.slideRightIn", 1000);
    }
  });

  $slideLeftIn.one('inview', function(event, isInView) {
    if (isInView) {
      // element is now visible in the viewport
      $(this).velocity("transition.slideLeftIn", 1000);
    }
  });

  $slideUpIn.one('inview', function(event, isInView) {
    if (isInView) {
      // element is now visible in the viewport
      $(this).velocity("transition.slideUpIn", 1000);
    }
  });

  $slideDownIn.one('inview', function(event, isInView) {
    if (isInView) {
      // element is now visible in the viewport
      $(this).velocity("transition.slideDownIn", 1000);
    }
  });

  ////////// for flex elements

  $fadeInFlex.one('inview', function(event, isInView) {
    if (isInView) {
      // element is now visible in the viewport
      $(this).velocity("transition.fadeIn", {display: "flex"}, 1000);
    }
  });

  $slideRightInFlex.one('inview', function(event, isInView) {
    if (isInView) {
      // element is now visible in the viewport
      $(this).velocity("transition.slideRightIn", {display: "flex"}, 1000);
    }
  });

  $slideLeftInFlex.one('inview', function(event, isInView) {
    if (isInView) {
      // element is now visible in the viewport
      $(this).velocity("transition.slideLeftIn", {display: "flex"}, 1000);
    }
  });

  $slideUpInFlex.one('inview', function(event, isInView) {
    if (isInView) {
      // element is now visible in the viewport
      $(this).velocity("transition.slideUpIn", {display: "flex"}, 1000);
    }
  });

  $slideDownInFlex.one('inview', function(event, isInView) {
    if (isInView) {
      // element is now visible in the viewport
      $(this).velocity("transition.slideDownIn", {display: "flex"}, 1000);
    }
  });

});
