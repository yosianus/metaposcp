function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if( charCode > 31 && (charCode < 48 || charCode > 57) && charCode!=46 ) {
        return false;
    }
    return true;
}

function phoneNumber(objek, separator) {
	var a = objek.value;
	var b = a.replace(/[^\d]/g,"");
	var	tmpVal = "";
	var panjang = b.length; var j = 0;

	for (i = panjang; i > 0; i--) {
		j = j + 1;
		if(i==1){
			tmpVal = "" + b.substr(i-1,1) + tmpVal;
		}else if(i==3 && panjang > 3){
			tmpVal = b.substr(i-1,1) + " - " +tmpVal;
		}else if(i==6 && panjang > 6){
			tmpVal = b.substr(i-1,1) + "-" +tmpVal;
		}else{
			tmpVal = b.substr(i-1,1) + tmpVal;
		}
	}
	objek.value = tmpVal;
}

$(document).ready(function() {

	jQuery.validator.addMethod("abcval", function(value, element) {
		//return this.optional(element) || /^\w[\w\d\s]*$/.test(value);
		return this.optional(element) || /^[a-z A-Z.-]+$/i.test(value);
	}, "Letters, numbers or underscores only");

	jQuery.validator.addMethod("addressval", function(value, element) {
		//return this.optional(element) || /^\w[\w\d\s]*$/.test(value);
		return this.optional(element) || /^[0-9:a-z+/.,-_ \n-!]+$/i.test(value);
	}, "Letters, numbers or underscores only");

	jQuery.validator.addMethod("phoneFormat", function(value, element) {
		//return this.optional(element) || /^\w[\w\d\s]*$/.test(value);
		return this.optional(element) || /^[0-9() +-]+$/i.test(value);
	}, "Letters, numbers or underscores only");

	$('form#contact_form_uiddata').validate({
	  rules: {
			name:{
				 required: true
			},
			email:{
				required: true,
				email:true
			},
			phone:{
				required: true,
				phoneFormat: true,
				minlength: 10
			},
			subject_inquiry:{
				required: true
			},
			pesan_member:{
				required: true
			}

	  },
	  messages: {

			name:{
				 required:"* This field can't be empty."
			},
			email:{
				required:"* This field can't be empty.",
				email:"* Please enter valid email address"
			},
			phone:{
				required:"* This field can't be empty.",
				phoneFormat:"* Please enter in numerical value only.",
				minlength: "* Please enter minimum length 10 character."
			},
			subject_inquiry:{
				required:"* This field can't be empty."
			},
			pesan_member:{
				required:"* This field can't be empty."
			}


	  },
	  errorPlacement: function(error, element){

		if (element.is(".subject_inquiry_input")) {
			$(".subject_inquiry_msg").html(error);

		}else{
			 error.insertAfter(element);
		}

	  },
	   submitHandler:function(form){
			form.submit();
	   }
	});



	$('form#newsletter_form').validate({
	  rules: {
			email:{
				 required: true,
				 email: true
			}

	  },
	  messages: {

			email:{
				 required:"* This field can't be empty.",
				 email: "* Please check your email invalid."
			}


	  }
	});


});
