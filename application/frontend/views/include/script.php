<!-- GMAPS -->
<script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyDD1AYXApRdEgfw0hmALurpm6m9Bg2Fkro"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/gmaps.min.js"></script>


<!-- FONTAWESOME -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/fontawesome-all.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/fontawesome.min.js"></script>

<!-- RECAPTCHA -->
<script src='https://www.google.com/recaptcha/api.js'></script>

<!-- JQUERY -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/jquery-3.2.1.min.js"></script>

<!-- JQUERY UI -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/jquery.ui.touch-punch.min.js"></script>

<!-- MODERNIZR -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/modernizr.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/jquery-migrate-1.3.0.min.js"></script>


<!-- IntlTelInput -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/intlTelInput.js"></script>
<!-- SLICK CAROUSEL -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/slick.min.js"></script>
<!-- FANCYBOX 3 -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/jquery.fancybox.min.js"></script>
<!-- CUSTOM SCROLLBAR -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- datetimepicker -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/jquery.datetimepicker.full.min.js"></script>

<!-- INVIEW -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/jquery.inview.min.js"></script>

<!-- VELOCITY -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/velocity.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/velocity.ui.min.js"></script>

<!-- WOW -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/wow.min.js"></script>

<!-- ANIMATE -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/animate-wow.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/animate-velocity.js"></script>

<!-- CUSTOM -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/custom.js"></script>

<!-- SWEETALERT -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/sweetalert.js"></script>

<!-- JQUERY.VALIDATE -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/jquery.validate.js"></script>

<!-- RNT_AJAX -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/frontend/rnt_ajax.js"></script>