
<!-- Fonts -->

<link href="<?php echo base_url();?>assets/css/frontend/fontawesome-all.min.css" rel="stylesheet" type="text/css" media="all" />

<!-- JsSocials -->
<link href="<?php echo base_url();?>assets/css/frontend/jssocials.css" rel="stylesheet" type="text/css" media="all" />

<link href="<?php echo base_url();?>assets/css/frontend/jssocials-theme-flat.css" rel="stylesheet" type="text/css" media="all" />

<!-- Datetimepicker -->
<link href="<?php echo base_url();?>assets/css/frontend/jquery.datetimepicker.min.css" rel="stylesheet" type="text/css" media="all" />

<!-- IntlTelInput -->
<link href="<?php echo base_url();?>assets/css/frontend/intlTelInput.css" rel="stylesheet" type="text/css" media="all" />

<!-- FancyBox 3 -->
<link href="<?php echo base_url();?>assets/css/frontend/jquery.fancybox.min.css" rel="stylesheet" type="text/css" media="all" />

<!-- Slick -->
<link href="<?php echo base_url();?>assets/css/frontend/slick.css" rel="stylesheet" type="text/css" media="all" />

<!-- JQUERY UI -->
<link href="<?php echo base_url();?>assets/css/frontend/jquery-ui.min.css" rel="stylesheet" type="text/css" media="all" />

<link href="<?php echo base_url();?>assets/css/frontend/jquery-ui.structure.min.css" rel="stylesheet" type="text/css" media="all" />

<link href="<?php echo base_url();?>assets/css/frontend/jquery-ui.theme.min.css" rel="stylesheet" type="text/css" media="all" />

<!-- ANIMATE -->
<link href="<?php echo base_url();?>assets/css/frontend/animate.css" rel="stylesheet" type="text/css" media="all" />

<link href="<?php echo base_url();?>assets/css/frontend/animate-velocity.css" rel="stylesheet" type="text/css" media="all" />

<!-- Custom -->
<link href="<?php echo base_url();?>assets/css/frontend/engage.css" rel="stylesheet" type="text/css" media="all" />

<link href="<?php echo base_url();?>assets/css/frontend/style.css" rel="stylesheet" type="text/css" media="all" />

<link href="<?php echo base_url();?>assets/css/frontend/sweetalert.css" rel="stylesheet" type="text/css" media="all" />

<link href="<?php echo base_url();?>assets/css/frontend/rnt_css.css" rel="stylesheet" type="text/css" media="all" />