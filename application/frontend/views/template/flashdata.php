<!-- Subscribe -->
<?php if($this->session->flashdata('msg') == 'subscribe_success'): ?>
    <script type="text/javascript">
        alert("Subscription is success, stay tuned for update information!");
    </script>
<?php endif; ?>

<?php if($this->session->flashdata('msg') == 'subscribe_failed'): ?>
    <script type="text/javascript">
        alert("Sorry subscription failed, please fill the email correctly!");
    </script>
<?php endif; ?>
<!-- Subscribe -->