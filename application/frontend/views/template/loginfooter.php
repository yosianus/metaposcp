<!DOCTYPE HTML>
<html class="no-js" lang="en"> 
<head>
	</head>

<body>
<footer id="web-footer">
	<div class="footer-bottom">
		<div class="main-container">
			<div class="content">
				<div class="fb-nav">
					<a href="<?php echo site_url("privacy");?>">Privacy Policy</a>
					<a href="<?php echo site_url("terms");?>">Terms and Conditions</a>
				</div><!-- .fb-nav -->
				<div class="copyright">
					Copyright &copy; 2018 MetaPos. All Rights Reserved.
				</div><!-- .copyright -->
				<div class="visionet">
					<p>A Product of:</p>
					<a href="http://www.visionet.co.id/en/" target="_blank"><img src="assets/image/logo-visionet.png" alt="VisioNet"></a>
				</div><!-- .visionet -->
			</div><!-- .content -->
		</div><!-- .main-container -->
	</div><!-- .footer-bottom -->
</footer>
</body>
</html>