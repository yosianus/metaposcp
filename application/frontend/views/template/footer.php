<section class="section-padding has-bg has-overlay overlay-gray" style="background-image:url(assets/image/5a3a074d25b2b_20171220134637-1.jpg);">
	<div class="main-container">
		<div class="max-800 lr-auto n-align-center">
			<h2 class="ngc-maintitle slideDownIn">Interested with Our Products?</h2>
		</div><!-- .max-800 -->
		<br />
		<div class="row">
			<div class="grid-child n-768-2per3 n-no-margin-bottom">
				<div class="contact-map slideLeftIn">
					<div id="map"></div>
				</div><!-- .contact-map -->
			</div><!-- .grid-child -->
			<div class="grid-child n-768-1per3 n-no-margin-bottom slideRightIn">
				<?php
					// foreach($contactt->result() as $row) {
					// 	$fax = $row->ContentText;
					// 	$phone = $row->ContentText;
					// 	$email = $row->ContentText;
					// 			}
				?>
				<h3 class="ngc-title n-primary">MetaPos</h3>
				<ul class="index-contact-list">
                    	<li>
							<span class="icon"><span class="fa fa-map-marker-alt awesome-icon"></span></span>
							<span class="text"><?=$officeaddress->row()->ContentText?></span>
						</li>
						<li>
							<span class="icon"><span class="fa fa-fax awesome-icon"></span></span>
							<span class="text"><?=$companyfax->row()->ContentText?></span>
						</li>
						<li>
							<span class="icon"><span class="fa fa-phone awesome-icon"></span></span>
							<span class="text"><?=$companyphone->row()->ContentText?></span>
						</li>
						<li>
							<span class="icon"><span class="fal fa-envelope awesome-icon"></span></span>
							<span class="text"><?=$salesemail->row()->ContentText?></span>
						</li>

				</ul><!-- .index-contact-list -->
				<a href="<?php echo site_url("contactController");?>" class="btn btn-secondary n-no-margin"><span class="fal fa-envelope"></span>&nbsp;&nbsp;Contact Us</a>
			</div><!-- .grid-child -->
		</div><!-- .row -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->


<script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyDD1AYXApRdEgfw0hmALurpm6m9Bg2Fkro"></script>
<script src="<?=base_url()?>assets/js/frontend/gmaps.min.js"></script>

<script>
    $(document).ready(function(){
    	$(".main-nav-1, .mobile-nav-1").addClass("active");

    	$('.ngc-slider').each(function(){
			var ngcSlider = $(this);
			ngcSlider.on('init', function(event, slick){
				$('.slick-active').addClass('show');
			});
			ngcSlider.slick({
				dots: true,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: true,
				//autoplay: true,
				//autoplaySpeed: 30000,
				prevArrow: ngcSlider.next().find(".ngc-slidePrev"),
				nextArrow: ngcSlider.next().find(".ngc-slideNext"),
				responsive: [
				    {
				      breakpoint: 768,
				      settings: {
				        dots: true,
						arrows: false,
				      }
				    }
				]
			});
			ngcSlider.on('afterChange', function(event, slick, currentSlide, nextSlide){
				$('.slick-slide').removeClass('show');
				$('.slick-active').addClass('show');
			});
		});

		map = new GMaps({
			el: '#map',
			lat: -6.244580,
			lng: 106.628348,
			scrollwheel: false,
			zoom:15,
			styles:
			    [
				    {
				        "featureType": "administrative.locality",
				        "elementType": "all",
				        "stylers": [
				            {
				                "hue": "#c79c60"
				            },
				            {
				                "saturation": 7
				            },
				            {
				                "lightness": 19
				            },
				            {
				                "visibility": "on"
				            }
				        ]
				    },
				    {
				        "featureType": "landscape",
				        "elementType": "all",
				        "stylers": [
				            {
				                "hue": "#ffffff"
				            },
				            {
				                "saturation": -100
				            },
				            {
				                "lightness": 100
				            },
				            {
				                "visibility": "simplified"
				            }
				        ]
				    },
				    {
				        "featureType": "poi",
				        "elementType": "all",
				        "stylers": [
				            {
				                "hue": "#ffffff"
				            },
				            {
				                "saturation": -100
				            },
				            {
				                "lightness": 100
				            },
				            {
				                "visibility": "off"
				            }
				        ]
				    },
				    {
				        "featureType": "road",
				        "elementType": "geometry",
				        "stylers": [
				            {
				                "hue": "#c79c60"
				            },
				            {
				                "saturation": -52
				            },
				            {
				                "lightness": -10
				            },
				            {
				                "visibility": "simplified"
				            }
				        ]
				    },
				    {
				        "featureType": "road",
				        "elementType": "labels",
				        "stylers": [
				            {
				                "hue": "#c79c60"
				            },
				            {
				                "saturation": -93
				            },
				            {
				                "lightness": 31
				            },
				            {
				                "visibility": "on"
				            }
				        ]
				    },
				    {
				        "featureType": "road.arterial",
				        "elementType": "labels",
				        "stylers": [
				            {
				                "hue": "#c79c60"
				            },
				            {
				                "saturation": -93
				            },
				            {
				                "lightness": -2
				            },
				            {
				                "visibility": "simplified"
				            }
				        ]
				    },
				    {
				        "featureType": "road.local",
				        "elementType": "geometry",
				        "stylers": [
				            {
				                "hue": "#c79c60"
				            },
				            {
				                "saturation": -52
				            },
				            {
				                "lightness": -10
				            },
				            {
				                "visibility": "simplified"
				            }
				        ]
				    },
				    {
				        "featureType": "transit",
				        "elementType": "all",
				        "stylers": [
				            {
				                "hue": "#c79c60"
				            },
				            {
				                "saturation": 10
				            },
				            {
				                "lightness": 69
				            },
				            {
				                "visibility": "on"
				            }
				        ]
				    },
				    {
				        "featureType": "water",
				        "elementType": "all",
				        "stylers": [
				            {
				                "hue": "#c79c60"
				            },
				            {
				                "saturation": -78
				            },
				            {
				                "lightness": 67
				            },
				            {
				                "visibility": "simplified"
				            }
				        ]
				    }
				]
			});

				map.addMarker({
										lat:   -6.219702,
										lng:   106.622357,
										icon:  "https://testerovo.000webhostapp.com/assets/image/metaposss.png",
										title: "Main Office",
										infoWindow: {
											content: "<div class='infowindow'><h1>Mobey Indonesia</h1><ul class='marker-data'><li>Jalan Bulevar Gajah Mada No.2120,Panunggangan Barat, Kota Tangerang,Banten 15138</li><li><strong>Phone</strong><br /><a href='tel: 021-55777678'> 021-55777678</a></li><li><strong>Fax</strong><br /><a href='tel: 021-55777677'> 021-55777677</a></li><li><strong>Email</strong><br /><a href='mailto:vidia@visionet.co.id'>vidia@visionet.co.id</a></li></ul></div>"
										}

		});
    });
    $(window).load(function(){

    });
</script>

 <?php
	    $col_email = $this->session->flashdata('email');

?>

<section class="section-padding n-1200-no-padding-bottom slideUpIn">
	<div class="main-container">
		<div class="newsletter-area">
			<div class="ngc-text">
				<h2 class="ngc-title n-primary">Subscribe to Our Newsletter</h2>
				<p>No spam, only promotions.</p>
			</div><!-- .ngc-text -->
            <form action="<?php echo base_url();?>" method="post" id="newsletter_form" onsubmit="myFunction()">
            <!-- <input type="hidden" name="_token" value="G4kVObgE4tBfYp0Wa5QMmWbCxKsoxawkfc60GgQ4"> -->
            <div class="newsletter-form">
				<input type="email" placeholder="Your email address .." class="input-text" name="email" maxlength="200" required="">
				<span><?php echo form_error('email', '<div class="alert-danger">', '</div>');?></span>
				<button class="submit-btn n-no-margin n-1-1per1" type="submit" name="_do_add_new">
					<span class="fal fa-envelope"></span>
					<span class="text n-1-hide n-992-show">SUBSCRIBE</span>
				</button>
			</div><!-- .newsletter-form -->
            </form>
		</div><!-- .newsletter-area -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<script>
function myFunction() {
    alert("The form was submitted");
}
</script>

<footer id="web-footer">
	<div class="footer-top">
		<div class="main-container">
			<div class="content">
				<div class="row">
					<div class="grid-child n-768-1per4 n-768-no-margin-bottom">
						<img src="<?php echo base_url();?>assets/image/metaposss.png" alt="MetaPos" class="footer-logo">
					</div><!-- .grid-child -->
					<div class="grid-child n-768-2per4 n-768-no-margin-bottom">
						<h3 class="ngc-title n-primary">Sitemap</h3>
						<div class="footer-nav">
                            <div class="fnav-group n-1-1per2">
								<a href="<?php echo base_url();?>">Home</a>
								<a href="<?php echo site_url("aboutus");?>">About Us</a>
								<a href="<?php echo site_url("howitworks");?>">How It Works</a>
								<a href="<?php echo site_url("contact");?>">Contact Us</a>
							</div><!-- .fnav-group -->
							<div class="fnav-group n-1-1per2">
								<a href="<?php echo site_url("product");?>">Our Features</a>
								<a href="<?php echo site_url("news");?>">News &amp; Updates</a>

                                <a href='https://metapos.vdicloud.id' target="new">Login</a>
								<?php /*
								<a href="<?php echo site_url("regis");?>">Register</a>
                                */ ?>
							</div><!-- .fnav-group -->
						</div><!-- .footer-nav -->
					</div><!-- .grid-child -->

                    <div class="grid-child n-768-1per4 n-no-margin-bottom">
						<h3 class="ngc-title n-primary">Stay Tuned</h3>
						<div class="footer-soc">
							<?php
							?>

							<div class="fsoc-item">
								<a href=<?=$facebooklink->row()->ContentText?> target="_blank">
									<span class="stacking-icons">
									  <i class="fa fa-circle circle"></i>
									  <i class="fab fa-facebook-f soc-icon" style="color:#fff;"></i>
									</span>
								</a>
							</div><!-- .fsoc-item -->

                        	<div class="fsoc-item">
								<a href=<?=$linkedinlink->row()->ContentText?> target="_blank">
									<span class="stacking-icons">
									  <i class="fa fa-circle circle"></i>
									  <i class="fab fa-linkedin-in soc-icon" style="color:#fff;"></i>
									</span>
								</a>
							</div><!-- .fsoc-item -->

                             <div class="fsoc-item">
								<a href=<?=$instagramlink->row()->ContentText?>>
									<span class="stacking-icons">
									  <i class="fa fa-circle circle"></i>
									  <i class="fab fa-instagram soc-icon" style="color:#fff;"></i>
									</span>
								</a>
							</div><!-- .fsoc-item -->

							<div class="fsoc-item">
								<a href=<?=$twitterlink->row()->ContentText?> target="_blank">
									<span class="stacking-icons">
									  <i class="fa fa-circle circle"></i>
									  <i class="fab fa-twitter soc-icon" style="color:#fff;"></i>
									</span>
								</a>
							</div><!-- .fsoc-item -->

                            <div class="fsoc-item">
								<a href=<?=$youtubelink->row()->ContentText?> target="_blank">
									<span class="stacking-icons">
									  <i class="fa fa-circle circle"></i>
									  <i class="fab fa-youtube soc-icon" style="color:#fff;"></i>
									</span>
								</a>
							</div><!-- .fsoc-item -->

                            <div class="fsoc-item">
								<a href=<?=$googlelink->row()->ContentText?> target="_blank">
									<span class="stacking-icons">
									  <i class="fa fa-circle circle"></i>
									  <i class="fab fa-google-plus-g soc-icon" style="color:#fff;"></i>
									</span>
								</a>
							</div><!-- .fsoc-item -->

						</div><!-- .footer-soc -->
					</div><!-- .grid-child -->
				</div><!-- .row -->
			</div><!-- .content -->
		</div><!-- .main-container -->
	</div><!-- .footer-top -->
	<div class="footer-bottom">
		<div class="main-container">
			<div class="content">
				<div class="fb-nav">
					<a href="<?php echo site_url("privacy");?>">Privacy Policy</a>
					<a href="<?php echo site_url("terms");?>">Terms and Conditions</a>
				</div><!-- .fb-nav -->
				<div class="copyright">
					Copyright &copy; 2018 MetaPos. All Rights Reserved.
				</div><!-- .copyright -->
				<div class="visionet">
					<p>A Product of:</p>
					<a href="http://www.visionet.co.id/en/" target="_blank"><img src="<?php echo base_url();?>assets/image/logo-visionet.png" alt="VisioNet"></a>
				</div><!-- .visionet -->
			</div><!-- .content -->
		</div><!-- .main-container -->
	</div><!-- .footer-bottom -->
</footer>
