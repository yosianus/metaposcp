

 <?php
	    $col_email = $this->session->flashdata('email');

?>

<section class="section-padding n-1200-no-padding-bottom slideUpIn">
	<div class="main-container">
		<div class="newsletter-area">
			<div class="ngc-text">
				<h2 class="ngc-title n-primary">Subscribe to Our Newsletter</h2>
				<p>No spam, only promotions.</p>
			</div><!-- .ngc-text -->
            <form action="<?php echo base_url();?>" method="post" id="newsletter_form" onsubmit="myFunction()">
            <div class="newsletter-form">
				<input type="email" placeholder="Your email address .." class="input-text" name="email" maxlength="200" value="<?=$col_email?>">
					<button class="submit-btn n-no-margin n-1-1per1" type="submit" name="_do_add_new">

					<span class="fal fa-envelope"></span>
					<span class="text n-1-hide n-992-show">SUBSCRIBE</span>
				</button>
			</div><!-- .newsletter-form -->
            </form>
		</div><!-- .newsletter-area -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<script>
function myFunction() {
    alert("The form was submitted");
}
</script>

<footer id="web-footer">
	<div class="footer-top">
		<div class="main-container">
			<div class="content">
				<div class="row">
					<div class="grid-child n-768-1per4 n-768-no-margin-bottom">
						<img src="<?php echo base_url();?>assets/image/metaposss.png" alt="MetaPos" class="footer-logo">
					</div><!-- .grid-child -->
					<div class="grid-child n-768-2per4 n-768-no-margin-bottom">
						<h3 class="ngc-title n-primary">Sitemap</h3>
						<div class="footer-nav">
                            <div class="fnav-group n-1-1per2">
								<a href="<?php echo base_url();?>">Home</a>
								<a href="<?php echo site_url("aboutus");?>">About Us</a>
								<a href="<?php echo site_url("howitworks");?>">How It Works</a>
								<a href="<?php echo site_url("contact");?>">Contact Us</a>
							</div><!-- .fnav-group -->
							<div class="fnav-group n-1-1per2">
								<a href="<?php echo site_url("product");?>">Our Features</a>
								<a href="<?php echo site_url("news");?>">News &amp; Updates</a>

                                <a href='https://metapos.vdicloud.id' target="new">Login</a>
								<?php
								/*
								<a href="<?php echo site_url("regis");?>">Register</a>
                                */
								?>
							</div><!-- .fnav-group -->
						</div><!-- .footer-nav -->
					</div><!-- .grid-child -->

                    <div class="grid-child n-768-1per4 n-no-margin-bottom">
						<h3 class="ngc-title n-primary">Stay Tuned</h3>
						<div class="footer-soc">
							<?php
							?>

							<div class="fsoc-item">
								<a href=<?=$facebooklink->row()->ContentText?> target="_blank">
									<span class="stacking-icons">
									  <i class="fa fa-circle circle"></i>
									  <i class="fab fa-facebook-f soc-icon" style="color:#fff;"></i>
									</span>
								</a>
							</div><!-- .fsoc-item -->

                        	<div class="fsoc-item">
								<a href=<?=$linkedinlink->row()->ContentText?> target="_blank">
									<span class="stacking-icons">
									  <i class="fa fa-circle circle"></i>
									  <i class="fab fa-linkedin-in soc-icon" style="color:#fff;"></i>
									</span>
								</a>
							</div><!-- .fsoc-item -->

                             <div class="fsoc-item">
								<a href=<?=$instagramlink->row()->ContentText?>>
									<span class="stacking-icons">
									  <i class="fa fa-circle circle"></i>
									  <i class="fab fa-instagram soc-icon" style="color:#fff;"></i>
									</span>
								</a>
							</div><!-- .fsoc-item -->

							<div class="fsoc-item">
								<a href=<?=$twitterlink->row()->ContentText?> target="_blank">
									<span class="stacking-icons">
									  <i class="fa fa-circle circle"></i>
									  <i class="fab fa-twitter soc-icon" style="color:#fff;"></i>
									</span>
								</a>
							</div><!-- .fsoc-item -->

                            <div class="fsoc-item">
								<a href=<?=$youtubelink->row()->ContentText?> target="_blank">
									<span class="stacking-icons">
									  <i class="fa fa-circle circle"></i>
									  <i class="fab fa-youtube soc-icon" style="color:#fff;"></i>
									</span>
								</a>
							</div><!-- .fsoc-item -->

                            <div class="fsoc-item">
								<a href=<?=$googlelink->row()->ContentText?> target="_blank">
									<span class="stacking-icons">
									  <i class="fa fa-circle circle"></i>
									  <i class="fab fa-google-plus-g soc-icon" style="color:#fff;"></i>
									</span>
								</a>
							</div><!-- .fsoc-item -->

						</div><!-- .footer-soc -->
					</div><!-- .grid-child -->
				</div><!-- .row -->
			</div><!-- .content -->
		</div><!-- .main-container -->
	</div><!-- .footer-top -->
	<div class="footer-bottom">
		<div class="main-container">
			<div class="content">
				<div class="fb-nav">
					<a href="<?php echo site_url("privacy");?>">Privacy Policy</a>
					<a href="<?php echo site_url("terms");?>">Terms and Conditions</a>
				</div><!-- .fb-nav -->
				<div class="copyright">
					Copyright &copy; 2018 MetaPos. All Rights Reserved.
				</div><!-- .copyright -->
				<div class="visionet">
					<p>A Product of:</p>
					<a href="http://www.visionet.co.id/en/" target="_blank"><img src="<?php echo base_url();?>assets/image/logo-visionet.png" alt="VisioNet"></a>
				</div><!-- .visionet -->
			</div><!-- .content -->
		</div><!-- .main-container -->
	</div><!-- .footer-bottom -->
</footer>
