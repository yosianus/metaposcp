<div id="web-header">
  <div class="main-container">
    <div class="content">
      <div class="logo">
        <a href="<?php echo base_url();?>" title="MetaPos">
          <img src="<?php echo base_url();?>assets/image/metaposss.png" alt="MetaPos"></a>
      </div><!-- .logo -->
      <ul class="main-nav n-1-hide n-992-show">
        <li class="main-nav-1"><a href="<?php echo base_url();?>">Home</a></li>
        <li class="main-nav-2"><a href="<?php echo site_url("aboutus");?>">About Us</a></li>

          <li class="main-nav-3 has-sub">
            <a href="<?php echo site_url("product");?>">Our Features <span class="fal fa-angle-down"></span></a>
            <div class="sub-nav-wrap">
              <div class="sub-nav">
                <?php
                  if($produk != false){
                    foreach($produk->result() as $row){
                ?>
                <div class="nav-item">
                  <a href="<?php echo site_url("ProdukController/detail/$row->Alias");?>"><?=$row->ProductName?></a>
                </div>
                <?php
                    }
                  }
                ?>
              </div><!-- .sub-nav -->
            </div><!-- .sub-nav-wrap -->
          </li>
          <li class="main-nav-4"><a href="<?php echo site_url("howitworks");?>">How It Works</a></li>
          <li class="main-nav-5"><a href="<?php echo site_url("contact");?>">Contact Us</a></li>
      </ul><!-- .main-nav -->
      <div class="header-right">
        <div class="hr-child">
          <a href="#" class="search-toggle"><span class="fas fa-search"></span></a>
        </div><!-- .hr-child -->
        <div class="hr-child lang-toggle-wrap">
          <a href="#" class="lang-toggle">
            <img src="<?php echo base_url();?>assets/image/eng.png">
            <span class="text">EN</span>
            <span class="far fa-angle-down"></span>
          </a>
          <div class="lang-selection-wrap">
            <div class="lang-selection">
              <div class="lang-item">
                <a href="#" class="active lang_btn" name="5">
                  <span class="text">English <span class='fa fa-check'></span></span>
                </a>
              </div><!-- .lang-item -->
              <!-- <div class="lang-item">
                <a href="#" class=" lang_btn" name="4">
                  <span class="text">Indonesia </span>
                </a> -->
              <!-- </div> --><!-- .lang-item -->
            </div><!--.lang-selection-->
          </div><!-- .lang-list-wrap -->

        </div><!-- .hr-child -->
        <div class="hr-child">
          <a data-fancybox data-type="ajax" data-src="<?php echo site_url("HomeController/login");?>" href="javascript:;" class="btn-member-toggle" data-toggle="modal">
            <span class="fas fa-user"></span>
            <span class="text n-1-hide n-768-show">LOGIN / SIGN UP</span>
          </a>
        </div><!-- .hr-child -->
      </div><!-- .header-right -->
      <a href="#" id="toggle-mmenu" class="toggle-mobilemenu n-992-hide">
        <span class="outer-span">
          <button class="c-hamburger c-hamburger--htx">
            <span>MENU</span>
          </button>
        </span>
      </a>
    </div><!-- .content -->
  </div><!-- .main-container -->
</div><!-- #web-header -->

<div class="mmenu-wrap">
  <div class="mmenu">
    <a href="#" class="close-mmenu-toggle">
      <span class="fa fa-circle"></span>
      <span class="fal fa-times"></span>
    </a>
    <div class="content-wrap">
      <div class="content">
        <div class="mmenu-nav">
          <div class="nav-item mobile-nav-1">
            <a href="<?php echo base_url();?>">Home</a>
          </div><!-- .nav-item -->

          <div class="nav-item mobile-nav-2">
            <a href="<?php echo site_url("aboutus");?>">About Us</a>
          </div><!-- .nav-item -->

          <div class="nav-item mobile-nav-3 has-sub">
            <a href="<?php echo site_url("product");?>">Our Products&nbsp;<span class="fal fa-angle-down"></span></a>

            <div class="sub-nav">
              <?php
                    if($produk != false){
                      foreach($produk->result() as $row){
                ?>
              <div class="nav-item">

                <a href="<?php echo site_url("ProdukController/detail/$row->ProductId");?>"><?=$row->ProductName?></a>
              </div>
              <?php
                    }
                  }
              ?>
            </div>
          </div>
          <div class="nav-item mobile-nav-4">
            <a href="<?php echo site_url("howitworks");?>">How It Works</a>
          </div><!-- .nav-item -->
          <div class="nav-item mobile-nav-5">
            <a href="<?php echo site_url("contact");?>">Contact Us</a>
          </div><!-- .nav-item -->
        </div><!-- .mmenu-nav -->
      </div><!-- .content -->
    </div><!-- .content-wrap -->
  </div><!-- .mmenu -->
</div><!-- .mmenu-wrap -->

<div class="search-area-wrap">
  <div class="search-area">
    <a href="#" class="close-search-toggle">
      <span class="fa fa-circle"></span>
      <span class="fal fa-times"></span>
    </a>
    <div class="content">
      <p class="ngc-title">Hello!</p>
      <p>What do you want to search?</p>

      <form action="<?php echo site_url("HomeController/search");?>" method="post" class="search-form">
        <input type="hidden" name="_token" value="G4kVObgE4tBfYp0Wa5QMmWbCxKsoxawkfc60GgQ4">
          <div class="search-form">
            <input type="text" placeholder="Type your search here" class="input-text" name="keyword" maxlength="200" required>
            <button class="submit-btn" name="_do_search">
              <span class="fal fa-search"></span><a href="<?php echo site_url("HomeController/search");?>" >
              <span class="text n-1-hide n-992-show">SEARCH</span></a>
            </button>
          </div><!-- .search-form -->
      </form>
    </div><!-- .content -->
  </div><!-- .search-area -->
</div><!-- .search-area-wrap -->
</div>
</div>
