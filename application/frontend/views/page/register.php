<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--<![endif]-->

<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="all,index,follow">
<meta name="googlebot" content="all,index,follow">
<meta name="revisit-after" content="2 days">
<meta name="rating" content="general">
<meta name="viewport" content="width=device-width, initial-scale=1">


<title>MetaPos</title>

<meta name="keywords" content="Welcome to MetaPos">
<meta name="description" content="Welcome to MetaPos">
<meta property="og:title" content="Welcome to MetaPos">
<meta property="og:image" content="assets/image/metaposss.png">
<meta property="og:site_name" content="MetaPos">
<meta property="og:description" content="Welcome to MetaPos">

<link rel="shortcut icon" href="assets/image/metaposss.png" type="image/x-icon">
<!-- Theme color for chrome, firefox and opera -->
<meta name="theme-color" content="#FFA812" />
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#FFA812">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="HandheldFriendly" content="true" />
<!-- Add to home screen and color theme -->


<?php
	echo $style;
	echo $script;
?>

</head>

<body class="n-no-padding">


<!-- html start here -->

<section id="clean-layout">
	<div class="main-container">
		<div class="max-430 lr-auto">
			<div class="popup-header">
                <h1><img src="assets/image/metaposss.png" alt="MetaPos"></h1>
				<p>Create Online Registration.</p>
				<p>Already have an account? <a href='https://metapos.vdicloud.id'>Login</a></p>
			</div><!-- .popup-header -->
		</div><!-- .max-430 -->

		<div class="max-750 lr-auto">
         <form action="" method="post" class="" onsubmit="myFunction()">
			<div class="row same-height medium-gutter" >
				<div class="grid-child n-540-1per2 n-no-margin-bottom">
					<col span="12" style="background-color:white">
					<div class="form-group">
						<label>Jenis Usaha</label>
								<div class="input-wrap has-icon">
									<input type="checkbox" name="jenis_usaha"> Kesehatan<br>
									<input type="checkbox" name="jenis_usaha"> Hiburan<br>
									<input type="checkbox" name="jenis_usaha"> Pendidikan<br>
									<input type="checkbox" name="jenis_usaha"> Makanan & Minuman<br>
									<input type="checkbox" name="jenis_usaha"> Perbelanjaan<br>
									<input type="checkbox" name="jenis_usaha"> Travel<br>
									<input type="checkbox" name="jenis_usaha"> Otomotif<br>
									<input type="checkbox" name="jenis_usaha"> Layanan Pribadi<br>
									<input type="checkbox" name="jenis_usaha"> Utilities<br>
									<input type="checkbox" name="jenis_usaha"> Olahraga<br>
									<input type="checkbox" name="jenis_usaha"> Transportasi<br>
									<input type="checkbox" name="jenis_usaha"> Online Shop<br>
								</div><!-- .input-wrap -->
					</div><!-- .form-group -->
				</div><!-- .grid-child -->
			</div><!-- .row -->

	<div class="max-750 lr-auto">
        <form action="" method="post" class="">
			<div class="row same-height medium-gutter">
				<div class="grid-child n-540-1per2 n-no-margin-bottom">
					<div class="form-group">
						<label>Nama Usaha</label>
						<div class="input-wrap has-icon">
							<input type="text" placeholder="" name="nama_usaha" maxlength="200">
						</div><!-- .input-wrap -->
					</div><!-- .form-group -->
				</div><!-- .grid-child -->
				<div class="grid-child n-540-1per2 n-no-margin-bottom">
					<div class="form-group">
						<label>Nama Pemilik</label>
						<div class="input-wrap has-icon">
							<input type="text" placeholder="" name="nama_pemilik" maxlength="200">
						</div><!-- .input-wrap -->
					</div><!-- .form-group -->
				</div><!-- .grid-child -->
				<div class="grid-child n-540-1per2 n-no-margin-bottom">
					<div class="form-group">
						<label>Kota</label>
						<div class="input-wrap has-icon">
  							<div class="dropdown">
   								<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Pilih Kota
    								<span class="caret"></span></button>
    									<ul class="dropdown-menu">
      										<li>Jakarta</li>
      										<li>Bandung</li>
      										<li>Semarang</li>
    									</ul>
 							 </div>
						</div><!-- .input-wrap -->
					</div><!-- .form-group -->
				</div><!-- .grid-child -->
				<div class="grid-child n-540-1per2 n-no-margin-bottom">
					<div class="form-group">
						<label>Email</label>
						<div class="input-wrap has-icon">
							<input type="text" placeholder="" name="email" maxlength="200">
						</div><!-- .input-wrap -->
					</div><!-- .form-group -->
				</div><!-- .grid-child -->
				<div class="grid-child n-540-1per2 n-no-margin-bottom">
					<div class="form-group">
						<label>Alamat Usaha</label>
						<div class="input-wrap has-icon">
							<input type="text" placeholder="" name="alamat" maxlength="200">
						</div><!-- .input-wrap -->
					</div><!-- .form-group -->
				</div><!-- .grid-child -->
				<div class="grid-child n-540-1per2 n-no-margin-bottom">
					<div class="form-group">
						<label>No. Ponsel</label>
						<div class="input-wrap has-icon">
							<input type="text" placeholder="" name="nomor_telepon">
						</div><!-- .input-wrap -->
					</div><!-- .form-group -->
				</div><!-- .grid-child -->
				<div class="grid-child n-540-1per2 n-no-margin-bottom">
					<div class="form-group">
						<label>OVO ID</label>
						<div class="input-wrap has-icon">
							<input type="text" placeholder="" name="ovo_id" maxlength="200">
						</div><!-- .input-wrap -->
					</div><!-- .form-group -->
				</div><!-- .grid-child -->
				<div class="grid-child n-540-1per2 n-no-margin-bottom">
					<div class="form-group">
						<label>Deskripsi</label>
						<div class="input-wrap has-icon">
							<input type="text" placeholder="" name="deskripsi" maxlength="200">
						</div><!-- .input-wrap -->
					</div><!-- .form-group -->
				</div><!-- .grid-child -->
			</div><!-- .row -->

			<div class="row medium-gutter">
				<div class="grid-child n-768-1per2 n-768-no-margin-bottom">
					<div class="captcha-wrap">
						<div class="g-recaptcha" data-sitekey="6Le5r1AUAAAAAIJdgXAac6uOShSVWwQlzVK7JMRG"></div>
					</div><!-- .captcha-wrap -->
				</div><!-- .grid-child -->
				<div class="grid-child n-768-1per2 n-no-margin-bottom">
					<button class="btn btn-secondary n-no-margin n-1-1per1 btn_urel_submit" >
						<!-- <button class="btn btn-secondary n-no-margin n-1-1per1 btn_urel_submit" type="submit" name="<?=$buttonname?>" value="submit"> -->
					REGISTER NOW</button>
                    <a href="#" style="display:none;" class="btn btn-secondary n-no-margin n-1-1per1 shdow-btn">LOADING...</a>
                    <span class="msg_terms_click"></span>
                    </div><!-- .grid-child -->
                </div><!-- .row -->
		</form><!-- .login-form -->

		<script>
function myFunction() {
    alert("The form was submitted");
}
</script>



		</div><!-- .max-750 -->
	</div><!-- .main-container -->
</section><!-- #clean-layout -->

<?php
	echo $regisfooter;
?>

</body>
</html>
