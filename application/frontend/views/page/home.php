<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--<![endif]-->

<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="all,index,follow">
<meta name="googlebot" content="all,index,follow">
<meta name="revisit-after" content="2 days">
<meta name="rating" content="general">
<meta name="viewport" content="width=device-width, initial-scale=1">


<title>Welcome to MetaPos Site</title>

<meta name="keywords" content="Welcome to MetaPos">
<meta name="description" content="Welcome to MetaPos">
<meta property="og:title" content="Welcome to MetaPos">
<meta property="og:image" content="assets/image/metaposss.png">
<meta property="og:site_name" content="MetaPos">
<meta property="og:description" content="Welcome to MetaPos">

<link rel="shortcut icon" href="<?php echo base_url();?>assets/image/metaposss.png" type="image/x-icon">
<!-- Theme color for chrome, firefox and opera -->
<meta name="theme-color" content="#FFA812" />
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#FFA812">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="HandheldFriendly" content="true" />
<!-- Add to home screen and color theme -->


<?php
	echo $style;
	echo $script;
?>

</head>

<body>
	<?php
		echo $header;
	?>

<section class="hero-slider ngc-slider-wrap n-992-dotLeft n-992-arrowHide n-1200-arrowShow n-1200-arrowEdge">
	<div class="ngc-slider">
		<div class="item">
			<div class="slider-item n-992-lightText">
				<div class="slider-media" style="background-image:url(assets/image/slide 1.png);">
					<a href="http://google.com">
						<img src="<?php echo base_url();?>assets/image/slide 1.png" class="slider-img" alt="Easier, safer, faster, and cost-effective payment solution">
					</a>
				</div><!-- .slider-media -->
				<div class="slider-text-container">
					<div class="main-container">
						<div class="slider-text">
							<!-- <div class="slider-title">Easier, safer, faster, and cost-effective payment solution</div> -->
							<p>METAPOS is easy to use and easy to be implemented in your business/store. All in one point of sales in an application.</p>
							<a href="<?php echo site_url("product");?>" class="btn btn-slide">LEARN MORE</a>
						</div><!-- .slider-text -->
					</div><!-- .main-container -->
				</div><!-- .slider-text-container -->
			</div><!-- .slider-item -->
		</div><!-- .item -->
		<div class="item">
			<div class="slider-item n-992-lightText">
				<div class="slider-media" style="background-image:url(assets/image/slide2.png);">
					<a href="#">
						<img src=" <?php echo base_url();?>assets/image/Artboard 3 copy 3.png" class="slider-img" alt="A world-class product for your business payment solution">
					</a>
				</div><!-- .slider-media -->
				<div class="slider-text-container">
					<div class="main-container">
						<div class="slider-text">
							<div class="slider-title">Accurate & Grow Your Business</div>
							<p>METAPOS is the one stop point of sales that takes care of your store with automated inventory counting to control, customer management, and valuable report that can be a fondation to grow your business</p>
						</div><!-- .slider-text -->
					</div><!-- .main-container -->
				</div><!-- .slider-text-container -->
			</div><!-- .slider-item -->
		</div><!-- .item -->
		<div class="item">
			<div class="slider-item n-992-lightText">
				<div class="slider-media" style="background-image:url(assets/image/slide 3.png);">
					<a href="#">
						<img src="<?php echo base_url();?>assets/image/slide 3.png" class="slider-img" alt="Protect your customer and business data ">
					</a>
				</div><!-- .slider-media -->
				<div class="slider-text-container">
					<div class="main-container">
						<div class="slider-text">
							<div class="slider-title">Simplified</div>
							<p>METAPOS is simple, not as complicated as the regular cash register. Everyone can easily use METAPOS.</p>
						</div><!-- .slider-text -->
					</div><!-- .main-container -->
				</div><!-- .slider-text-container -->
			</div><!-- .slider-item -->
		</div><!-- .item -->
	</div><!-- .ngc-slider -->
	<div class="ngc-slider-arrow">
		<div class="nsa-container">
			<a href="#" class="ngc-slideNext">Next</a>
			<a href="#" class="ngc-slidePrev">Prev</a>
		</div><!-- .nsa-container -->
	</div><!-- .ngc-slider-arrow -->
</section><!-- .ngc-slider-limiter -->

<section class="section-padding">
	<div class="main-container">
		<div class="index-about">
			<div class="ia-image n-1-hide n-768-show slideLeftInFlex">
				 <img src="<?php echo base_url();?>assets/image/ryny.png" alt="MetaPos Device">
			</div><!-- .ia-image -->
			<div class="content">
        <h1 class="ngc-maintitle slideUpIn">What is MetaPos?</h1>
				<p class="ngc-intro slideUpIn">Developed in 2017, METAPOS is an apps offering one stop point of sales solution. METAPOS focuses on merchant operation, in order to deliver easiness, simplified, and accurate point of sales solutions for your day to day store operating activities. With the features offered, METAPOS aims to eliminate error in recording daily transaction, offering multiple payment methods, and providing accurate store performace report automatically.</p>
<!--
				<p class="ngc-intro slideUpIn">Dikembangkan pada tahun 2017, METAPOS adalah inovasi baru dari sistem point of sales. METAPOS berfokus pada operasi pedagang, dalam rangka memberikan solusi penjualan terbaik, mudah, sederhana, dan akurat untuk kegiatan operasi toko Anda. Dengan fitur-fitur yang ditawarkan, METAPOS bertujuan untuk menyingkirkankan kesalahan dalam pencatatan transaksi, menawarkan metode pembayaran yang beragam, dan menyediakan laporan penjualan toko secara otomatis melalui sistem yang menjadikan Metapos sistem manajemen kasir berkelas dunia. Dengan menyediakan layanan yang lengkap dari instalasi hingga pemeliharaan, Metapos akan memastikan bisnis Anda berjalan lebih baik dari yang pernah ada.</p>
-->

				<div class="row same-height why-us">
					<div class="grid-child n-360-1per2">
						<div class="wu-item slideRightIn">
							<img src="<?php echo base_url();?>assets/image/low cost.png" alt="Secure">
							<h2 class="ngc-title">Low Cost</h2>
							<p>We provide you Cash Register Solution with Low Operation Cost to make your business grow effortlessly</p>
<!--
							<img src="<?php echo base_url();?>assets/image/laporan.png" alt="Laporan" style="width:70px;height:75px;">
							<h2 class="ngc-title">Laporan & Dashboard Transaksi realtime</h2>
							<!-- <p>Reliable agent banking with point-to-point encryption to safe guard all transactions with Certified National Standard Indonesian Chip Card Specification (NSICCS)</p> -->
						</div><!-- .wu-item -->
					</div><!-- .grid-child -->
					<div class="grid-child n-360-1per2">
						<div class="wu-item slideRightIn">
							<img src="<?php echo base_url();?>assets/image/realtime dashboard.png" alt="Fast Update">
							<h2 class="ngc-title">Realtime Dashboard</h2>
							<p>One Click away to monitoring Store Performance with realtime dashboard and various report that we tailored for you. </p>
<!--
							<img src="<?php echo base_url();?>assets/image/metode.png" alt="Metode Pembayaran Variatif" style="width:70px;height:75px;">
							<h2 class="ngc-title">Metode Pembayaran Variatif</h2>
							<!-- <p>We are constantly improving with free updates that equip you with great new features to help you run your business more efficiently and effectively.</p> -->
						</div><!-- .wu-item -->
					</div><!-- .grid-child -->
					<div class="grid-child n-360-1per2">
						<div class="wu-item slideRightIn">
							<img src="<?php echo base_url();?>assets/image/persediaan.png" alt="Easy">
							<h2 class="ngc-title">Inventory Management</h2>
							<p>We can help you to maintain your inventory, so you can track goods and always have a 'good to go' sales.</p>
<!--
							<img src="<?php echo base_url();?>assets/image/persediaan.png" alt="Kontrol Persediaan" style="width:70px;height:75px;">
							<h2 class="ngc-title">Kontrol Persediaan</h2>
							<!-- <p>MetaPos is easy to use and easy to be implemented in your business , Support for Magnetic and Chip card</p> -->
						</div><!-- .wu-item -->
					</div><!-- .grid-child -->
					<div class="grid-child n-360-1per2">
						<div class="wu-item slideRightIn">
							<img src="<?php echo base_url();?>assets/image/program loyalitas.png" alt="Program loyalitas pelanggan" style="width:70px;height:75px;">
							<h2 class="ngc-title">Program loyalitas pelanggan</h2>
							<!-- <p>MetaPos is the point of sale that takes care of digital receipts, sales reports and provides valuable analytics so that makes it easier to run your business.</p> -->
						</div><!-- .wu-item -->
					</div><!-- .grid-child -->
					<div class="grid-child n-360-1per2">
						<div class="wu-item slideRightIn">
							<img src="<?php echo base_url();?>assets/image/settlement.png" alt="Settlement" style="width:70px;height:75px;">
							<h2 class="ngc-title">Settlement</h2>
							<!-- <p>MetaPos is easy to use and easy to be implemented in your business , Support for Magnetic and Chip card</p> -->
						</div><!-- .wu-item -->
					</div><!-- .grid-child -->
					<div class="grid-child n-360-1per2">
						<div class="wu-item slideRightIn">
							<img src="<?php echo base_url();?>assets/image/promo.png" alt="Promo & Diskon" style="width:70px;height:75px;">
							<h2 class="ngc-title">Promo & Diskon</h2>
							<!-- <p>MetaPos is the point of sale that takes care of digital receipts, sales reports and provides valuable analytics so that makes it easier to run your business.</p> -->
						</div><!-- .wu-item -->
					</div><!-- .grid-child -->
					<div class="grid-child n-360-1per2">
						<div class="wu-item slideRightIn">
							<img src="<?php echo base_url();?>assets/image/monitoring.png" alt="Monitoring" style="width:70px;height:78px;">
							<h2 class="ngc-title">Monitoring</h2>
							<!-- <p>MetaPos is easy to use and easy to be implemented in your business , Support for Magnetic and Chip card</p> -->
						</div><!-- .wu-item -->
					</div><!-- .grid-child -->

				</div><!-- .why-us -->
				<a href="<?php echo site_url("aboutus");?>" class="btn btn-secondary n-no-margin slideRightIn">Learn More</a>
			</div><!-- .content -->
		</div><!-- .index-about -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding has-bg has-overlay overlay-gray" style="background-image:url(assets/image/5a39ef8544fc8_20171220120509-1.jpg);">
	<div class="main-container">
		<div class="max-800 lr-auto n-align-center">
			<h2 class="ngc-maintitle slideDownIn">Features</h2>
			<p class="ngc-intro slideUpIn">Cash Register solutions by MetaPos are easier, simple, accurate and more cost-effective than traditional cash register solution. Our world-class products can facilitate your transaction better for your business</p>
		</div><!-- .max-800 -->
		<div class="row same-height">
			<?php
                if($produkdetail != false){
                    foreach($produkdetail->result() as $row){
            ?>
            <div class="grid-child n-540-1per2 n-768-1per3">
            	<div class="pc-item fadeIn">

            		<div class="ngc-media">
            			<a href="<?php echo site_url("ProdukController/detail/$row->Alias");?>">

                            <img class="center" src="<?=$row->Pic?>">
            			</a>
            		</div><!-- .ngc-media -->
            		<div class="ngc-text">
            			<h3 class="ngc-title"><?=$row->ProductName?></h3>
                            <p><?=$row->Description?></p>
            			<a href="<?php echo site_url("ProdukController/detail/$row->Alias");?>" class="link-more">Learn More &rsaquo;</a>
            		</div><!-- .ngc-text -->
            	</div><!-- .pc-item -->
            </div><!-- .grid-child -->
            <?php
                    }
                }
            ?>
        </div>
		<div class="n-align-center slideUpIn">
			<a href="<?php echo site_url("product");?>" class="btn btn-secondary n-no-margin">VIEW ALL METAPOS FEATURES</a>
		</div>
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding">
	<div class="main-container">
		<div class="row">
			<div class="grid-child n-768-1per3 n-768-no-margin-bottom n-align-center n-768-align-left">
				<h2 class="ngc-maintitle slideLeftIn">Our Clients</h2>
				<p class="ngc-intro n-no-margin-bottom slideLeftIn">MetaPos has been trusted to make our clients' payment easier, safer, and cost-effective from various industry such as the bank, retail, department store, education, healthcare, food & beverages and ready to help you to begin your transaction evolution.</p>
			</div><!-- .grid-child -->
			<div class="grid-child n-768-2per3 n-no-margin-bottom">
				<div class="row medium-gutter same-height n-1-row-center n-768-row-left">
					<?php
                		if($klien != false){
                    		foreach($klien->result() as $row){
            		?>
					<div class="grid-child n-1-1per3 n-540-1per4 n-768-1per3 n-992-1per4 slideUpInFlex">
						<div class="client-item">
							<a href="#" target="_blank">

								<img src="<?=$row->ClientLogo?>">

							</a>
						</div> <!--.client-item -->
					</div> <!--.grid-child -->

					<?php
							}
						}
					?>
                </div><!-- .row -->
			</div><!-- .grid-child -->
		</div><!-- .row -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding has-bg has-overlay overlay-primary" style="background-image: url(assets/image/5a39ee49f05d6_20171220115953-1.jpg);">
	<div class="main-container">
        <div class="max-800 lr-auto n-align-center">
			<h2 class="ngc-maintitle n-primary slideDownIn">Client Testimonials</h2>
			<p class="ngc-intro slideUpIn">How MetaPos helps Businesses Grows.</p>
		</div><!-- .max-800 -->
		<div class="testi-carousel-wrap">
			<div class="testi-carousel">
				<?php
                	if($testimoni != false){
                    	foreach($testimoni->result() as $row){
            	?>
                <div class="item">
                	<div class="testi-item slideUpIn">
                		<div class="testi-text">

                			<p class="n-no-margin"><?=$row->Testimonial?></p>

                		</div><!-- .testi-text -->
                		<div class="testi-people">
                			<div class="ngc-media">
                				<img src="<?=$row->ClientLogo?>" alt="ovo">
                			</div><!-- .ngc-media -->
                			<h3 class="ngc-title"><?=$row->ClientName?></h3>

                			<!-- <span class="ngc-subtext">Business Development</span> -->
                		</div><!-- .testi-people -->
                	</div><!-- .testi-item slideUpIn -->
                </div><!-- .item -->
                <?php
                		}
                	}
                ?>
			</div><!-- .testi-carousel -->
		</div><!-- .testi-carousel-wrap -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding">
	<div class="main-container">
		<div class="max-800 lr-auto n-align-center">
			<h2 class="ngc-maintitle slideDownIn">Latest News &amp; Updates</h2>
			<p class="ngc-intro slideUpIn">Get updated with our fresh news from MetaPos to get to know our newest features in our products. Have an insight about our event and team that develops MetaPos</p>
		</div><!-- .max-800 -->
		<div class="row same-height">

			<div class="grid-child n-768-1per2 slideLeftInFlex">
				<?php
                	if($artikel != false){
                    	foreach($artikel->result() as $row){
            	?>
				<div class="news-item">
					<div class="ngc-media">

							<img src="<?php echo base_url();?>assets/image/5acaf3c05f466_20180409120152-1.jpg" alt="MetaPos, the newly launched mPOS solution for Payment, is live.">
						</a>
					</div><!-- .ngc-media -->
					<div class="ngc-text">
						<div class="news-date"><!--
							<span class="d-date">01</span>
							<span class="d-month">Mar</span -->>
						</div><!-- .news-date -->
						<h3 class="ngc-title">
							<a href="#"><?=$row->Title?></a>
						</h3>
						<span class="ngc-news-tag">
							<span class="fa fa-tags"></span> News
						</span><!-- .ngc-news-tag -->
						<p class="ngc-text-preview"><?=$row->ContentText?></p>

					</div><!-- .ngc-text -->
				</div><!-- .news-item -->
				<?php
						}
					}
				?>
			</div><!-- .item -->
		</div><!-- .row -->
		<div class="n-align-center slideUpIn">
			<a href="<?php echo site_url("news");?>" class="btn btn-secondary n-no-margin">VIEW MORE NEWS</a>
		</div>
	</div><!-- .main-container -->
</section><!-- .section-padding -->



<?php
	echo $footer;
?>

<div class="nuke-overlay">
   <div class="nuke-modal-content">
       <img src="<?php echo base_url();?>assets/image/loading.gif" />
   </div><!--Buat item added-->
</div>


<script type='text/javascript'>
  Modernizr.addTest('firefox', typeof InstallTrigger !== 'undefined')
  Modernizr.addTest('ie', (navigator.appName == 'Microsoft Internet Explorer') || (Object.hasOwnProperty.call(window, "ActiveXObject") && !window.ActiveXObject))
</script>
