<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--<![endif]-->

<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="all,index,follow">
<meta name="googlebot" content="all,index,follow">
<meta name="revisit-after" content="2 days">
<meta name="rating" content="general">
<meta name="viewport" content="width=device-width, initial-scale=1">


<title>Our Features | MetaPos</title>

<meta name="keywords" content="Welcome to MetaPos">
<meta name="description" content="Welcome to MetaPos">
<meta property="og:title" content="Welcome to MetaPos">
<meta property="og:image" content="assets/image/metaposss.png">
<meta property="og:site_name" content="MetaPos">
<meta property="og:description" content="Welcome to MetaPos">


<link rel="shortcut icon" href="<?php echo base_url();?>assets/image/metaposss.png" type="image/x-icon">
<!-- Theme color for chrome, firefox and opera -->
<meta name="theme-color" content="#FFA812" />
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#FFA812">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="HandheldFriendly" content="true" />
<!-- Add to home screen and color theme -->

<?php
	echo $style;
	echo $script;
?>

</head>

<body>
<?php
	echo $header;
?>

<section class="static-banner">
	<img src="<?php echo base_url();?>assets/image/5acee7eeb23fa_20180412120030-1.jpg" alt="Product List">
</section><!-- .static-banner -->
<section class="breadcrumbs-section">
	<div class="main-container">
		<ul class="breadcrumbs">
			<li><a href="<?php echo base_url();?>">Home</a></li>
			<li>Our Features</li>
		</ul>
	</div><!-- .main-container -->
</section><!-- .breadcrumbs-section -->

<section class="section-padding">
	<div class="main-container">
		<div class="max-800 lr-auto n-align-center">
			<h1 class="ngc-maintitle slideDownIn">Features</h1>
			<p class="ngc-intro slideUpIn">Cash Register solutions by MetaPos are easier, simple, accurate and more cost-effective than traditional cash register solution. Our world-class products can facilitate your transaction better for your business</p>
		</div><!-- .max-800 -->

		<div class="row same-height">
			<?php
                if($produk != false){
                    foreach($produk->result() as $row){
            ?>
            <div class="grid-child n-540-1per2 n-768-1per3">
            	<div class="pc-item fadeIn">

            		<div class="ngc-media">
            			<a href="<?php echo site_url("ProdukController/detail/$row->Alias");?>">

                            <img src="<?=$row->Pic?>">
            			</a>
            		</div><!-- .ngc-media -->
            		<div class="ngc-text">
            			<h3 class="ngc-title"><?=$row->ProductName?></h3>
                            <p><?=$row->Description?></p>
                            <!-- <input type="hidden" id="detailId" name="detailId" value="<?php //echo $row->ProductId;?>">
 -->            			<a href="<?php echo site_url("ProdukController/detail/$row->Alias");?>" class="link-more">Learn More &rsaquo;</a>
            		</div><!-- .ngc-text -->
            	</div><!-- .pc-item -->
            </div><!-- .grid-child -->
            <?php
                                                }
                                            }
                                        ?>

         </div><!-- .row -->

		<br />


	</div><!-- .main-container -->
</section><!-- .section-padding -->

<script>
    $(document).ready(function(){
    	$(".main-nav-3, .mobile-nav-3").addClass("active");
    });
    $(window).load(function(){

    });
</script>


<?php
	echo $aboutfooter;
?>

<div class="nuke-overlay">
   <div class="nuke-modal-content">
       <img src="<?php echo base_url();?>assets/image/loading.gif" />
   </div><!--Buat item added-->
</div>
