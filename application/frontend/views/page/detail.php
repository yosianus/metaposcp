<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--<![endif]-->

<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="all,index,follow">
<meta name="googlebot" content="all,index,follow">
<meta name="revisit-after" content="2 days">
<meta name="rating" content="general">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Our Features | MetaPos</title>

<meta name="keywords" content="Welcome to MetaPos">
<meta name="description" content="Welcome to MetaPos">
<meta property="og:title" content="Welcome to MetaPos">
<meta property="og:image" content="assets/image/metaposss.png">
<meta property="og:site_name" content="MetaPos">
<meta property="og:description" content="Welcome to MetaPos">

<link rel="shortcut icon" href="<?php echo base_url();?>assets/image/metaposss.png" type="image/x-icon">
<!-- Theme color for chrome, firefox and opera -->
<meta name="theme-color" content="#FFA812" />
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#FFA812">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="HandheldFriendly" content="true" />
<!-- Add to home screen and color theme -->

<?php
	echo $style;
	echo $script;
?>

</head>

<body>

<?php
	echo $header;
	$name = "";
	$logo = "";
	$des = "";
	if($detail != false){
		$name = $detail->row()->ProductName;
		$logo = $detail->row()->Pic;
		$des = $detail->row()->Description;
	}
?>


<section class="static-banner">
	<img src="<?php echo base_url();?>assets/image/5acee84768278_20180412120159-1.jpg" alt="#">
</section><!-- .static-banner -->
<section class="breadcrumbs-section">
	<div class="main-container">
		<ul class="breadcrumbs">
			<li><a href="<?php echo base_url();?>">Home</a></li>
			<li><a href="<?php echo base_url('product');?>">Our Products</a></li>
			<li><?=$name?></li>
		</ul>
	</div><!-- .main-container -->
</section><!-- .breadcrumbs-section -->

<section class="section-padding">
	<div class="main-container">
		<div class="row">
		<?php
		if($detail != false){
		?>
			<div class="grid-child n-992-1per2 n-992-no-margin-bottom fadeIn">
				<div class="product-images-wrap">
                    <div class="slider-for-product">
                    	<div class="item">
                    		<div class="ngc-media">
                    				<img class="center" src="<?=$logo?>">

                    		</div><!-- .ngc-media -->
                    	</div><!-- .item -->
                    </div><!-- .slider-for-product -->

				</div><!-- .product-images-wrap -->
			</div><!-- .grid-child -->

        	<div class="grid-child n-992-1per2 n-no-margin-bottom slideRightIn">
				<h1 class="ngc-maintitle"><?=$name?></h1>
				<p class="ngc-intro"><?=$des?></p>
				<div class="nuke-wysiwyg">

				</div><!-- .nuke-wysiwyg -->
			</div><!-- .grid-child -->
		<?php
		}else{
		?>
		<div align="center">
			<p><h1>Data You Search Is Not Avalaible</h1></p>
		</div>
		<?php
		}
		?>
		</div><!-- .row -->
	</div><!-- .main-container -->

</section><!-- .section-padding -->

<section class="section-padding n-no-padding-top">
	<div class="main-container">
		<div class="max-800 lr-auto n-align-center">
			<h1 class="ngc-maintitle slideDownIn">Why MetaPos?</h1>
			<p class="ngc-intro slideUpIn">Cash Register solutions by MetaPos are easier, simple, accurate and more cost-effective than traditional cash register solution. Our world-class products can facilitate your transaction better for your business</p>
		</div><!-- .max-800 -->
		<div class="row same-height why-us">

            <div class="grid-child n-360-1per2 n-768-1per3">
            	<div class="wu-item slideRightIn">
                    <img src="<?php echo base_url();?>assets/image/low cost.png" alt="Secure">
                    <h2 class="ngc-title">Low Cost</h2>
                    <p>We provide you Cash Register Solution with Low Operation Cost to make your business grow effortlessly</p>
                </div><!-- .wu-item -->
            </div><!-- .grid-child -->
            <div class="grid-child n-360-1per2 n-768-1per3">
            	<div class="wu-item slideRightIn">
                    <img src="<?php echo base_url();?>assets/image/realtime dashboard.png" alt="Fast Update">
                    <h2 class="ngc-title">Realtime Dashboard</h2>
                    <p>One Click away to monitoring Store Performance with realtime dashboard and various report that we tailored for you. </p>
                </div><!-- .wu-item -->
            </div><!-- .grid-child -->
            <div class="grid-child n-360-1per2 n-768-1per3">
            	<div class="wu-item slideRightIn">
                    <img src="<?php echo base_url();?>assets/image/paymentmethod.png" alt="Grow Your Business">
                    <h2 class="ngc-title">Payment Method</h2>
                    <p>MetaPos provide various payment method to support your business.</p>
                </div><!-- .wu-item -->
            </div><!-- .grid-child -->

		</div><!-- .why-us -->

<!--
			<p class="ngc-intro slideUpIn">Accept card payments issued by any financial institution in whatever form International schemes or local debit card using a single low cost terminal. Integrates with existing app to get started quickly.</p>
		</div>
		<div class="row same-height why-us">

            <div class="grid-child n-360-1per2 n-768-1per3">
            	<div class="wu-item slideRightIn">
            		<img src="<?php //echo base_url();?>assets/image/laporan.png" alt="Laporan" style="width:70px;height:75px;">
            		<h2 class="ngc-title">Laporan & Dashboard Transaski realtime</h2>
            		<p>Reliable agent banking with point-to-point encryption to safe guard all transactions with Certified National Standard Indonesian Chip Card Specification (NSICCS)</p>
            	</div>
            </div>
            <div class="grid-child n-360-1per2 n-768-1per3">
            	<div class="wu-item slideRightIn">
            		<img src="<?php echo base_url();?>assets/image/metode.png" alt="Metode Pembayaran Variatif" style="width:70px;height:75px;">
            		<h2 class="ngc-title">Metode Pembayaran Variatif</h2>
            		<p>We are constantly improving with free updates that equip you with great new features to help you run your business more efficiently and effectively</p>
            	</div>
            </div>
            <div class="grid-child n-360-1per2 n-768-1per3">
            	<div class="wu-item slideRightIn">
            		<img src="<?php echo base_url();?>assets/image/persediaan.png" alt="Kontrol Persediaan" style="width:70px;height:75px;">
            		<h2 class="ngc-title">Kontrol Persediaan</h2>
            		<p>MetaPos is easy to use and easy to be implemented in your business , Support for Magnetic and Chip card</p>
            	</div>
            </div>
		</div>
-->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding has-bg has-overlay overlay-primary" style="background-image:url(assets/image/5acee84768278_20180412120159-1.jpg);">
	<div class="main-container">
		<div class="row">
			<div class="grid-child n-768-1per3 slideLeftIn n-no-margin-bottom">
				<h2 class="ngc-maintitle">MetaPOS Package</h2>
				<p class="ngc-intro">For best price contact us!</p>
			</div><!-- .grid-child -->

            <div class="grid-child n-768-2per3 n-no-margin-bottom">

                <div class="filter-child slideUpIn">
                	<a href="#" class="fc-toggle">Standard Feature</a>
                	<div class="fc-content">
                		<div class="package-skema-price">
                			<span class="n-primary price">IDR 150.000</span>
                			<span class="text">/ Merchant / bulan</span>
                		</div><!-- .package-skema-price -->
                		<div class="nuke-wysiwyg">
                			<p>Basic Feature + Loyalty Management + Inventory Management + MetaWaiter<br/>
                            1 User Owner<br/>
                            1 Store & 1 Spv User<br/>
                            2 Cashier User
                            </p>
                		</div><!-- .nuke-wysiwyg -->
                		<a href="<?php echo base_url('contact');?>" class="btn btn-secondary">Contact Us</a>
                	</div><!-- .fc-content -->
                </div><!-- .filter-child -->

                <div class="filter-child slideUpIn">
                	<a href="#" class="fc-toggle">MetaPOS + Mobey Bundling</a>
                	<div class="fc-content">
                		<div class="package-skema-price">
                			<span class="n-primary price">IDR 200.000</span>
                			<span class="text"> / bulan</span>
                			<span class="n-primary skema"><strong>+ Include MPOS Device Rental Fee</strong></span>
                		</div><!-- .package-skema-price -->
                		<div class="nuke-wysiwyg">
                			<p>Mobey : <br/>
                            MDR Debit NPG <br/>
                            On us : 0.15% Off us : 1 %<br/>
                            MDR Credit Card<br/>
                            On us : 2.5% Off us : 2.5 %<br/>
                            <i>**Negotiable</i></p>
                		</div><!-- .nuke-wysiwyg -->
                		<a href="<?php echo base_url('contact');?>" class="btn btn-secondary">Contact Us</a>
                	</div><!-- .fc-content -->
                </div><!-- .filter-child -->

                <div class="filter-child slideUpIn">
                	<a href="#" class="fc-toggle">Standard Feature (Contract)</a>
                	<div class="fc-content"><div class="package-skema-price">
                		<span class="n-primary price">IDR 1.800.000</span>
                		<span class="text">/ Merchant / tahun</span>
                		<span class="n-primary skema"><strong>+ Free 3 months usage</strong></span>
                	</div><!-- .package-skema-price -->
                	<div class="nuke-wysiwyg">
                		<p>150.000 x 12 = 1.800.000<br/>
                        <b>Free 3 months usage</b><br/>
                        1 Month Trial + 3 Month after Contract<br/>
                        Total Usage : 16 months
                        </p>
                	</div><!-- .nuke-wysiwyg -->
                	<a href="<?php echo base_url('contact');?>" class="btn btn-secondary">Contact Us</a>
                </div><!-- .fc-content -->
            </div><!-- .filter-child -->

            <div class="filter-child slideUpIn">
            	<a href="#" class="fc-toggle">Event Management</a>
            	<div class="fc-content">
            		<div class="package-skema-price">
            			<span class="n-primary price">IDR 100.000 - IDR 150.000</span>
            			<span class="text">/ Store / bulan</span>
            		</div><!-- .package-skema-price -->
            		<div class="nuke-wysiwyg">
            			<p>Centralized or Decentralized. Please contact us for more Info</p>
            		</div><!-- .nuke-wysiwyg -->
            		<a href="<?php echo base_url('contact');?>" class="btn btn-secondary">Contact Us</a>
            	</div><!-- .fc-content -->
            </div><!-- .filter-child -->

			</div><!-- .grid-child -->
		</div><!-- .row -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->


<script>
    $(document).ready(function(){
    	$(".main-nav-3, .mobile-nav-3").addClass("active");
    });
    $(window).load(function(){

    });
</script>


<?php
	echo $aboutfooter;
?>

<div class="nuke-overlay">
   <div class="nuke-modal-content">
       <img src="<?php echo base_url();?>assets/image/loading.gif" />
   </div><!--Buat item added-->
</div>

<script>
    $(document).ready(function(){

 			$(".lang_btn").click(function(e){
				$(".nuke-overlay, .nuke-modal-content").fadeIn();

				var itemlist = $(this).attr("name");
				$.post("http://mobey.id/change-language-parameter", {_token:"xs0BlrO91bujlPKM2yZzDJWmFY2fIjB7zZr3IEfe", "itemlist": itemlist },
				function(data){
					$(".nuke-overlay, .nuke-modal-content").fadeOut();
					location.reload();
				});

				e.preventDefault();
			});
    });
</script>
