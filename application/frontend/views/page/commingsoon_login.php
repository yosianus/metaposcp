<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>

<?php
	echo $script;
	echo $style;
	  ?>

</style>
</head>
<body>
<!-- <?php
	if($action == 'addnew'){
	    $col_jenis = $this->session->flashdata('jenis_usaha');
	    $col_usaha = $this->session->flashdata('nama_usaha');
	    $col_pemilik = $this->session->flashdata('nama_pemilik');
	    $col_kota = $this->session->flashdata('kota');
	    $col_email = $this->session->flashdata('email');
	    $col_alamat = $this->session->flashdata('alamat');
	    $col_telepon = $this->session->flashdata('nomor_telepon');
	    $col_id = $this->session->flashdata('ovo_id');
	    $col_deskripsi = $this->session->flashdata('deskripsi');
	    $buttonname = "_do_add_new";
	    $savesite = base_url('home/add_data');
	}
?> -->

<div class="popup-general popup-register">
	<div class="max-430 lr-auto">
		<div class="popup-header">
			<h1><img src="<?php echo base_url();?>assets/image/metaposss.png" alt="MetaPos"></h1>
            <p>Online Registration Is Coming Soon.</p>
			<p>Already have an account? <a href='https://metapos.vdicloud.id'>Login</a></p>
		</div><!-- .popup-header -->
	</div><!-- .max-430 -->
</div>

<script>
function myFunction() {
    alert("The form was submitted");
}
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('form#popuplogin_id').validate({
            rules: {
                email:{
                    required: true,
                    email:true
                },
                password:{
                    required: true
                }
            },
            messages: {
                email:{
                    required:"* This field can't be empty.",
                    email:"* Please enter valid email address"
                },
                password:{
                    required:"* This field can't be empty."
                }
            },
            submitHandler:function(form){
            //next submit
                form.submit();
            }
        });
    });
</script>

</div><!-- .max-430 -->
</div><!-- .popup-general -->
</body>
</html>
