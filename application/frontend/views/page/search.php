<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--<![endif]-->

<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="all,index,follow">
<meta name="googlebot" content="all,index,follow">
<meta name="revisit-after" content="2 days">
<meta name="rating" content="general">
<meta name="viewport" content="width=device-width, initial-scale=1">


<title>Welcome to MetaPos Site</title>

<meta name="keywords" content="Welcome to MetaPos">
<meta name="description" content="Welcome to MetaPos">
<meta property="og:title" content="Welcome to MetaPos">
<meta property="og:image" content="assets/image/metaposss.png">
<meta property="og:site_name" content="MetaPos">
<meta property="og:description" content="Welcome to MetaPos">


<link rel="shortcut icon" href="<?php echo base_url();?>assets/image/metaposss.png" type="image/x-icon">
<!-- Theme color for chrome, firefox and opera -->
<meta name="theme-color" content="#FFA812" />
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#FFA812">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="HandheldFriendly" content="true" />
<!-- Add to home screen and color theme -->

<?php
	echo $style;
	echo $script;
?>

</head>

<body>
<?php
	echo $header;
?>

<section class="breadcrumbs-section">
	<div class="main-container">
		<ul class="breadcrumbs">
			<li><a href="<?php echo base_url();?>">Home</a></li>
			<li>Search Result</li>
		</ul>
	</div><!-- .main-container -->
</section><!-- .breadcrumbs-section -->

<section class="section-padding">
	<div class="main-container">

		<!-- <h1 class="ngc-maintitle slideDownIn"><?php
	echo 'Results for ' . ($_GET[$key]);?></h1> -->
		<br />

        <div class="search-child slideLeftIn">
			<h2 class="ngc-maintitle n-primary">Our Features</h2>
			<div class="row same-height">
			<?php
			if($data_produk != false){
        		foreach($data_produk->result() as $row) {
           			$name = $row->ProductName;
            		$logo = $row->Pic;
            		$des = $row->Description;

    		?>
            <div class="grid-child n-540-1per2 n-768-1per3">
            	<div class="pc-item fadeIn">
            		<div class="ngc-media">
            			<a href="<?php echo site_url("ProdukController/detail");?>">

                            <img src="<?=$row->Pic?>">
            			</a>
            		</div><!-- .ngc-media -->
            		<div class="ngc-text">
            			<h3 class="ngc-title"><?=$row->ProductName?></h3>
                            <p><?=$row->Description?></p>
            			<a href="<?php echo site_url("ProdukController/detail/$row->ProductId");?>" class="link-more">Learn More &rsaquo;</a>
            		</div><!-- .ngc-text -->
            	</div><!-- .pc-item -->
            </div><!-- .grid-child -->
            <?php
            	}
			}
            ?>
        </div>
		</div><!-- .search-child -->

		<div class="search-child slideRightIn">
			<h2 class="ngc-maintitle n-primary">News &amp; Updates</h2>
			<div class="row same-height">
				<?php
				if($data_artikel != false){
        			foreach($data_artikel->result() as $row) {
           				$judul = $row->Title;
            			$des = $row->ContentText;
    			?>
				<div class="grid-child n-768-1per2">
					<div class="news-item">
						<div class="ngc-media">
						</div> <!--.ngc-media -->
						<div class="ngc-text">
							 <h3 class="ngc-title">
								<a href="#"><?=$row->Title?></a>
							</h3>
							<span class="ngc-news-tag">
								<span class="fa fa-tags"></span> News
							</span><!-- .ngc-news-tag -->
							<p class="ngc-text-preview"><?=$row->ContentText?></p>
							<a href="#" class="link-more">Read More &rsaquo;</a>
						</div><!-- .ngc-text -->
					</div><!-- .news-item -->
				</div><!-- .item -->
				<?php
					}
				}
            ?>
             </div><!-- .row -->
		</div><!-- .search-child -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->



<div class="nuke-overlay">
   <div class="nuke-modal-content">
       <img src="assets/image/loading.gif" />
   </div><!--Buat item added-->
</div>

<?php
	echo $aboutfooter;
?>

<script>
    $(document).ready(function(){

 			$(".lang_btn").click(function(e){
				$(".nuke-overlay, .nuke-modal-content").fadeIn();

				var itemlist = $(this).attr("name");
				$.post("http://mobey.id/change-language-parameter", {_token:"iCEDfSNh5YzbJ03q2Tm4S6XqGdSsJFMNkU5RDBED", "itemlist": itemlist },
				function(data){
					$(".nuke-overlay, .nuke-modal-content").fadeOut();
					location.reload();
				});

				e.preventDefault();
			});
    });
</script>
