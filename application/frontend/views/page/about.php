<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--<![endif]-->

<html class="no-js" lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="all,index,follow">
<meta name="googlebot" content="all,index,follow">
<meta name="revisit-after" content="2 days">
<meta name="rating" content="general">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>About MetaPos</title>

<meta name="keywords" content="Welcome to MetaPos">
<meta name="description" content="Welcome to MetaPos">
<meta property="og:title" content="Welcome to MetaPos">
<meta property="og:image" content="assets/image/metaposss.png">
<meta property="og:site_name" content="MetaPos">
<meta property="og:description" content="Welcome to MetaPos">


<link rel="shortcut icon" href="<?php echo base_url();?>assets/image/metaposss.png" type="image/x-icon">
<!-- Theme color for chrome, firefox and opera -->
<meta name="theme-color" content="#FFA812" />
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#FFA812">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="HandheldFriendly" content="true" />
<!-- Add to home screen and color theme -->

<?php
	echo $style;
	echo $script;
?>

</head>

<body>

<?php
	echo $header;
?>

<section class="static-banner">
	<img src="<?php echo base_url();?>assets/image/5ac3914a81a28_20180403213554-1.jpg" alt="MetaPos">
</section><!-- .static-banner -->
<section class="breadcrumbs-section">
	<div class="main-container">
		<ul class="breadcrumbs">
			<li><a href="<?php echo base_url();?>">Home</a></li>
			<li>About Us</li>
		</ul>
	</div><!-- .main-container -->
</section><!-- .breadcrumbs-section -->

<section class="section-padding n-no-padding-bottom">
	<div class="main-container">
		<div class="max-800 lr-auto n-align-center">
			<h1 class="ngc-maintitle slideDownIn">MetaPos</h1>
			<p class="ngc-intro slideUpIn">Easier, simple, accurate, and cheaper Cash Register</p>
		</div><!-- .max-800 -->

		<div class="nuke-wysiwyg slideUpIn">
			<div class="row">
				<div class="grid-child n-768-1per2 n-no-margin-bottom">
					<p>"METAPOS is a one stop Point of Sales solution that support your retail business and events by providing simplified selling process and well-organized store database, so that you can increase your store performance. Supported by VisioNet (PT Visionet Data Internasional) a nationwide IT Company Managed Services, which has been providing total services so that customer can focus in their own core business. Metapos also focus on customer needs as a basic to develop their products to be the best solution provider. "&nbsp;&nbsp;</p>
					<p>&nbsp;</p>
				</div><!-- .grid-child -->
				<div class="grid-child n-768-1per2 n-no-margin-bottom">
					<img src="<?php echo base_url();?>assets/image/metaposabout.png" alt="METAPOS">
					<!--
					<p>"METAPOS adalah solusi sistem Point of Sales yang mendukung usaha retail dan event Anda melalui penyediaan proses sistem penjualan yang simpel dan database yang rapi, sehingga Anda dapat meningkatkan kinerja toko Anda. Didukung oleh VisioNet (PT Visionet Data Internasional) sebagai perusahaan IT Managed Services, Metapos bertujuan untuk menjadi produk berkelas dunia dengan layanan yang prima untuk meningkatkan performa dan pengukuran penjualan. Metapos berfokus juga kepada kebutuhan konsumen sebagai basis pengembangan produk yang mampu menyediakan solusi terbaik untuk semua jenis bisnis."</p>
					-->
				</div><!-- .grid-child -->
			</div><!-- .row -->
		</div><!-- .nuke-wysiwyg -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding">
	<div class="main-container">
		<div class="index-about">
			<div class="ia-image n-1-hide n-768-show slideLeftInFlex">
                <img src="<?php echo base_url();?>assets/image/sunmi 3.jpg" alt="MetaPos Device">
			</div><!-- .ia-image -->
			<div class="content">
				<div class="row same-height why-us">
					<div class="grid-child n-360-1per2">
						<div class="wu-item slideRightIn">
							<img src="<?php echo base_url();?>assets/image/low cost.png" alt="Secure">
							<h2 class="ngc-title">Low Cost</h2>
							<p>We provide you Cash Register Solution with Low Operation Cost to make your business grow effortlessly</p>
<!--
							<img src="<?php echo base_url();?>assets/image/laporan.png" alt="Laporan" style="width:70px;height:75px;">
							<h2 class="ngc-title">Laporan & Dashboard Transaksi realtime</h2>
							<!-- <p>Reliable agent banking with point-to-point encryption to safe guard all transactions with Certified National Standard Indonesian Chip Card Specification (NSICCS)</p> -->
						</div><!-- .wu-item -->
					</div><!-- .grid-child -->
					<div class="grid-child n-360-1per2">
						<div class="wu-item slideRightIn">
							<img src="<?php echo base_url();?>assets/image/realtime dashboard.png" alt="Fast Update">
							<h2 class="ngc-title">Realtime Dashboard</h2>
							<p>One Click away to monitoring Store Performance with realtime dashboard and various report that we tailored for you. </p>
							<!--
							<img src="<?php echo base_url();?>assets/image/metode.png" alt="Metode Pembayaran Variatif" style="width:70px;height:75px;">
							<h2 class="ngc-title">Metode Pembayaran Variatif</h2>
							<!-- <p>We are constantly improving with free updates that equip you with great new features to help you run your business more efficiently and effectively.</p> -->
						</div><!-- .wu-item -->
					</div><!-- .grid-child -->
					<div class="grid-child n-360-1per2">
						<div class="wu-item slideRightIn">
							<img src="<?php echo base_url();?>assets/image/persediaan.png" alt="Easy">
							<h2 class="ngc-title">Inventory Management</h2>
							<p>We can help you to maintain your inventory, so you can track goods and always have a 'good to go' sales.</p>
							<!--
							<img src="<?php echo base_url();?>assets/image/persediaan.png" alt="Kontrol Persediaan" style="width:70px;height:75px;">
							<h2 class="ngc-title">Kontrol Persediaan</h2>
							<!-- <p>MetaPos is easy to use and easy to be implemented in your business , Support for Magnetic and Chip card</p> -->
						</div><!-- .wu-item -->
					</div><!-- .grid-child -->
					<div class="grid-child n-360-1per2">
						<div class="wu-item slideRightIn">
							<img src="<?php echo base_url();?>assets/image/program loyalitas.png" alt="Program loyalitas pelanggan" style="width:70px;height:75px;">
							<h2 class="ngc-title">Program loyalitas pelanggan</h2>
							<!-- <p>MetaPos is the point of sale that takes care of digital receipts, sales reports and provides valuable analytics so that makes it easier to run your business.</p> -->
						</div><!-- .wu-item -->
					</div><!-- .grid-child -->
					<div class="grid-child n-360-1per2">
						<div class="wu-item slideRightIn">
							<img src="<?php echo base_url();?>assets/image/settlement.png" alt="Settlement" style="width:70px;height:79px;">
							<h2 class="ngc-title">Settlement</h2>
							<!-- <p>MetaPos is easy to use and easy to be implemented in your business , Support for Magnetic and Chip card</p> -->
						</div><!-- .wu-item -->
					</div><!-- .grid-child -->
					<div class="grid-child n-360-1per2">
						<div class="wu-item slideRightIn">
							<img src="<?php echo base_url();?>assets/image/promo.png" alt="Promo & Diskon" style="width:70px;height:79px;">
							<h2 class="ngc-title">Promo & Diskon</h2>
							<!-- <p>MetaPos is the point of sale that takes care of digital receipts, sales reports and provides valuable analytics so that makes it easier to run your business.</p> -->
						</div><!-- .wu-item -->
					</div><!-- .grid-child -->
					<div class="grid-child n-360-1per2">
						<div class="wu-item slideRightIn">
							<img src="<?php echo base_url();?>assets/image/monitoring.png" alt="Monitoring" style="width:70px;height:78px;">
							<h2 class="ngc-title">Monitoring</h2>
							<!-- <p>MetaPos is easy to use and easy to be implemented in your business , Support for Magnetic and Chip card</p> -->
						</div><!-- .wu-item -->
					</div><!-- .grid-child -->
				</div><!-- .why-us -->
			</div><!-- .content -->
		</div><!-- .index-about -->
		<div class="about-ckeditor-2">
			<div class="row">
				<div class="grid-child n-768-1per3 n-768-no-margin-bottom">
					<div class="about-quote n-align-center n-768-align-left slideLeftIn">
						"Your POS Solution"
					</div><!-- .about-quote -->
				</div><!-- .grid-child -->
				<div class="grid-child n-768-2per3 n-no-margin-bottom">
					<div class="nuke-wysiwyg slideRightIn">
						<p><img alt="XXX" src="<?php echo base_url();?>assets/image/slide2.PNG" style="width: 800px; height: 375px;" /></p>
					</div><!-- .nuke-wysiwyg -->
				</div><!-- .grid-child -->
			</div><!-- .row -->
		</div><!-- .about-ckeditor-2 -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding n-no-padding-top">
	<div class="main-container">
		<div class="row">
			<div class="grid-child n-768-1per3 n-768-no-margin-bottom n-align-center n-768-align-left">
				<h2 class="ngc-maintitle slideLeftIn">Our Clients</h2>
				<p class="ngc-intro n-no-margin-bottom slideLeftIn">MetaPos has been trusted to make our clients' payment easier, safer, and cost-effective from various industry such as the bank, retail, department store, education, healthcare, food & beverages and ready to help you to begin your transaction evolution.</p>
			</div><!-- .grid-child -->
			<div class="grid-child n-768-2per3 n-no-margin-bottom">
				<div class="row medium-gutter same-height n-1-row-center n-768-row-left">
					<?php
                		if($klien != false){
                    		foreach($klien->result() as $row){
            		?>
					<div class="grid-child n-1-1per3 n-540-1per4 n-768-1per3 n-992-1per4 slideUpInFlex">
						<div class="client-item">
							<a href="#" target="_blank">

								<img src="<?=$row->ClientLogo?>">

							</a>
						</div> <!--.client-item -->
					</div> <!--.grid-child -->
					<?php
							}
						}
					?>

                </div><!-- .row -->
			</div><!-- .grid-child -->
		</div><!-- .row -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding has-bg has-overlay overlay-primary" style="background-image:url(assets/image/5a39ee49f05d6_20171220115953-1.jpg);">
	<div class="main-container">
        <div class="max-800 lr-auto n-align-center">
			<h2 class="ngc-maintitle n-primary slideDownIn">Client Testimonials</h2>
			<p class="ngc-intro slideUpIn">How MetaPos helps Businesses Grows.</p>
		</div><!-- .max-800 -->
		<div class="testi-carousel-wrap">
			<div class="testi-carousel">
				<?php
                	if($testimoni != false){
                    	foreach($testimoni->result() as $row){
            	?>
                <div class="item">
                	<div class="testi-item slideUpIn">
                		<div class="testi-text">

                			<p class="n-no-margin"><?=$row->Testimonial?></p>

                		</div><!-- .testi-text -->
                		<div class="testi-people">
                			<div class="ngc-media">
                				<img src="<?=$row->ClientLogo?>" alt="ovo">
                			</div><!-- .ngc-media -->
                			<h3 class="ngc-title"><?=$row->ClientName?></h3>

                			<!-- <span class="ngc-subtext">Business Development</span> -->
                		</div><!-- .testi-people -->
                	</div><!-- .testi-item slideUpIn -->
                </div><!-- .item -->
                <?php
                		}
                	}
                ?>

			</div><!-- .testi-carousel -->
		</div><!-- .testi-carousel-wrap -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding">
	<div class="main-container">
		<div class="max-970 lr-auto">

            <h2 class="ngc-maintitle slideDownIn n-align-center">About VisioNet</h2>

            <div class="nuke-wysiwyg slideUpIn">

				<div class="row">&nbsp;</div>
				<p>Visionet Data International is the first total IT Managed Services Company in Indonesia. Founded in 2006 as Visionet Internasional then diverted to Visionet Data Internasional.&nbsp; Offering a nation-wide coverage for more than 201 service points in 173 cities and 34 provinces in Indonesia. Having the ISO 9001:2008, ISO 27001:2013, we are focused on helping and supporting our customer in managing their business IT operation needs and ensuring the best performance as well as reliability and effectiveness in day-to-day basis so our customer can gladly focus on their core business. PT Visionet Data International offers an integrated leasing and managed service from data center operation service for various business applications to day-to-day IT operation in Indonesia.&nbsp;</p>
			</div><!-- .nuke-wysiwyg -->
        	<br />
			<div class="ngc-video-container">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/1r3cXVh2n1A?rel=0" frameborder="0" allowfullscreen></iframe>
			</div><!-- .ngc-video-container -->

		</div><!-- .max-800 -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section>
	<div class="main-container">
		<div class="max-800 lr-auto n-align-center">
			<h2 class="ngc-maintitle slideDownIn">Our Products</h2>
			<br />
		</div><!-- .max-800 -->
	<div class="row same-height">
			<?php
                if($produkdetail != false){
                    foreach($produkdetail->result() as $row){
            ?>
            <div class="grid-child n-540-1per2 n-768-1per3">
            	<div class="pc-item fadeIn">

            		<div class="ngc-media">
            			<a href="<?php echo site_url("ProdukController/detail/$row->Alias");?>">

                            <img class="center" src="<?=$row->Pic?>">
            			</a>
            		</div><!-- .ngc-media -->
            		<div class="ngc-text">
            			<h3 class="ngc-title"><?=$row->ProductName?></h3>
                            <p><?=$row->Description?></p>
            			<a href="<?php echo site_url("ProdukController/detail/$row->Alias");?>" class="link-more">Learn More &rsaquo;</a>
            		</div><!-- .ngc-text -->
            	</div><!-- .pc-item -->
            </div><!-- .grid-child -->
            <?php
                                                }
                                            }
                                        ?>
                                    </div>
                                    <div class="n-align-center slideUpIn">
			<a href="<?php echo site_url("product");?>" class="btn btn-secondary n-no-margin">VIEW ALL METAPOS FEATURES</a>
		</div>
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<script>
    $(document).ready(function(){
    	$(".main-nav-2, .mobile-nav-2").addClass("active");
    });
    $(window).load(function(){

    });
</script>


<?php
	echo $aboutfooter;
?>

<div class="nuke-overlay">
   <div class="nuke-modal-content">
       <img src="<?php echo base_url();?>assets/image/loading.gif" />
   </div><!--Buat item added-->
</div>

<script>
    $(document).ready(function(){

 			$(".lang_btn").click(function(e){
				$(".nuke-overlay, .nuke-modal-content").fadeIn();

				var itemlist = $(this).attr("name");
				$.post("http://mobey.id/change-language-parameter", {_token:"xs0BlrO91bujlPKM2yZzDJWmFY2fIjB7zZr3IEfe", "itemlist": itemlist },
				function(data){
					$(".nuke-overlay, .nuke-modal-content").fadeOut();
					location.reload();
				});

				e.preventDefault();
			});
    });
</script>
