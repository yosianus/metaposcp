<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--<![endif]-->

<html class="no-js" lang="en"> 
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="all,index,follow">
<meta name="googlebot" content="all,index,follow">
<meta name="revisit-after" content="2 days">
<meta name="author" content="Nukegraphic Indonesia">
<meta name="rating" content="general">
<meta name="viewport" content="width=device-width, initial-scale=1">

  
<title>Terms and Conditions | MetaPos</title>

<meta name="keywords" content="Welcome to MetaPos">
<meta name="description" content="Welcome to MetaPos">
<meta property="og:title" content="Welcome to MetaPos">
<meta property="og:image" content="assets/image/metaposss.png">
<meta property="og:site_name" content="MetaPos">
<meta property="og:description" content="Welcome to MetaPos">

<!-- <meta property="og:url" content="http://mobey.id/home"> -->

<link rel="shortcut icon" href="assets/image/metaposss.png" type="image/x-icon">
<!-- Theme color for chrome, firefox and opera -->
<meta name="theme-color" content="#FFA812" />
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#FFA812">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="HandheldFriendly" content="true" />
<!-- Add to home screen and color theme -->

<?php
	echo $style;
	echo $script;
?>

</head>

<body>
<?php
	echo $header;
?>


<section class="static-banner">
	<img src="<?php echo base_url();?>assets/image/5acee7d550ecc_20180412120005-1.jpg" alt="Terms and Conditions">
</section><!-- .static-banner -->
<section class="breadcrumbs-section">
	<div class="main-container">
		<ul class="breadcrumbs">
			<li><a href="<?php echo base_url();?>">Home</a></li>
			<li>Terms and Conditions</li>
		</ul>
	</div><!-- .main-container -->
</section><!-- .breadcrumbs-section -->

<section class="section-padding">
	<div class="main-container">
		<h1 class="ngc-maintitle slideRightIn">Terms and Conditions</h1>
		<div class="section-pages">
			<article class="ngc-pages-main">
				<?php
					foreach($termlink->result() as $row) {
						$content = $row->ContentText;
					}
				?>
				<p class="ngc-intro slideRightIn"><?=$content?></p>
				
                <div class="nuke-wysiwyg slideRightIn">
					
				</div><!-- .nuke-wysiwyg -->
				
                                
			</article><!-- .ngc-pages-main -->
			
            <aside class="ngc-pages-aside">
	<div class="aside-content">
		<h3 class="n-992-hide hidden-title">Menu</h3>
		<a href="#" class="close-pages-menu n-992-hide">
			<span class="fa fa-circle"></span>
			<span class="fal fa-times"></span>
		</a>
		<div class="aside-menu slideLeftIn">
			<ul>
				<!-- <li class="aside-1"><a href="http://mobey.id/faq">FAQ</a></li> -->
				<li class="aside-2"><a href="<?php echo site_url("privacy");?>">Privacy Policy</a></li>
				<li class="aside-3"><a href="<?php echo site_url("terms");?>">Terms and Conditions</a></li>
			</ul>
		</div><!-- .aside-menu -->
	</div><!-- .aside-content -->
</aside><!-- .ngc-pages-aside -->            
		</div><!-- .section-pages -->
	</div><!-- .main-container -->
</section>

<a href="#" class="pages-menu-toggle n-992-hide"><span class="fa fa-filter"></span>OPEN MENU</a>

<script>
    $(document).ready(function(){
    	$(".aside-3 a").addClass("active");
    });
    $(window).load(function(){
    	
    });
</script>


<div class="nuke-overlay">
   <div class="nuke-modal-content">
       <img src="<?php echo base_url();?>assets/image/loading.gif" />
   </div><!--Buat item added-->
</div>

<?php
	echo $aboutfooter;
?>

<script>
    $(document).ready(function(){
	
 			$(".lang_btn").click(function(e){
				$(".nuke-overlay, .nuke-modal-content").fadeIn();
				
				var itemlist = $(this).attr("name");
				$.post("http://mobey.id/change-language-parameter", {_token:"iCEDfSNh5YzbJ03q2Tm4S6XqGdSsJFMNkU5RDBED", "itemlist": itemlist },
				function(data){	
					$(".nuke-overlay, .nuke-modal-content").fadeOut();
					location.reload();					
				});		
				
				e.preventDefault();	
			});	
    });
</script>