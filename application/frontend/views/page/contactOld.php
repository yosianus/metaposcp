<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--<![endif]-->

<html class="no-js" lang="en"> 
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="all,index,follow">
<meta name="googlebot" content="all,index,follow">
<meta name="revisit-after" content="2 days">
<meta name="author" content="Nukegraphic Indonesia">
<meta name="rating" content="general">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Contact Us | MetaPOS</title>

<meta name="keywords" content="Welcome to MetaPos">
<meta name="description" content="Welcome to MetaPos">
<meta property="og:title" content="Welcome to MetaPos">
<meta property="og:image" content="assets/image/metaposss.png">
<meta property="og:site_name" content="MetaPos">
<meta property="og:description" content="Welcome to MetaPos">

<!-- <meta property="og:url" content="http://mobey.id/home"> -->

<link rel="shortcut icon" href="<?php echo base_url();?>assets/image/metaposss.png" type="image/x-icon">
<!-- Theme color for chrome, firefox and opera -->
<meta name="theme-color" content="#FFA812" />
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#FFA812">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="HandheldFriendly" content="true" />
<!-- Add to home screen and color theme -->

<?php
	echo $style;
	echo $script;
?>

</head>

<body>
<?php
	echo $header;
?>

<?php
	if($action == 'addnew'){
	    $col_name = $this->session->flashdata('name');
	    $col_email = $this->session->flashdata('email');
	    $col_phone = $this->session->flashdata('phone');
	    $col_pesan = $this->session->flashdata('pesan_member');
	    $buttonname = "_do_add_new";
	    $savesite = base_url('contact/add_data');
	    // $langaction = "Add New";
	}
?>

<section class="static-banner">
	<img src="<?php echo base_url();?>assets/image/5acee78e4b22d_20180412115854-1.jpg" alt="Contact Us">
</section><!-- .static-banner -->
<section class="breadcrumbs-section">
	<div class="main-container">
		<ul class="breadcrumbs">
			<li><a href="<?php echo base_url();?>">Home</a></li>
			<li>Contact Us</li>
		</ul>
	</div><!-- .main-container -->
</section><!-- .breadcrumbs-section -->

<section class="section-padding">
	<div class="main-container">
		<div class="max-800 lr-auto n-align-center">
			<h1 class="ngc-maintitle slideDownIn">Contact Us</h1>
			<p class="ngc-intro slideUpIn">Our team is ready to hear all of your problem and opinion</p>
		</div><!-- .max-800 -->
		
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding n-no-padding-top">
	<div class="main-container">
		<div class="row">
			<div class="grid-child n-992-2per3">
				<div class="contact-form slideLeftIn">

                <form class="contact-form" method="post" action="#" name="formsubmit" onsubmit="myFunction()">
                    <!-- <input type="hidden" name="_token" value="xs0BlrO91bujlPKM2yZzDJWmFY2fIjB7zZr3IEfe"> -->	
                        
					<div class="row same-height medium-gutter">
						<div class="grid-child n-540-1per2 n-no-margin-bottom">
							<div class="form-group">
								<label>Name</label>
								<div class="input-wrap has-icon">
									<input type="text" placeholder="Your name" name="name" maxlength="200" required>
									<span class="fa-wrapper"><span class="fa fa-user icon"></span></span>
								</div><!-- .input-wrap -->
							</div><!-- .form-group -->
						</div><!-- .grid-child -->
						<div class="grid-child n-540-1per2 n-no-margin-bottom">
							<div class="form-group">
								<label>Email</label>
								<div class="input-wrap has-icon">
									<input type="email" placeholder="Your email address" name="email" maxlength="200" required>
									<span class="fa-wrapper"><span class="fal fa-envelope icon"></span></span>
								</div><!-- .input-wrap -->
							</div><!-- .form-group -->
						</div><!-- .grid-child -->
						<div class="grid-child n-540-1per2 n-no-margin-bottom">
							<div class="form-group">
								<label>Phone</label>
								<div class="input-wrap has-icon">
									<input type="tel" placeholder="Your phone number" name="phone" maxlength="20" required>
									<span class="fa-wrapper"><span class="fa fa-phone icon"></span></span>
								</div><!-- .input-wrap -->
							</div><!-- .form-group -->
						</div><!-- .grid-child -->
						<div class="grid-child n-no-margin-bottom">
							<div class="form-group">
								<label>Message</label>
								<div class="input-wrap has-icon">
									<textarea rows="4" placeholder="Type your message here" name="pesan_member" required></textarea>
									<span class="fa-wrapper"><span class="fa fa-pencil icon"></span></span>
								</div><!-- .input-wrap -->
							</div><!-- .form-group -->
						</div><!-- .grid-child -->
					</div><!-- .row -->
					<div class="row medium-gutter">
						
						<div class="grid-child n-768-1per2 n-no-margin-bottom">
							<button class="btn btn-secondary n-no-margin n-1-1per1" type="submit" name="<?=$buttonname?>" value="submit">SUBMIT</button>
						</div><!-- .grid-child -->
					</div><!-- .row -->
                   
                   </form>
				
                </div><!-- .contact-form -->
			</div><!-- .grid-child -->

<script>
function myFunction() {
    alert("The form was submitted");
}
</script>

			<div class="grid-child n-992-1per3">
				<div class="contact-side-info slideRightIn">
					<?php

					?>
					<h3 class="ngc-title">MetaPos</h3>
					<ul class="index-contact-list aside">
						<li>
							<span class="icon"><span class="fa fa-map-marker-alt awesome-icon"></span></span>
							<span class="text"><?=$officeaddress->row()->ContentText?></span>
						</li>
						<li>
							<span class="icon"><span class="fa fa-fax awesome-icon"></span></span>
							<span class="text"><?=$companyfax->row()->ContentText?></span>
						</li>
						<li>
							<span class="icon"><span class="fa fa-phone awesome-icon"></span></span>
							<span class="text"><?=$companyphone->row()->ContentText?></span>
						</li>
						<li>
							<span class="icon"><span class="fal fa-envelope awesome-icon"></span></span>
							<span class="text"><?=$salesemail->row()->ContentText?></span>
						</li>
					</ul><!-- .index-contact-list -->
				</div><!-- .contact-side-info -->
			</div><!-- .grid-child -->
            
		</div><!-- .row -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<div class="contact-map slideUpIn">
	<div id="map"></div>
</div><!-- .contact-map -->


<script>
    $(document).ready(function(){
    	$(".main-nav-5, .mobile-nav-5").addClass("active");

    	map = new GMaps({
			el: '#map',
			lat: -6.244580, 
			lng: 106.628348,
			scrollwheel: false,
			zoom:15,
			styles: 
			    [
				    {
				        "featureType": "administrative.locality",
				        "elementType": "all",
				        "stylers": [
				            {
				                "hue": "#c79c60"
				            },
				            {
				                "saturation": 7
				            },
				            {
				                "lightness": 19
				            },
				            {
				                "visibility": "on"
				            }
				        ]
				    },
				    {
				        "featureType": "landscape",
				        "elementType": "all",
				        "stylers": [
				            {
				                "hue": "#ffffff"
				            },
				            {
				                "saturation": -100
				            },
				            {
				                "lightness": 100
				            },
				            {
				                "visibility": "simplified"
				            }
				        ]
				    },
				    {
				        "featureType": "poi",
				        "elementType": "all",
				        "stylers": [
				            {
				                "hue": "#ffffff"
				            },
				            {
				                "saturation": -100
				            },
				            {
				                "lightness": 100
				            },
				            {
				                "visibility": "off"
				            }
				        ]
				    },
				    {
				        "featureType": "road",
				        "elementType": "geometry",
				        "stylers": [
				            {
				                "hue": "#c79c60"
				            },
				            {
				                "saturation": -52
				            },
				            {
				                "lightness": -10
				            },
				            {
				                "visibility": "simplified"
				            }
				        ]
				    },
				    {
				        "featureType": "road",
				        "elementType": "labels",
				        "stylers": [
				            {
				                "hue": "#c79c60"
				            },
				            {
				                "saturation": -93
				            },
				            {
				                "lightness": 31
				            },
				            {
				                "visibility": "on"
				            }
				        ]
				    },
				    {
				        "featureType": "road.arterial",
				        "elementType": "labels",
				        "stylers": [
				            {
				                "hue": "#c79c60"
				            },
				            {
				                "saturation": -93
				            },
				            {
				                "lightness": -2
				            },
				            {
				                "visibility": "simplified"
				            }
				        ]
				    },
				    {
				        "featureType": "road.local",
				        "elementType": "geometry",
				        "stylers": [
				            {
				                "hue": "#c79c60"
				            },
				            {
				                "saturation": -52
				            },
				            {
				                "lightness": -10
				            },
				            {
				                "visibility": "simplified"
				            }
				        ]
				    },
				    {
				        "featureType": "transit",
				        "elementType": "all",
				        "stylers": [
				            {
				                "hue": "#c79c60"
				            },
				            {
				                "saturation": 10
				            },
				            {
				                "lightness": 69
				            },
				            {
				                "visibility": "on"
				            }
				        ]
				    },
				    {
				        "featureType": "water",
				        "elementType": "all",
				        "stylers": [
				            {
				                "hue": "#c79c60"
				            },
				            {
				                "saturation": -78
				            },
				            {
				                "lightness": 67
				            },
				            {
				                "visibility": "simplified"
				            }
				        ]
				    }
				]
			});
			
					map.addMarker({
										lat:   -6.219702, 
										lng:   106.622357,
										icon:  "http://mobey.id/images/icon/icon-marker-main.png",
										title: "Main Office",
										infoWindow: {
											content: "<div class='infowindow'><h1>MetaPOS</h1><ul class='marker-data'><li>Jalan Bulevar Gajah Mada No.2120,Panunggangan Barat, Kota Tangerang,Banten 15138</li><li><strong>Phone</strong><br /><a href='tel: 021-55777678'> 021-55777678</a></li><li><strong>Fax</strong><br /><a href='tel: 021-55777677'> 021-55777677</a></li><li><strong>Email</strong><br /><a href='mailto:vidia@visionet.co.id'>vidia@visionet.co.id</a></li></ul></div>"
										}
			
		});
    });
    $(window).load(function(){
    	
    });
</script>


<?php
	echo $aboutfooter;
?>

<div class="nuke-overlay">
   <div class="nuke-modal-content">
       <img src="<?php echo base_url();?>assets/image/loading.gif" />
   </div><!--Buat item added-->
</div>

<script>
    $(document).ready(function(){
	
 			$(".lang_btn").click(function(e){
				$(".nuke-overlay, .nuke-modal-content").fadeIn();
				
				var itemlist = $(this).attr("name");
				$.post("http://mobey.id/change-language-parameter", {_token:"xs0BlrO91bujlPKM2yZzDJWmFY2fIjB7zZr3IEfe", "itemlist": itemlist },
				function(data){	
					$(".nuke-overlay, .nuke-modal-content").fadeOut();
					location.reload();					
				});		
				
				e.preventDefault();	
			});	
    });
</script>