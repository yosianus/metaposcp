<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--<![endif]-->

<html class="no-js" lang="en"> 
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="all,index,follow">
<meta name="googlebot" content="all,index,follow">
<meta name="revisit-after" content="2 days">
<meta name="author" content="Nukegraphic Indonesia">
<meta name="rating" content="general">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Our Products | MetaPos</title>

<meta name="keywords" content="Welcome to MetaPos">
<meta name="description" content="Welcome to MetaPos">
<meta property="og:title" content="Welcome to MetaPos">
<meta property="og:image" content="assets/image/metaposss.png">
<meta property="og:site_name" content="MetaPos">
<meta property="og:description" content="Welcome to MetaPos">
<meta property="og:url" content="http://mobey.id/home">

<link rel="shortcut icon" href="assets/image/metaposss.png" type="image/x-icon">
<!-- Theme color for chrome, firefox and opera -->
<meta name="theme-color" content="#FFA812" />
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#FFA812">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="HandheldFriendly" content="true" />
<!-- Add to home screen and color theme -->

<?php
	echo $style;
	echo $script;
?>

</head>

<body>

<?php
	echo $header;
?>

<section class="static-banner">
	<img src="http://mobey.id/uploads/5acee8720cccf_20180412120242-1.jpg" alt="Merchant Services">
</section><!-- .static-banner -->
<section class="breadcrumbs-section">
	<div class="main-container">
		<ul class="breadcrumbs">
			<li><a href="<?php echo site_url("metaposController/home");?>">Home</a></li>
			<li><a href="<?php echo site_url("metaposController/produk");?>">Our Products</a></li>
			<li>Merchant Services</li>
		</ul>
	</div><!-- .main-container -->
</section><!-- .breadcrumbs-section -->

<section class="section-padding">
	<div class="main-container">
		<div class="row">
			<div class="grid-child n-992-1per2 n-992-no-margin-bottom fadeIn">
				<div class="product-images-wrap">
					
                    <div class="slider-for-product">
                    	<div class="item">
                    		<div class="ngc-media">
                    			<a href="http://mobey.id/uploads/5aaa0faaf34af_20180315131610-1.jpg" data-fancybox="gallery">
                    				<img src="http://mobey.id/uploads/5aaa0faaf34af_20180315131610-1.jpg" alt="Merchant Services">
                    			</a>
                    		</div><!-- .ngc-media -->
                    	</div><!-- .item -->
                    	<div class="item">
                    		<div class="ngc-media">
                    			<a href="http://mobey.id/uploads/5aaa10267dea0_20180315131814-1.jpg" data-fancybox="gallery">
                    				<img src="http://mobey.id/uploads/5aaa10267dea0_20180315131814-1.jpg" alt="Merchant Services">
                    			</a>
                    		</div><!-- .ngc-media -->
                    	</div><!-- .item -->
                    	<div class="item">
                    		<div class="ngc-media">
                    			<a href="http://mobey.id/uploads/5aaa10f2c0ba8_20180315132138-1.jpg" data-fancybox="gallery">
                    				<img src="http://mobey.id/uploads/5aaa10f2c0ba8_20180315132138-1.jpg" alt="Merchant Services">
                    			</a>
                    		</div><!-- .ngc-media -->
                    	</div><!-- .item -->
                    </div><!-- .slider-for-product -->
                    <div class="slider-product-nav-wrap">
                    	<div class="slider-product-nav">
                    		<div class="item">
                    			<div class="ngc-media">
                    				<img src="http://mobey.id/uploads/5aaa0faaf34af_20180315131610-1.jpg" alt="Merchant Services">
                    			</div><!-- .ngc-media -->
                    		</div><!-- .item -->
                    		<div class="item">
                    			<div class="ngc-media">
                    				<img src="http://mobey.id/uploads/5aaa10267dea0_20180315131814-1.jpg" alt="Merchant Services">
                    			</div><!-- .ngc-media -->
                    		</div><!-- .item -->
                    		<div class="item">
                    			<div class="ngc-media">
                    				<img src="http://mobey.id/uploads/5aaa10f2c0ba8_20180315132138-1.jpg" alt="Merchant Services">
                    			</div><!-- .ngc-media -->
                    		</div><!-- .item -->
                    	</div><!-- .slider-product-nav -->
                    </div><!-- .slider-product-nav-wrap -->
                    
				</div><!-- .product-images-wrap -->
			</div><!-- .grid-child -->
		
        	<div class="grid-child n-992-1per2 n-no-margin-bottom slideRightIn">
				<h1 class="ngc-maintitle">Merchant Services</h1>
				<p class="ngc-intro">Spreading across 34 Provinces in 181 Cities and more than 200 Service points through out Indonesia Our People (more than 1,500 engineers) are ready to assist you anytime </p>
				<div class="nuke-wysiwyg">
					<p><strong>Certified ISO 9001:2008</strong> for Operation &amp; Maintenance Services in three main area&nbsp;:</p>
					<ul>
						<li>
							<p>Electronic Draft Capture (EDC)</p>
						</li>
						<li>
							<p>Desktop, Server, Network (DSN)</p>
						</li>
						<li>
							<p>Information Technology (IT)</p>
						</li>
					</ul>
					<p><strong>Certified ISO 9001:2008</strong> for Quality Management System&nbsp;in Contact Center Services.&nbsp;</p>
					<p><strong>Certified ISO 27001:2013</strong> for Information Security Management System in Data Center Management.</p>
				</div><!-- .nuke-wysiwyg -->
			</div><!-- .grid-child -->
            
		</div><!-- .row -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding n-no-padding-top">
	<div class="main-container">
		<div class="max-800 lr-auto n-align-center">
			<h1 class="ngc-maintitle slideDownIn">Why Need Merchant Service?</h1>
			<p class="ngc-intro slideUpIn">Merchant IT Services from PT Visionet Data Internasional a leading provider of IT managed services for the financial industry. A full range of store, office and restaurant payment solutions. Also assist in payment device managed services starting from providing the hardware, operational managed services (preventive maintenance, case management), back office support to deployment/roll out with real time reporting and systematized operation supported by current technology, best practice and experiences to ensure transparency, speed and data accuracy.</p>
		</div><!-- .max-800 -->
		<div class="row same-height why-us">
			
            <div class="grid-child n-360-1per2 n-768-1per3">
            	<div class="wu-item slideRightIn">
            		<img src="http://mobey.id/uploads/5a39dbce0c2db_20171220104102-1.png" alt="Secure">
            		<h2 class="ngc-title">Secure</h2>
            		<p>Reliable agent banking with point-to-point encryption to safe guard all transactions with Certified National Standard Indonesian Chip Card Specification (NSICCS)</p>
            	</div><!-- .wu-item -->
            </div><!-- .grid-child -->
            <div class="grid-child n-360-1per2 n-768-1per3">
            	<div class="wu-item slideRightIn">
            		<img src="http://mobey.id/uploads/5a39dbd81ab92_20171220104112-1.png" alt="Fast Update">
            		<h2 class="ngc-title">Fast Update</h2>
            		<p>We are constantly improving with free updates that equip you with great new features to help you run your business more efficiently and effectively</p>
            	</div><!-- .wu-item -->
            </div><!-- .grid-child -->
            <div class="grid-child n-360-1per2 n-768-1per3">
            	<div class="wu-item slideRightIn">
            		<img src="http://mobey.id/uploads/5a39dbe240f37_20171220104122-1.png" alt="Easy">
            		<h2 class="ngc-title">Easy</h2>
            		<p>Mobey is easy to use and easy to be implemented in your business , Support for Magnetic and Chip card</p>
            	</div><!-- .wu-item -->
            </div><!-- .grid-child -->

		</div><!-- .why-us -->
		
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding has-bg has-overlay overlay-primary" style="background-image:url(http://mobey.id/uploads/5acee8720cccf_20180412120242-1.jpg);">
	<div class="main-container">
		<div class="row">
			<div class="grid-child n-768-1per3 slideLeftIn n-no-margin-bottom">
				<h2 class="ngc-maintitle">Mobey Package</h2>
				<p class="ngc-intro">For best price contact us!</p>
			</div><!-- .grid-child -->
			
            <div class="grid-child n-768-2per3 n-no-margin-bottom">

			</div><!-- .grid-child -->
		</div><!-- .row -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding n-no-padding-bottom">
	<div class="main-container">
		<div class="max-800 lr-auto n-align-center">
			<h2 class="ngc-maintitle slideDownIn">Related Products</h2>
			<br />
		</div><!-- .max-800 -->
		<div class="product-carousel-wrap">
			<div class="product-carousel">
				<div class="item">
					<div class="pc-item fadeIn">
						<div class="ngc-media">
							<a href="http://mobey.id/products-detail/d180-mpos/12">
								<img src="http://mobey.id/uploads/5aaa018143f54_20180315121545-1.jpg" alt="D180 MPOS">
							</a>
						</div><!-- .ngc-media -->
						<div class="ngc-text">
							<h3 class="ngc-title">D180 MPOS</h3>
							<p>Pocket-sized &amp; touch-press key design&nbsp;|&nbsp;iOS&nbsp;&amp; Android compatible&nb &hellip;</p>
							<a href="http://mobey.id/products-detail/d180-mpos/12" class="link-more">Learn More &rsaquo;</a>
						</div><!-- .ngc-text -->
					</div><!-- .pc-item -->
				</div><!-- .item -->
				<div class="item">
					<div class="pc-item fadeIn">
						<div class="ngc-media">
							<a href="http://mobey.id/products-detail/a920-paydroid/13">
								<img src="http://mobey.id/uploads/5aaa03822cdb7_20180315122418-1.jpg" alt="A920 PayDroid">
							</a>
						</div><!-- .ngc-media -->
						<div class="ngc-text">
							<h3 class="ngc-title">A920 PayDroid</h3>
							<p>Open Android OS&nbsp;|&nbsp;Large colour touch screen&nbsp;|&nbsp;Double injection&nbsp;|& &hellip;</p>
							<a href="http://mobey.id/products-detail/a920-paydroid/13" class="link-more">Learn More &rsaquo;</a>
						</div><!-- .ngc-text -->
					</div><!-- .pc-item -->
				</div><!-- .item -->
			</div><!-- .product-carousel -->
		</div><!-- .product-carousel-wrap -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<script>
    $(document).ready(function(){
    	$(".main-nav-3, .mobile-nav-3").addClass("active");
    });
    $(window).load(function(){
    	
    });
</script>

<section class="section-padding n-1200-no-padding-bottom slideUpIn">
	<div class="main-container">
		<div class="newsletter-area">
			<div class="ngc-text">
				<h2 class="ngc-title n-primary">Subscribe to Our Newsletter</h2>
				<p>No spam, only promotions.</p>
			</div><!-- .ngc-text -->
            <form action="http://mobey.id/do-subscribe-newsletter" method="post" id="newsletter_form">
            <input type="hidden" name="_token" value="xs0BlrO91bujlPKM2yZzDJWmFY2fIjB7zZr3IEfe">		
            <div class="newsletter-form">
				<input type="email" placeholder="Your email address .." class="input-text" name="email" maxlength="200">
				<button class="submit-btn">
					<span class="fal fa-envelope"></span>
					<span class="text n-1-hide n-992-show">SUBSCRIBE</span>
				</button>
			</div><!-- .newsletter-form -->
            </form>
		</div><!-- .newsletter-area -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<?php
	echo $footer;
?>

<div class="nuke-overlay">
   <div class="nuke-modal-content">
       <img src="http://mobey.id/images/loading.gif" />
   </div><!--Buat item added-->
</div>

<script>
    $(document).ready(function(){
	
 			$(".lang_btn").click(function(e){
				$(".nuke-overlay, .nuke-modal-content").fadeIn();
				
				var itemlist = $(this).attr("name");
				$.post("http://mobey.id/change-language-parameter", {_token:"xs0BlrO91bujlPKM2yZzDJWmFY2fIjB7zZr3IEfe", "itemlist": itemlist },
				function(data){	
					$(".nuke-overlay, .nuke-modal-content").fadeOut();
					location.reload();					
				});		
				
				e.preventDefault();	
			});	
    });
</script>