<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--<![endif]-->

<html class="no-js" lang="en"> 
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="all,index,follow">
<meta name="googlebot" content="all,index,follow">
<meta name="revisit-after" content="2 days">
<meta name="author" content="Nukegraphic Indonesia">
<meta name="rating" content="general">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Our Products | MetaPos</title>

<meta name="keywords" content="Welcome to MetaPos">
<meta name="description" content="Welcome to MetaPos">
<meta property="og:title" content="Welcome to MetaPos">
<meta property="og:image" content="assets/image/metaposss.png">
<meta property="og:site_name" content="MetaPos">
<meta property="og:description" content="Welcome to MetaPos">
<meta property="og:url" content="http://mobey.id/home">

<link rel="shortcut icon" href="assets/image/metaposss.png" type="image/x-icon">
<!-- Theme color for chrome, firefox and opera -->
<meta name="theme-color" content="#FFA812" />
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#FFA812">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="HandheldFriendly" content="true" />
<!-- Add to home screen and color theme -->

<?php
	echo $style;
	echo $script;
?>

</head>

<body>

<?php
	echo $header;
?>

<section class="static-banner">
	<img src="http://mobey.id/uploads/5acee84768278_20180412120159-1.jpg" alt="D180 MPOS">
</section><!-- .static-banner -->
<section class="breadcrumbs-section">
	<div class="main-container">
		<ul class="breadcrumbs">
			<li><a href="<?php echo site_url("metaposController/home");?>">Home</a></li>
			<li><a href="<?php echo site_url("metaposController/produk");?>">Our Products</a></li>
			<li>D180 MPOS</li>
		</ul>
	</div><!-- .main-container -->
</section><!-- .breadcrumbs-section -->

<section class="section-padding">
	<div class="main-container">
		<div class="row">
			<div class="grid-child n-992-1per2 n-992-no-margin-bottom fadeIn">
				<div class="product-images-wrap">
					
                    <div class="slider-for-product">
                    	<div class="item">
                    		<div class="ngc-media">
                    			<a href="http://mobey.id/uploads/5a39ce6116c74_20171220094345-1.jpg" data-fancybox="gallery">
                    				<img src="http://mobey.id/uploads/5a39ce6116c74_20171220094345-1.jpg" alt="D180 MPOS">
                    			</a>
                    		</div><!-- .ngc-media -->
                    	</div><!-- .item -->
                    	<div class="item">
                    		<div class="ngc-media">
                    			<a href="http://mobey.id/uploads/5aaa087fb3225_20180315124535-1.jpg" data-fancybox="gallery">
                    				<img src="http://mobey.id/uploads/5aaa087fb3225_20180315124535-1.jpg" alt="D180 MPOS">
                    			</a>
                    		</div><!-- .ngc-media -->
                    	</div><!-- .item -->
                    	<div class="item">
                    		<div class="ngc-media">
                    			<a href="http://mobey.id/uploads/5aaa088fa04b2_20180315124551-1.jpg" data-fancybox="gallery">
                    				<img src="http://mobey.id/uploads/5aaa088fa04b2_20180315124551-1.jpg" alt="D180 MPOS">
                    			</a>
                    		</div><!-- .ngc-media -->
                    	</div><!-- .item -->
                    </div><!-- .slider-for-product -->
                    <div class="slider-product-nav-wrap">
                    	<div class="slider-product-nav">
                    		<div class="item">
                    			<div class="ngc-media">
                    				<img src="http://mobey.id/uploads/5a39ce6116c74_20171220094345-1.jpg" alt="D180 MPOS">
                    			</div><!-- .ngc-media -->
                    		</div><!-- .item -->
                    		<div class="item">
                    			<div class="ngc-media">
                    				<img src="http://mobey.id/uploads/5aaa087fb3225_20180315124535-1.jpg" alt="D180 MPOS">
                    			</div><!-- .ngc-media -->
                    		</div><!-- .item -->
                    		<div class="item">
                    			<div class="ngc-media">
                    				<img src="http://mobey.id/uploads/5aaa088fa04b2_20180315124551-1.jpg" alt="D180 MPOS">
                    			</div><!-- .ngc-media -->
                    		</div><!-- .item -->
                    	</div><!-- .slider-product-nav -->
                    </div><!-- .slider-product-nav-wrap -->
                    
				</div><!-- .product-images-wrap -->
			</div><!-- .grid-child -->
		
        	<div class="grid-child n-992-1per2 n-no-margin-bottom slideRightIn">
				<h1 class="ngc-maintitle">D180 MPOS</h1>
				<p class="ngc-intro">The beautifully designed D180 streamlines payment into a unique personal program, and you can now take all your value-added applications on the D180 to provide a high-tech and user-friendly customer interaction point.</p>
				<div class="nuke-wysiwyg">

					<p><strong><em>Pocket-sized &amp; touch-press key design</em></strong><strong><em>&nbsp;</em>|&nbsp;<em>iOS&nbsp;&amp; Android compatible&nbsp;</em>|&nbsp;Mfi&nbsp;<em>certified by Apple&nbsp;</em>|&nbsp;<em>Visa payWave &amp; MasterCard&nbsp;Contactless certified</em><em>&nbsp;</em>|&nbsp;<em>Bluetooth / WiFi / GPRS connectivity&nbsp;</em></strong></p>

				</div><!-- .nuke-wysiwyg -->
			</div><!-- .grid-child -->
            
		</div><!-- .row -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding n-no-padding-top">
	<div class="main-container">
		<div class="max-800 lr-auto n-align-center">
			<h1 class="ngc-maintitle slideDownIn">Why Mobey?</h1>
			<p class="ngc-intro slideUpIn">Accept card payments issued by any financial institution in whatever form International schemes or local debit card using a single low cost terminal. Integrates with existing app to get started quickly.</p>
		</div><!-- .max-800 -->
		<div class="row same-height why-us">
			
            <div class="grid-child n-360-1per2 n-768-1per3">
            	<div class="wu-item slideRightIn">
            		<img src="http://mobey.id/uploads/5a39d5d8f0a36_20171220101536-1.png" alt="Secure">
            		<h2 class="ngc-title">Secure</h2>
            		<p>Reliable agent banking with point-to-point encryption to safe guard all transactions with Certified National Standard Indonesian Chip Card Specification (NSICCS)</p>
            	</div><!-- .wu-item -->
            </div><!-- .grid-child -->
            <div class="grid-child n-360-1per2 n-768-1per3">
            	<div class="wu-item slideRightIn">
            		<img src="http://mobey.id/uploads/5a39d617c2a3f_20171220101639-1.png" alt="Fast Update">
            		<h2 class="ngc-title">Fast Update</h2>
            		<p>We are constantly improving with free updates that equip you with great new features to help you run your business more efficiently and effectively</p>
            	</div><!-- .wu-item -->
            </div><!-- .grid-child -->
            <div class="grid-child n-360-1per2 n-768-1per3">
            	<div class="wu-item slideRightIn">
            		<img src="http://mobey.id/uploads/5a39d625f2ef3_20171220101653-1.png" alt="Easy">
            		<h2 class="ngc-title">Easy</h2>
            		<p>Mobey is easy to use and easy to be implemented in your business , Support for Magnetic and Chip card</p>
            	</div><!-- .wu-item -->
            </div><!-- .grid-child -->

		</div><!-- .why-us -->
		
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding has-bg has-overlay overlay-primary" style="background-image:url(http://mobey.id/uploads/5acee84768278_20180412120159-1.jpg);">
	<div class="main-container">
		<div class="row">
			<div class="grid-child n-768-1per3 slideLeftIn n-no-margin-bottom">
				<h2 class="ngc-maintitle">Mobey Package</h2>
				<p class="ngc-intro">For best price contact us!</p>
			</div><!-- .grid-child -->
			
            <div class="grid-child n-768-2per3 n-no-margin-bottom">
				
                <div class="filter-child slideUpIn">
                	<a href="#" class="fc-toggle">Mobey Personal Package I</a>
                	<div class="fc-content">
                		<div class="package-skema-price">
                			<span class="n-primary price">IDR 165.000</span>
                			<span class="text">/ device / bulan</span>
                			<span class="n-primary skema"><strong>+ Debit On Us 0.15%  + Debit Off Us 1.0%</strong></span>
                		</div><!-- .package-skema-price -->
                		<div class="nuke-wysiwyg">
                			<p>Maximum Transaction Rp 5.000.000</p>
                		</div><!-- .nuke-wysiwyg -->
                		<a href="http://mobey.id/contact" class="btn btn-secondary">Contact Us</a>
                	</div><!-- .fc-content -->
                </div><!-- .filter-child -->

                <div class="filter-child slideUpIn">
                	<a href="#" class="fc-toggle">Mobey Personal Package II</a>
                	<div class="fc-content">
                		<div class="package-skema-price">
                			<span class="n-primary price">IDR 265.000</span>
                			<span class="text">/ device / bulan</span>
                			<span class="n-primary skema"><strong>+ Debit On Us 0.15%  + Debit Off Us 1.0%</strong></span>
                		</div><!-- .package-skema-price -->
                		<div class="nuke-wysiwyg">
                			<p>Maximum Transaction Rp 10.000.000</p>
                		</div><!-- .nuke-wysiwyg -->
                		<a href="http://mobey.id/contact" class="btn btn-secondary">Contact Us</a>
                	</div><!-- .fc-content -->
                </div><!-- .filter-child -->

                <div class="filter-child slideUpIn">
                	<a href="#" class="fc-toggle">Mobey Personal Package III</a>
                	<div class="fc-content"><div class="package-skema-price">
                		<span class="n-primary price">IDR 665.000</span>
                		<span class="text">/ device / bulan</span>
                		<span class="n-primary skema"><strong>+ Debit On Us 0.15%  + Debit Off Us 1.0%</strong></span>
                	</div><!-- .package-skema-price -->
                	<div class="nuke-wysiwyg">
                		<p>Maximum Transaction Rp 30.000.000</p>
                	</div><!-- .nuke-wysiwyg -->
                	<a href="http://mobey.id/contact" class="btn btn-secondary">Contact Us</a>
                </div><!-- .fc-content -->
            </div><!-- .filter-child -->

            <div class="filter-child slideUpIn">
            	<a href="#" class="fc-toggle">Mobey Personal Package IV</a>
            	<div class="fc-content">
            		<div class="package-skema-price">
            			<span class="n-primary price">IDR 1.065.000</span>
            			<span class="text">/ device / bulan</span>
            			<span class="n-primary skema"><strong>+ Debit On Us 0.15%  + Debit Off Us 1.0%</strong></span>
            		</div><!-- .package-skema-price -->
            		<div class="nuke-wysiwyg">
            			<p>Maximum Transaction Rp 50.000.000</p>
            		</div><!-- .nuke-wysiwyg -->
            		<a href="http://mobey.id/contact" class="btn btn-secondary">Contact Us</a>
            	</div><!-- .fc-content -->
            </div><!-- .filter-child -->
    			
			</div><!-- .grid-child -->
		</div><!-- .row -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding n-no-padding-bottom">
	<div class="main-container">
		<div class="max-800 lr-auto n-align-center">
			<h2 class="ngc-maintitle slideDownIn">Related Products</h2>
			<br />
		</div><!-- .max-800 -->
		<div class="product-carousel-wrap">
			<div class="product-carousel">
				<div class="item">
					<div class="pc-item fadeIn">
						<div class="ngc-media">
							<a href="http://mobey.id/products-detail/a920-paydroid/13">
								<img src="http://mobey.id/uploads/5aaa03822cdb7_20180315122418-1.jpg" alt="A920 PayDroid">
							</a>
						</div><!-- .ngc-media -->
						<div class="ngc-text">
							<h3 class="ngc-title">A920 PayDroid</h3>
							<p>Open Android OS&nbsp;|&nbsp;Large colour touch screen&nbsp;|&nbsp;Double injection&nbsp;|& &hellip;</p>
							<a href="http://mobey.id/products-detail/a920-paydroid/13" class="link-more">Learn More &rsaquo;</a>
						</div><!-- .ngc-text -->
					</div><!-- .pc-item -->
				</div><!-- .item -->
				<div class="item">
					<div class="pc-item fadeIn">
						<div class="ngc-media">
							<a href="http://mobey.id/products-detail/merchant-services/14">
								<img src="http://mobey.id/uploads/5aaa0cbdef434_20180315130341-1.jpg" alt="Merchant Services">
							</a>
						</div><!-- .ngc-media -->
						<div class="ngc-text">
							<h3 class="ngc-title">Merchant Services</h3>
							<p>Certified ISO 9001:2008 for Operation &amp; Maintenance Services in three main area&nbsp;: &hellip;</p>
							<a href="http://mobey.id/products-detail/merchant-services/14" class="link-more">Learn More &rsaquo;</a>
						</div><!-- .ngc-text -->
					</div><!-- .pc-item -->
				</div><!-- .item -->
			</div><!-- .product-carousel -->
		</div><!-- .product-carousel-wrap -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<script>
    $(document).ready(function(){
    	$(".main-nav-3, .mobile-nav-3").addClass("active");
    });
    $(window).load(function(){
    	
    });
</script>

<section class="section-padding n-1200-no-padding-bottom slideUpIn">
	<div class="main-container">
		<div class="newsletter-area">
			<div class="ngc-text">
				<h2 class="ngc-title n-primary">Subscribe to Our Newsletter</h2>
				<p>No spam, only promotions.</p>
			</div><!-- .ngc-text -->
            <form action="http://mobey.id/do-subscribe-newsletter" method="post" id="newsletter_form">
            <input type="hidden" name="_token" value="xs0BlrO91bujlPKM2yZzDJWmFY2fIjB7zZr3IEfe">		
            <div class="newsletter-form">
				<input type="email" placeholder="Your email address .." class="input-text" name="email" maxlength="200">
				<button class="submit-btn">
					<span class="fal fa-envelope"></span>
					<span class="text n-1-hide n-992-show">SUBSCRIBE</span>
				</button>
			</div><!-- .newsletter-form -->
            </form>
		</div><!-- .newsletter-area -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<?php
	echo $footer;
?>

<div class="nuke-overlay">
   <div class="nuke-modal-content">
       <img src="http://mobey.id/images/loading.gif" />
   </div><!--Buat item added-->
</div>

<script>
    $(document).ready(function(){
	
 			$(".lang_btn").click(function(e){
				$(".nuke-overlay, .nuke-modal-content").fadeIn();
				
				var itemlist = $(this).attr("name");
				$.post("http://mobey.id/change-language-parameter", {_token:"xs0BlrO91bujlPKM2yZzDJWmFY2fIjB7zZr3IEfe", "itemlist": itemlist },
				function(data){	
					$(".nuke-overlay, .nuke-modal-content").fadeOut();
					location.reload();					
				});		
				
				e.preventDefault();	
			});	
    });
</script>