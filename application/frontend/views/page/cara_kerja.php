<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--<![endif]-->

<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="all,index,follow">
<meta name="googlebot" content="all,index,follow">
<meta name="revisit-after" content="2 days">
<meta name="author" content="Nukegraphic Indonesia">
<meta name="rating" content="general">
<meta name="viewport" content="width=device-width, initial-scale=1">


<title>How It Works | MetaPos</title>

<meta name="keywords" content="Welcome to MetaPos">
<meta name="description" content="Welcome to MetaPos">
<meta property="og:title" content="Welcome to MetaPos">
<meta property="og:image" content="assets/image/metaposss.png">
<meta property="og:site_name" content="MetaPos">
<meta property="og:description" content="Welcome to MetaPos">

<!-- <meta property="og:url" content="http://mobey.id/home"> -->

<link rel="shortcut icon" href="<?php echo base_url();?>assets/image/metaposss.png" type="image/x-icon">
<!-- Theme color for chrome, firefox and opera -->
<meta name="theme-color" content="#FFA812" />
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#FFA812">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="HandheldFriendly" content="true" />
<!-- Add to home screen and color theme -->

<?php
	echo $style;
	echo $script;
?>

</head>

<body>
<?php
	echo $header;
?>

<section class="static-banner">
	<img src="<?php echo base_url();?>assets/image/5a377abb70f1f_20171218152219-1.jpg" alt="How It Works?">
</section><!-- .static-banner -->
<section class="breadcrumbs-section">
	<div class="main-container">
		<ul class="breadcrumbs">
			<li><a href="<?php echo base_url();?>">Home</a></li>
			<li>How It Works</li>
		</ul>
	</div><!-- .main-container -->
</section><!-- .breadcrumbs-section -->

<section class="section-padding">
	<div class="main-container">
		<div class="index-about">
			<div class="ia-image n-1-hide n-768-show slideLeftInFlex">
				<img src="<?php echo base_url();?>assets/image/sunmi 2.png" alt="How It Works?">
			</div><!-- .ia-image -->
			<div class="content">
				<h1 class="ngc-maintitle slideUpIn">How It Works?</h1>
				<div class="ngc-intro slideUpIn">
					<p>Using traditional way in recording sales transaction, can be uneffective and wasting time. Merchants should collect their invoice, record manually, and have no idea about current profit, top selling item, and customers database. Metapos help merchants to eliminate error in recording daily transaction, offering multiple payment methods, and providing accurate store performace report automatically. At the end of the day, merchants will understand their store performance and make a best decision for their business. Getting started with Metapos and you are ready to run your business!</p>
          <!--
					<p>Mencatat transaksi penjualan secara tradisional akan sangat tidak efektif dan menghabiskan waktu. Toko harus mengumpulkan nota penjualan, mencatat dan merekap secara manual, dan terkadang tidak benar-benar mengetahui laba bersih usahanya, produk apa yang paling laku terjual, dan tidak mengetahui konsumennya sendiri. Metapos akan membantu toko anda untuk menyingkirkan kesalahan dalam pencatatan transaksi, menawarkan metode pembayaran beragam, dan menyediakan laporan penjualan toko secara otomatis melalui sistem. Pada akhirnya, performa toko dan pengambilan keputusan sangat penting untuk pertumbuhan bisnis Anda. Inilah mengapa Metapos hadir sebagai solusi manajemen kasir untuk memenuhi kebutuhan Anda. Mulailah menggunakan Metapos untuk menjalankan bisnis Anda:</p>
          -->
				</div>
			</div><!-- .content -->
		</div><!-- .index-about -->
    	<div class="hiw-steps">
            <div class="item">
            	<div class="steps n-align-center slideRightIn">
            		<div class="ngc-media">
            			<img src="<?php echo base_url();?>assets/image/5a377ee6de16e_20171218154006-1.png" alt="Registration">
            			<span class="step-number">1</span>
            		</div><!-- .ngc-media -->
            		<div class="ngc-text">
            			<h2 class="ngc-title n-primary">Download</h2>
<!--
            			<h2 class="ngc-title n-primary">Download aplikasi Metapos di Google Play Store</h2>
-->
            			<p>&nbsp</p>
            		</div><!-- .ngc-text -->
            	</div><!-- .steps -->
            </div><!-- .item -->
            <div class="item">
            	<div class="steps n-align-center slideRightIn">
            		<div class="ngc-media">
            			<img src="<?php echo base_url();?>assets/image/5a377f1a24c51_20171218154058-1.png" alt="Bank Screening">
            			<span class="step-number">2</span>
            		</div><!-- .ngc-media -->
            		<div class="ngc-text">
            			<h2 class="ngc-title n-primary">Registration</h2>
<!--
            			<h2 class="ngc-title n-primary">Daftarkan toko anda di aplikasi Metapos atau Website Metapos</h2>
-->
            			<p>&nbsp</p>
            		</div><!-- .ngc-text -->
            	</div><!-- .steps -->
            </div><!-- .item -->
            <div class="item">
            	<div class="steps n-align-center slideRightIn">
            		<div class="ngc-media">
            			<img src="<?php echo base_url();?>assets/image/5a377f3129f95_20171218154121-1.png" alt="Installation / Setup">
            			<span class="step-number">3</span>
            		</div><!-- .ngc-media -->
            		<div class="ngc-text">
            			<h2 class="ngc-title n-primary">Setup Store <br/>& Ready to Go!</h2>
<!--
            			<h2 class="ngc-title n-primary">Metapos siap untuk digunakan dan jangan lupa untuk mengecek laporan penjualan toko anda kapan saja, dimana saja</h2>
-->
            			<p>&nbsp</p>
            		</div><!-- .ngc-text -->
            	</div><!-- .steps -->
            </div><!-- .item -->
		</div><!-- .hiw-steps -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding has-bg has-overlay overlay-primary" style="background-image:url(assets/image/5a37849c2cbda_20171218160428-1.jpg);">
	<div class="main-container">
		<div class="row">
			<div class="grid-child n-768-1per3 slideLeftIn n-no-margin-bottom">
				<h2 class="ngc-maintitle">Who Can Use MetaPos?</h2>
				<p class="ngc-intro">Any type of your business, big or small, can make use of Metapos in their day-to-day transaction.</p>
<!--
				<p class="ngc-intro">Apapun jenis bisnis Anda, besar atau kecil, dapat menggunakan Metapos dalam transaksaksi kesehariannya.</p>
-->
			</div><!-- .grid-child -->

			<div class="grid-child n-768-2per3 n-no-margin-bottom">
				<div class="row same-height">
					<div class="grid-child n-540-1per2">
						<div class="who-can slideRightIn">
							<img src="<?php echo base_url();?>assets/image/5a38962ad330b_20171219113138-1.png" alt="Merchant">
							<h3 class="ngc-title n-primary">Owner</h3>
<!--
							<h3 class="ngc-title n-primary">Pemilik</h3>
							<p>Pemilik dapat mengontrol toko-toko mereka kapanpun dan dimanapu</p>
-->
						</div><!-- .who-can -->
					</div><!-- .grid-child -->
					<div class="grid-child n-540-1per2">
						<div class="who-can slideRightIn">
							<img src="<?php echo base_url();?>assets/image/5a3896403dd4a_20171219113200-1.png" alt="Third-Party">
							<h3 class="ngc-title n-primary">Supervisor</h3>
              <!--
							<p>Supervisor dapat mengatur operasi toko sehari-hari</p>
              -->
						</div><!-- .who-can -->
					</div><!-- .grid-child -->
					<div class="grid-child n-540-1per2">
						<div class="who-can slideRightIn">
							<img src="<?php echo base_url();?>assets/image/5a38965022387_20171219113216-1.png" alt="Bank">
							<h3 class="ngc-title n-primary">Cashier</h3>
						</div><!-- .who-can -->
					</div><!-- .grid-child -->
					<div class="grid-child n-540-1per2">
						<div class="who-can slideRightIn">
							<img src="<?php echo base_url();?>assets/image/5a38965e2b34a_20171219113230-1.png" alt="Partner">
							<h3 class="ngc-title n-primary">Event<br/>Organizer</h3>
						</div><!-- .who-can -->
					</div><!-- .grid-child -->
				</div><!-- .row -->
			</div><!-- .grid-child -->

		</div><!-- .row -->
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<section class="section-padding n-no-padding-bottom">
	<div class="main-container">
		<div class="max-800 lr-auto n-align-center">
			<h2 class="ngc-maintitle slideDownIn">Features</h2>
			<br />
		</div><!-- .max-800 -->
		<div class="row same-height">
			<?php
                if($produk != false){
                    foreach($produk->result() as $row){
            ?>
            <div class="grid-child n-540-1per2 n-768-1per3">
            	<div class="pc-item fadeIn">
            		<div class="ngc-media">
            			<a href="<?php echo site_url("ProdukController/detail/$row->Alias");?>">
                            <img class="center" src="<?=$row->Pic?>">
            			</a>
            		</div><!-- .ngc-media -->
            		<div class="ngc-text">
            			<h3 class="ngc-title"><?=$row->ProductName?></h3>
                            <p><?=$row->Description?></p>
            			<a href="<?php echo site_url("ProdukController/detail/$row->Alias");?>" class="link-more">Learn More &rsaquo;</a>
            		</div><!-- .ngc-text -->
            	</div><!-- .pc-item -->
            </div><!-- .grid-child -->
            <?php
                    }
                }
            ?>
        </div>
	</div><!-- .main-container -->
</section><!-- .section-padding -->

<script>
    $(document).ready(function(){
    	$(".main-nav-4, .mobile-nav-4").addClass("active");
    });
    $(window).load(function(){

    });
</script>


<?php
	echo $aboutfooter;
?>

<div class="nuke-overlay">
   <div class="nuke-modal-content">
       <img src="<?php echo base_url();?>assets/image/loading.gif" />
   </div><!--Buat item added-->
</div>

<script>
    $(document).ready(function(){

 			$(".lang_btn").click(function(e){
				$(".nuke-overlay, .nuke-modal-content").fadeIn();

				var itemlist = $(this).attr("name");
				$.post("http://mobey.id/change-language-parameter", {_token:"xs0BlrO91bujlPKM2yZzDJWmFY2fIjB7zZr3IEfe", "itemlist": itemlist },
				function(data){
					$(".nuke-overlay, .nuke-modal-content").fadeOut();
					location.reload();
				});

				e.preventDefault();
			});
    });
</script>
</body>
</html>
