<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> 
<!--<![endif]-->

<html class="no-js" lang="en"> 

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="all,index,follow">
<meta name="googlebot" content="all,index,follow">
<meta name="revisit-after" content="2 days">
<meta name="author" content="Nukegraphic Indonesia">
<meta name="rating" content="general">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>News & Updates | MetaPos</title>

<meta name="keywords" content="Welcome to MetaPos">
<meta name="description" content="Welcome to MetaPos">
<meta property="og:title" content="Welcome to MetaPos">
<meta property="og:image" content="assets/image/metaposss.png">
<meta property="og:site_name" content="MetaPos">
<meta property="og:description" content="Welcome to MetaPos">


<link rel="shortcut icon" href="assets/image/metaposss.png" type="image/x-icon">
<!-- Theme color for chrome, firefox and opera -->
<meta name="theme-color" content="#FFA812" />
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#FFA812">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="HandheldFriendly" content="true" />
<!-- Add to home screen and color theme -->

<?php
	echo $style;
	echo $script;
?>

</head>

<body>

<?php
	echo $header;
?>

<section class="static-banner">
	<img src="<?php echo base_url();?>assets/image/5acee7e2882b0_20180412120018-1.jpg" alt="News">
</section><!-- .static-banner -->
<section class="breadcrumbs-section">
	<div class="main-container">
		<ul class="breadcrumbs">
			<li><a href="<?php echo site_url();?>">Home</a></li>
			<li>News</li>
		</ul>
	</div><!-- .main-container -->
</section><!-- .breadcrumbs-section -->

<section class="section-padding">
	<div class="main-container">
		<div class="max-800 lr-auto n-align-center">
			<h1 class="ngc-maintitle slideDownIn">News</h1>
			<p class="ngc-intro slideUpIn">&nbsp</p>
		</div><!-- .max-800 -->

		<ul class="ngc-tabs">
			<li><a href="<?php echo site_url("NewsController");?>" class='active'>News</a></li><li><a href="<?php echo site_url("UpdateController");?>" >Updates</a></li>
		</ul><!-- .ngc-tabs -->
		<br />

		<div class="row same-height">
			
            <div class="grid-child n-768-1per2 slideLeftInFlex">
            	<?php
                	if($artikel != false){
                    	foreach($artikel->result() as $row){
            	?>
            	<div class="news-item">
            		<div class="ngc-media">
            			<a href="<?php echo site_url("NewsController");?>">
            				<img src="<?php echo base_url();?>assets/image/5acaf3c05f466_20180409120152-1.jpg" alt="MetaPos, the newly launched mPOS solution for Payment, is live.">
            			</a>
            		</div><!-- .ngc-media -->
            		<div class="ngc-text">
						<div class="news-date"><!-- 
							<span class="d-date">01</span>
							<span class="d-month">Mar</span -->>
						</div><!-- .news-date -->
						<h3 class="ngc-title">
							<a href="#"><?=$row->Title?></a>
						</h3>
						<span class="ngc-news-tag">
							<span class="fa fa-tags"></span> News
						</span><!-- .ngc-news-tag -->
						<p class="ngc-text-preview"><?=$row->ContentText?></p>
						
					</div><!-- .ngc-text -->
            	</div><!-- .news-item -->
            	<?php
            			}
            		}
            	?>
            </div><!-- .grid-child -->
        </div><!-- .row -->
	    <br />		
	</div><!-- .main-container -->
</section><!-- .section-padding -->


<script>
    $(document).ready(function(){
    	
    });
    $(window).load(function(){
    	
    });
</script>

<?php
	echo $aboutfooter;
?>

<div class="nuke-overlay">
   <div class="nuke-modal-content">
       <img src="<?php echo base_url();?>assets/image/loading.gif" />
   </div><!--Buat item added-->
</div>

<script>
    $(document).ready(function(){
	
 			$(".lang_btn").click(function(e){
				$(".nuke-overlay, .nuke-modal-content").fadeIn();
				
				var itemlist = $(this).attr("name");
				$.post("http://mobey.id/change-language-parameter", {_token:"N1TzBLcplY3wiZEriNybsW5yNUHbPAaD6lym377n", "itemlist": itemlist },
				function(data){	
					$(".nuke-overlay, .nuke-modal-content").fadeOut();
					location.reload();					
				});		
				
				e.preventDefault();	
			});	
    });
</script>