<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class metaposModel extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function index(){
		$this->load->view('welcome_message');
	}

	public function get_info($field,$table,$where=array(),$sorting_field="",$sorting_type="asc"){
		$this->db->select($field);
		if(count($where) > 0){
			$this->db->where($where);
		}
		if($sorting_field != ""){
			$this->db->order_by($sorting_field,$sorting_type);
		}
		$query = $this->db->get($table);
		if($query->num_rows() > 0){
			return $query;
		}
		return false;
	}

	public function get_info_where_and_where_not_in($selected, $table_name, $where, $where_not_in_field,$where_not_in){
        $this->db->select($selected);
        $this->db->where($where);
        $this->db->where_not_in($where_not_in_field,$where_not_in);
        $query = $this->db->get($table_name);
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }

    public function add_new_data($table_name, $data){
      $this->db->insert($table_name, $data);
  	}

  	public function get_produk($keyword){
		$this->db->select('ProductId,ProductName,Pic,Description,Alias');
		$this->db->like('ProductName',$keyword);
		$this->db->or_like('Description',$keyword);

		$this->db->from('TR_Product');
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query;
		}
		return false;
	}

	public function get_artikel($keyword){
		$this->db->select('ArticleId,Title,ContentText');
		$this->db->like('Title',$keyword);
		$this->db->or_like('ContentText',$keyword);

		$this->db->from('TR_Article');
		$query = $this->db->get();

		return $query;
	}

	//  public function search($keyword){
	//  	$this->db->select('ProductName,Pic,Description');
	//  	$this->db->like($keyword);
	//  	$query = $this->db->get('TR_Product');

	//  	return $query;
	// }

	public function artikel(){
		$this->db->select('Title,ContentText');
		$this->db->limit(1);
		$query = $this->db->get('TR_Article');

		return $query;
	}

	public function klien(){
		$this->db->select('TestimonialId,ClientName,ClientLogo');
		$this->db->limit(8);
		$query = $this->db->get('TT_Testimonial');

		return $query;
	}

	public function testimoni(){
		$this->db->select('TestimonialId,ClientName,ClientLogo,Testimonial');
		$this->db->limit(3);
		$query = $this->db->get('TT_Testimonial');

		return $query;
	}


	public function produk(){
		$this->db->select('ProductId,ProductName,Pic,Description,Alias');
		$query = $this->db->get('TR_Product');
		return $query;
	}

	public function produkdetail(){
		$this->db->select('ProductId,ProductName,Pic,Description,Alias');
		$this->db->limit(3);
		$query = $this->db->get('TR_Product');
		return $query;
	}

	public function detail($alias){
		$this->db->select('ProductId,ProductName,Pic,Description,Alias');
		$this->db->where('Alias', $alias);
		$this->db->from('TR_Product');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query;
		}
		return FALSE;
	}

	public function privacy(){
		$this->db->select('ContentText');
		$query = $this->db->get('TR_Setting');
		return $query;
	}

	public function term(){
		$this->db->select('ContentText');
		$query = $this->db->get('TR_Setting');
		return $query;
	}

	public function subscribe($value){
		$this->db->insert('Subscribe', $value);

	}



}
