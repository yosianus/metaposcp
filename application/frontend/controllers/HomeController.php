<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('MetaposModel');
	}


	//Buat tampilin halaman Home
	public function index(){
		if(isset($_POST['_do_add_new'])){
            $this->_do_add_new();
        }
        else{
            $data['action'] = 'addnew';

// ----- BAGIAN PRODUCT HEADER ----------


			$data['produk'] = $this->MetaposModel->produk('TR_Product');

			$data['header']=$this->load->view('template/header.php',$data,TRUE);

// ----------- BAGIAN LINK SOSMED ------------
			$where_fb_link['Alias'] = 'facebook';
			$datafooter['facebooklink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_fb_link);

			$where_linkedin_link['Alias'] = 'linked-in';
			$datafooter['linkedinlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_linkedin_link);

			$where_instagram_link['Alias'] = 'instagram';
			$datafooter['instagramlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_instagram_link);

			$where_twitter_link['Alias'] = 'twitter';
			$datafooter['twitterlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_twitter_link);

			$where_youtube_link['Alias'] = 'youtube';
			$datafooter['youtubelink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_youtube_link);

			$where_google_link['Alias'] = 'google-plus';
			$datafooter['googlelink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_google_link);

// ------------- BAGIAN CONTACT --------------
			$where_address_link['Alias'] = 'office-address';
			$datafooter['officeaddress'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_address_link);

			$where_fax_link['Alias'] = 'company-fax';
			$datafooter['companyfax'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_fax_link);

			$where_phone_link['Alias'] = 'company-phone';
			$datafooter['companyphone'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_phone_link);

			$where_email_link['Alias'] = 'sales-email';
			$datafooter['salesemail'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_email_link);


			$data['footer']=$this->load->view('template/footer.php',$datafooter,TRUE);

// ------------- BAGIAN CONTACT --------------
			$where_address_link['Alias'] = 'office-address';
			$datacontact['officeaddress'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_address_link);

			$where_fax_link['Alias'] = 'company-fax';
			$datacontact['companyfax'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_fax_link);

			$where_phone_link['Alias'] = 'company-phone';
			$datacontact['companyphone'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_phone_link);

			$where_email_link['Alias'] = 'sales-email';
			$datacontact['salesemail'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_email_link);

		 	$data['aboutfooter']=$this->load->view('template/aboutfooter.php',$datacontact,TRUE);

		 	$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		 	$data['style']=$this->load->view('include/style.php',NULL,TRUE);

		 	$data['testimoni'] = $this->MetaposModel->testimoni('TT_Testimonial');
			$data['klien'] = $this->MetaposModel->klien('TT_Testimonial');

			$data['produkdetail'] = $this->MetaposModel->produkdetail('TR_Product');

			$data['artikel'] = $this->MetaposModel->artikel('TR_Article');

			$this->session->set_flashdata('msg', "signup_success");

			$this->load->view('page/home.php', $data);
        }
    }



    private function _do_add_new(){
         $this->load->library('form_validation');

        $email = $this->input->post('email');

        $this->form_validation->set_rules('email', 'email', 'valid_email',
        	array('valid_email'=>'You must enter a valid email address!'));

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $data['Email'] = $email;

            $this->MetaposModel->add_new_data('TT_Subscription',$data);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', "subscribe_success");

            // redirect(site_url(''));

            redirect(base_url()."", 'refresh');
        }
        else{
            $this->session->set_flashdata('msg', "subscribe_failed");

            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url()."", 'refresh');
        }
    }

	public function check_produk($produkname,$produk_id=''){
      $where['ProductName'] = $produkname;
      $where['Status'] = 1;
      if($produk_id == ''){
          $infodata = $this->general_model->get_info('ProductName', 'TR_Product', $where);
      }else{
          $infodata = $this->general_model->get_info_where_and_where_not_in('ProductId', 'TR_Product', $where,'ProductId',$produk_id);
      }

      if($infodata != false){
          $this->form_validation->set_message('check_check_produkname','ProdukName is already at our data.');
          return false;
      }
      else{
          return true;
      }
  	}


	//Buat tampilin halaman login
	public function login(){
// ------ BAGIAN PRODUCT HEADER ----------

		$data['produk'] = $this->MetaposModel->produk('TR_Product');

		$data['header']=$this->load->view('template/header.php',$data,TRUE);

// ----------- BAGIAN LINK SOSMED ------------
		$where_fb_link['Alias'] = 'facebook';
		$datafooter['facebooklink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_fb_link);

		$where_linkedin_link['Alias'] = 'linked-in';
		$datafooter['linkedinlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_linkedin_link);

		$where_instagram_link['Alias'] = 'instagram';
		$datafooter['instagramlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_instagram_link);

		$where_twitter_link['Alias'] = 'twitter';
		$datafooter['twitterlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_twitter_link);

		$where_youtube_link['Alias'] = 'youtube';
		$datafooter['youtubelink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_youtube_link);

		$where_google_link['Alias'] = 'google-plus';
		$datafooter['googlelink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_google_link);

//-------------- BAGIAN CONTACT --------------
		$where_address_link['Alias'] = 'office-address';
		$datafooter['officeaddress'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_address_link);

		$where_fax_link['Alias'] = 'company-fax';
		$datafooter['companyfax'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_fax_link);

		$where_phone_link['Alias'] = 'company-phone';
		$datafooter['companyphone'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_phone_link);

		$where_email_link['Alias'] = 'sales-email';
		$datafooter['salesemail'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_email_link);

// -------------------------------------------
		$data['footer']=$this->load->view('template/footer.php',$datafooter,TRUE);

		$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		$data['style']=$this->load->view('include/style.php',NULL,TRUE);

		// $data['testimoni'] = $this->MetaposModel->testimoni();
		// $data['klien'] = $this->MetaposModel->klien();

		 // $data['produk'] = $this->MetaposModel->produk();

		$this->load->view('page/commingsoon_login.php');
	}

	public function search(){
		if(isset($_POST['_do_search'])){
			 // echo "lallaa";
			$keyword = $this->input->post('keyword');
			$data['from'] = "cari";

			$data['data_produk'] = $this->MetaposModel->get_produk($keyword);
		  	 $data['data_artikel'] = $this->MetaposModel->get_artikel($keyword);

		  	$key = $this->input->get('keyword');

			$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		  	$data['style']=$this->load->view('include/style.php',NULL,TRUE);

			// ---------- BAGIAN PRODUCT HEADER ----------

			$data['produk'] = $this->MetaposModel->produk('TR_Product');

			$data['header']=$this->load->view('template/header.php',$data,TRUE);

			// ----------- BAGIAN LINK SOSMED ------------
			$where_fb_link['Alias'] = 'facebook';
			$datafooter['facebooklink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_fb_link);

			$where_linkedin_link['Alias'] = 'linked-in';
			$datafooter['linkedinlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_linkedin_link);

			$where_instagram_link['Alias'] = 'instagram';
			$datafooter['instagramlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_instagram_link);

			$where_twitter_link['Alias'] = 'twitter';
			$datafooter['twitterlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_twitter_link);

			$where_youtube_link['Alias'] = 'youtube';
			$datafooter['youtubelink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_youtube_link);

			$where_google_link['Alias'] = 'google-plus';
			$datafooter['googlelink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_google_link);

		 	$data['aboutfooter']=$this->load->view('template/aboutfooter.php',$datafooter,TRUE);

		  	$data['flashdata']=$this->load->view('template/flashdata.php',NULL,TRUE);


			$this->load->view('page/search.php', $data);
		}
		else{
			 // echo "oekoekoke";
			$data['from'] = "data_tidak_ditemukan";
			$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		  	$data['style']=$this->load->view('include/style.php',NULL,TRUE);

			// ---------- BAGIAN PRODUCT HEADER ----------

			$data['produk'] = $this->MetaposModel->produk('TR_Product');

			$data['header']=$this->load->view('template/header.php',$data,TRUE);

			// ----------- BAGIAN LINK SOSMED ------------
			$where_fb_link['Alias'] = 'facebook';
			$datafooter['facebooklink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_fb_link);

			$where_linkedin_link['Alias'] = 'linked-in';
			$datafooter['linkedinlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_linkedin_link);

			$where_instagram_link['Alias'] = 'instagram';
			$datafooter['instagramlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_instagram_link);

			$where_twitter_link['Alias'] = 'twitter';
			$datafooter['twitterlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_twitter_link);

			$where_youtube_link['Alias'] = 'youtube';
			$datafooter['youtubelink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_youtube_link);

			$where_google_link['Alias'] = 'google-plus';
			$datafooter['googlelink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_google_link);

		 	$data['aboutfooter']=$this->load->view('template/aboutfooter.php',$datafooter,TRUE);

			$data['data_produk'] = false;
		  	$data['flashdata']=$this->load->view('template/flashdata.php',NULL,TRUE);

			$this->load->view('page/search.php', $data);
		}
	}

}
