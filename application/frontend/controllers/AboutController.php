<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AboutController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		$this->load->model('MetaposModel');
	}


	//Buat tampilin halaman about
	public function index(){
		if(isset($_POST['_do_add_new'])){
            $this->_do_add_new();
        }
        else{
            $data['action'] = 'addnew';

// ----- BAGIAN PRODUCT HEADER ----------

			$data['produk'] = $this->MetaposModel->produk('TR_Product');

			$data['header']=$this->load->view('template/header.php',$data,TRUE);

// ----------- BAGIAN LINK SOSMED ------------
			$where_fb_link['Alias'] = 'facebook';
			$datafooter['facebooklink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_fb_link);

			$where_linkedin_link['Alias'] = 'linked-in';
			$datafooter['linkedinlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_linkedin_link);

			$where_instagram_link['Alias'] = 'instagram';
			$datafooter['instagramlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_instagram_link);

			$where_twitter_link['Alias'] = 'twitter';
			$datafooter['twitterlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_twitter_link);

			$where_youtube_link['Alias'] = 'youtube';
			$datafooter['youtubelink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_youtube_link);

			$where_google_link['Alias'] = 'google-plus';
			$datafooter['googlelink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_google_link);

// ------------- BAGIAN CONTACT --------------
			$where_address_link['Alias'] = 'office-address';
			$datafooter['officeaddress'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_address_link);

			$where_fax_link['Alias'] = 'company-fax';
			$datafooter['companyfax'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_fax_link);

			$where_phone_link['Alias'] = 'company-phone';
			$datafooter['companyphone'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_phone_link);

			$where_email_link['Alias'] = 'sales-email';
			$datafooter['salesemail'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_email_link);


			$data['footer']=$this->load->view('template/footer.php',$datafooter,TRUE);

// ------------- BAGIAN CONTACT --------------
			$where_address_link['Alias'] = 'office-address';
			$datacontact['officeaddress'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_address_link);

			$where_fax_link['Alias'] = 'company-fax';
			$datacontact['companyfax'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_fax_link);

			$where_phone_link['Alias'] = 'company-phone';
			$datacontact['companyphone'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_phone_link);

			$where_email_link['Alias'] = 'sales-email';
			$datacontact['salesemail'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_email_link);

		 	$data['aboutfooter']=$this->load->view('template/aboutfooter.php',$datacontact,TRUE);

// ------------- BAGIAN about --------------

			$where_about_link['Alias'] = 'about-us';
			$dataabout['about'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_about_link);

		 	$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		 	$data['style']=$this->load->view('include/style.php',NULL,TRUE);

		 	$data['testimoni'] = $this->MetaposModel->testimoni('TT_Testimonial');
			$data['klien'] = $this->MetaposModel->klien('TT_Testimonial');

		 	$data['produkdetail'] = $this->MetaposModel->produkdetail('TR_Product');

			$this->load->view('page/about.php', $data);
        }
    }

	private function _do_add_new(){
        $this->load->library('form_validation');

        $email = $this->input->post('email');

        $this->form_validation->set_rules('email', 'email', 'required');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $data['Email'] = $email;

            $this->MetaposModel->add_new_data('TT_Subscription',$data);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Users is success added.');

            redirect(site_url(''));
        }
        else{
            $this->session->set_flashdata('email', $email);

            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());
            redirect(site_url(''));
        }
    }


}
