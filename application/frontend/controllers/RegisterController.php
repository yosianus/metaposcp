<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegisterController extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

	//Buat tampilin halaman login
	public function index(){


		$data['regisfooter']=$this->load->view('template/regisfooter.php',NULL,TRUE);

		$data['script']=$this->load->view('include/script.php',NULL,TRUE);

		$data['style']=$this->load->view('include/style.php',NULL,TRUE);

		$this->load->view('page/register.php',$data);
	}
}