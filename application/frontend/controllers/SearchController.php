<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SearchController extends CI_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('MetaposModel');
	}

	public function index(){
		//$keyword = $this->input->post('search');
		// echo $this->input->post('search');
		// 	die;

		// echo "lalala";
		if(isset($_GET['_do_search'])){
			$keyword = $this->input->post('search');
			$data['from'] = "cari";
			// $data['result_search'] = $this->MetaposModel->search($keyword);

			$data['data_produk'] = $this->MetaposModel->get_produk($keyword);

			$data['data_artikel'] = $this->MetaposModel->get_artikel($keyword);

			$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		  	$data['style']=$this->load->view('include/style.php',NULL,TRUE);

			// ---------- BAGIAN PRODUCT HEADER ----------

			$data['produk'] = $this->MetaposModel->produk('TR_Product');

			$data['header']=$this->load->view('template/header.php',$data,TRUE);

			// ----------- BAGIAN LINK SOSMED ------------
			$where_fb_link['Alias'] = 'facebook';
			$datafooter['facebooklink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_fb_link);

			$where_linkedin_link['Alias'] = 'linked-in';
			$datafooter['linkedinlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_linkedin_link);

			$where_instagram_link['Alias'] = 'instagram';
			$datafooter['instagramlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_instagram_link);

			$where_twitter_link['Alias'] = 'twitter';
			$datafooter['twitterlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_twitter_link);

			$where_youtube_link['Alias'] = 'youtube';
			$datafooter['youtubelink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_youtube_link);

			$where_google_link['Alias'] = 'google-plus';
			$datafooter['googlelink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_google_link);

		 	$data['aboutfooter']=$this->load->view('template/aboutfooter.php',$datafooter,TRUE);

		  	$data['flashdata']=$this->load->view('template/flashdata.php',NULL,TRUE);

			$this->load->view('page/search.php', $data);
		}
		else{
			// echo "okeokeok";

			$data['from'] = "data_tidak_ditemukan";
			$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		  	$data['style']=$this->load->view('include/style.php',NULL,TRUE);

			// ---------- BAGIAN PRODUCT HEADER ----------

			$data['produk'] = $this->MetaposModel->produk('TR_Product');

			$data['header']=$this->load->view('template/header.php',$data,TRUE);

			// ----------- BAGIAN LINK SOSMED ------------
			$where_fb_link['Alias'] = 'facebook';
			$datafooter['facebooklink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_fb_link);

			$where_linkedin_link['Alias'] = 'linked-in';
			$datafooter['linkedinlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_linkedin_link);

			$where_instagram_link['Alias'] = 'instagram';
			$datafooter['instagramlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_instagram_link);

			$where_twitter_link['Alias'] = 'twitter';
			$datafooter['twitterlink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_twitter_link);

			$where_youtube_link['Alias'] = 'youtube';
			$datafooter['youtubelink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_youtube_link);

			$where_google_link['Alias'] = 'google-plus';
			$datafooter['googlelink'] = $this->MetaposModel->get_info('ContentText','TR_Setting',$where_google_link);

		 	$data['aboutfooter']=$this->load->view('template/aboutfooter.php',$datafooter,TRUE);

		  	$data['flashdata']=$this->load->view('template/flashdata.php',NULL,TRUE);


			$this->load->view('page/search.php', $data);
		}
	}

}
