<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DetailprodukController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		$this->load->model('metaposModel');
	}


	//Buat tampilin halaman detail produk
	public function index(){
// ---------- BAGIAN PRODUCT HEADER ----------
		$data['produkMaster'] = $this->MetaposModel->produkMaster();
			
		$data['produk'] = $this->metaposModel->produk('TR_Product');

		$data['header']=$this->load->view('template/header.php',$data,TRUE);

// ----------- BAGIAN LINK SOSMED ------------
		$where_fb_link['Alias'] = 'facebook';
		$datafooter['facebooklink'] = $this->metaposModel->get_info('ContentText','TR_Setting',$where_fb_link);

		$where_linkedin_link['Alias'] = 'linked-in';
		$datafooter['linkedinlink'] = $this->metaposModel->get_info('ContentText','TR_Setting',$where_linkedin_link);

		$where_instagram_link['Alias'] = 'instagram';
		$datafooter['instagramlink'] = $this->metaposModel->get_info('ContentText','TR_Setting',$where_instagram_link);

		$where_twitter_link['Alias'] = 'twitter';
		$datafooter['twitterlink'] = $this->metaposModel->get_info('ContentText','TR_Setting',$where_twitter_link);

		$where_youtube_link['Alias'] = 'youtube';
		$datafooter['youtubelink'] = $this->metaposModel->get_info('ContentText','TR_Setting',$where_youtube_link);

		$where_google_link['Alias'] = 'google-plus';
		$datafooter['googlelink'] = $this->metaposModel->get_info('ContentText','TR_Setting',$where_google_link);

		 $data['aboutfooter']=$this->load->view('template/aboutfooter.php',$datafooter,TRUE);
// --------------------------------------------------
		 $data['script']=$this->load->view('include/script.php',NULL,TRUE);
		 $data['style']=$this->load->view('include/style.php',NULL,TRUE);

		 // $data['produk'] = $this->metaposModel->get_info('TR_Product');

		$this->load->view('page/detail.php', $data);
	}

	public function check_produk($produkname,$produk_id=''){
      $where['ProductName'] = $produkname;
      $where['Status'] = 1;
      if($produk_id == ''){
          $infodata = $this->general_model->get_info('ProductName', 'TR_Product', $where);
      }else{
          $infodata = $this->general_model->get_info_where_and_where_not_in('ProductId', 'TR_Product', $where,'ProductId',$produk_id);
      }

      if($infodata != false){
          $this->form_validation->set_message('check_check_produkname','ProdukName is already at our data.');
          return false;
      }
      else{
          return true;
      }
  }

}
