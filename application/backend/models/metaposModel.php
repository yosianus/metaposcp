<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MetaposModel extends CI_Model {


	public function index(){
		$this->load->view('welcome_message');
	}

	public function get_info($field,$table,$where=array(),$sorting_field="",$sorting_type="asc"){
		$this->db->select($field);
		if(count($where) > 0){
			$this->db->where($where);
		}
		if($sorting_field != ""){
			$this->db->order_by($sorting_field,$sorting_type);
		}
		$query = $this->db->get($table);
		if($query->num_rows() > 0){
			return $query;
		}
		return false;
	}

	public function get_info_where_and_where_not_in($selected, $table_name, $where, $where_not_in_field,$where_not_in){
        $this->db->select($selected);
        $this->db->where($where);
        $this->db->where_not_in($where_not_in_field,$where_not_in);
        $query = $this->db->get($table_name);
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }

	public function add_new_data($table_name, $data){
      $this->db->insert($table_name, $data);
  }

	public function edit_data($table_name, $data, $where){
      $this->db->update($table_name, $data, $where);
  }

	public function delete_data($table_name, $where){
      $this->db->delete($table_name,$where);
  }

  public function get_login($username){
  	$query = $this->db->query("SELECT * FROM TT_User WHERE UserName='".$username."' ");
		$result = $query->result_array();

		return $result;
  }

}
