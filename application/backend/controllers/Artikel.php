<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {

	public function __construct(){

            parent::__construct();
            $this->load->model('MetaposModel');

       }

	public function index(){
		$data['header']=$this->load->view('template/header.php',NULL,TRUE);
		$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
		$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		$data['style']=$this->load->view('include/style.php',NULL,TRUE);

		$data['data_list'] = $this->MetaposModel->get_info('*','TR_Article');

		$this->load->view('page/artikel',$data);
	}

	public function add_data(){
		if(isset($_POST['_do_add_new'])){
            $this->_do_add_new();
        }else{
            $data['action'] = 'addnew';

						$data['header']=$this->load->view('template/header.php',NULL,TRUE);
						$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
						$data['script']=$this->load->view('include/script.php',NULL,TRUE);
						$data['style']=$this->load->view('include/style.php',NULL,TRUE);

            $this->load->view('page/artikel_form',$data);
        }
	}

	private function _do_add_new(){
        $this->load->library('form_validation');

        $title = $this->input->post('Title');
        $des = $this->input->post('ContentText');
        $thumbnail = $this->input->post('Thumbnail');
        $alias = $this->input->post('Alias');

        $this->form_validation->set_rules('Title', 'Title', 'required');
        $this->form_validation->set_rules('ContentText', 'ContentText', 'required');
        $this->form_validation->set_rules('Thumbnail', 'Thumbnail', 'required');
        $this->form_validation->set_rules('Alias', 'Alias', 'required');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $data['Title'] = $title;
            $data['ContentText'] = $des;
            $data['Thumbnail'] = $thumbnail;
            $data['Alias'] = $alias;

            $this->MetaposModel->add_new_data('TR_Article',$data);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Article is success added.');

            redirect(site_url('artikel'));
        }else{
            $this->session->set_flashdata('title', $title);
            $this->session->set_flashdata('contenttext', $des);
            $this->session->set_flashdata('thumbnail', $thumbnail);
            $this->session->set_flashdata('alias', $alias);

            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());
            redirect(site_url('artikel/add_data'));
        }
    }

	public function edit_data($id=""){
    if(isset($_POST['_do_edit'])){
        $this->_do_edit($id);
    }else{
				$data['header']=$this->load->view('template/header.php',NULL,TRUE);
				$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
				$data['script']=$this->load->view('include/script.php',NULL,TRUE);
				$data['style']=$this->load->view('include/style.php',NULL,TRUE);
        $where['ArticleId'] = $id;

				$info_data = $this->MetaposModel->get_info('*','TR_Article',$where);
        $data['action'] = 'editdata';

				if($info_data != false){
					$data['info_data'] = $info_data->row();
				}else{
					redirect(base_url('artikel'));
				}

        $this->load->view('page/artikel_form',$data);
    }
	}

	public function _do_edit($id){
        $this->load->library('form_validation');

       $title = $this->input->post('Title');
        $des = $this->input->post('ContentText');
        $thumbnail = $this->input->post('Thumbnail');
        $alias = $this->input->post('Alias');

        $this->form_validation->set_rules('Title', 'Title', 'required');
        $this->form_validation->set_rules('ContentText', 'ContentText', 'required');
        $this->form_validation->set_rules('Thumbnail', 'Thumbnail', 'required');
        $this->form_validation->set_rules('Alias', 'Alias', 'required');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $where['ArticleId'] = $id;

            $data['Title'] = $title;
            $data['ContentText'] = $des;
            $data['Thumbnail'] = $thumbnail;
            $data['Alias'] = $alias;
            
            $this->MetaposModel->edit_data('TR_Article',$data,$where);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Edit Article is Success.');
            $this->session->set_flashdata('msgtype', 'success');

            redirect(site_url('artikel'));
        }else{
            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());

            redirect(site_url('artikel/edit_data/'.$id));
        }
    }

	public function check_username($title,$article_id=''){
      $where['Title'] = $title;
      $where['Status'] = 1;
      if($artikel_id == ''){
          $infodata = $this->MetaposModel->get_info('Titile', 'TR_Article', $where);
      }else{
          $infodata = $this->MetaposModel->get_info_where_and_where_not_in('ArticleId', 'TR_Article', $where,'ArticleId',$artikel_id);
      }

      if($infodata != false){
          $this->form_validation->set_message('check_check_username','Article is already at our data.');
          return false;
      }else{
          return true;
      }
  }
}
