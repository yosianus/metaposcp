<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function __construct(){

            parent::__construct();
            $this->load->model('MetaposModel');

       }

	public function index(){
		$data['header']=$this->load->view('template/header.php',NULL,TRUE);
		$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
		$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		$data['style']=$this->load->view('include/style.php',NULL,TRUE);

		$data['data_list'] = $this->MetaposModel->get_info('*','TR_Product');

		$this->load->view('page/product',$data);
	}

	public function add_data(){
		if(isset($_POST['_do_add_new'])){
            $this->_do_add_new();
        }else{
            $data['action'] = 'addnew';

						$data['header']=$this->load->view('template/header.php',NULL,TRUE);
						$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
						$data['script']=$this->load->view('include/script.php',NULL,TRUE);
						$data['style']=$this->load->view('include/style.php',NULL,TRUE);

            $this->load->view('page/produk_form',$data);
        }
	}

	private function _do_add_new(){
        $this->load->library('form_validation');
        $this->load->library('upload');

        $title = $this->input->post('ProductName');
        $des = $this->input->post('Description');
        $pic = $this->input->post('Pic');
        $alias = $this->input->post('Alias');

        $this->form_validation->set_rules('ProductName', 'ProductName', 'required');
        $this->form_validation->set_rules('Description', 'Description', 'required');
        $this->form_validation->set_rules('Pic', 'Pic', 'required');
        $this->form_validation->set_rules('Alias', 'Alias', 'required');

        //Upload File
            $config['upload_path']          = 'assets/image/';
            $config['allowed_types']        = 'jpg|png';
            $config['max_width']            = 1920;
            $config['max_height']           = 1080;
           
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->do_upload('pic');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $data['ProductName'] = $title;
            $data['Description'] = $des;
            $data['Pic'] = $pic;
            $data['Alias'] = $alias;

            $this->MetaposModel->add_new_data('TR_Product',$data);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Product is success added.');

            redirect(site_url('produk'));
        }else{
            $this->session->set_flashdata('productname', $title);
            $this->session->set_flashdata('description', $des);
            $this->session->set_flashdata('pic', $pic);
            $this->session->set_flashdata('alias', $alias);

            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());
            redirect(site_url('produk/add_data'));
        }
    }

	public function edit_data($id=""){
    if(isset($_POST['_do_edit'])){
        $this->_do_edit($id);
    }else{
				$data['header']=$this->load->view('template/header.php',NULL,TRUE);
				$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
				$data['script']=$this->load->view('include/script.php',NULL,TRUE);
				$data['style']=$this->load->view('include/style.php',NULL,TRUE);
        $where['ProductId'] = $id;

				$info_data = $this->MetaposModel->get_info('*','TR_Product',$where);
        $data['action'] = 'editdata';

				if($info_data != false){
					$data['info_data'] = $info_data->row();
				}else{
					redirect(base_url('produk'));
				}

        $this->load->view('page/produk_form',$data);
    }
	}

	public function _do_edit($id){
        $this->load->library('form_validation');

       $title = $this->input->post('ProductName');
        $des = $this->input->post('Description');
        $pic = $this->input->post('Pic');
        $alias = $this->input->post('Alias');

        $this->form_validation->set_rules('ProductName', 'ProductName', 'required');
        $this->form_validation->set_rules('Description', 'Description', 'required');
        $this->form_validation->set_rules('Pic', 'Pic', 'required');
        $this->form_validation->set_rules('Alias', 'Alias', 'required');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $where['ProductId'] = $id;

            $data['ProductName'] = $title;
            $data['Description'] = $des;
            $data['Pic'] = $photo;
            $data['Alias'] = $alias;
            
            $this->MetaposModel->edit_data('TR_Product',$data,$where);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Edit Product is Success.');
            $this->session->set_flashdata('msgtype', 'success');

            redirect(site_url('produk'));
        }else{
            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());

            redirect(site_url('produk/edit_data/'.$id));
        }
    }

	public function check_username($productname,$product_id=''){
      $where['ProductName'] = $productname;
      $where['Status'] = 1;
      if($product_id == ''){
          $infodata = $this->MetaposModel->get_info('ProductName', 'TR_Product', $where);
      }else{
          $infodata = $this->MetaposModel->get_info_where_and_where_not_in('ProductId', 'TR_Product', $where,'ProductId',$product);
      }

      if($infodata != false){
          $this->form_validation->set_message('check_check_username','UserName is already at our data.');
          return false;
      }else{
          return true;
      }
  }
}
