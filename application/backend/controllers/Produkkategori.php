<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produkkategori extends CI_Controller {

	public function __construct(){

            parent::__construct();
            $this->load->model('MetaposModel');

       }

	public function index(){
		$data['header']=$this->load->view('template/header.php',NULL,TRUE);
		$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
		$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		$data['style']=$this->load->view('include/style.php',NULL,TRUE);

		$data['data_list'] = $this->MetaposModel->get_info('*','TR_ProductCategory');

		$this->load->view('page/product_category',$data);
	}

	public function add_data(){
		if(isset($_POST['_do_add_new'])){
            $this->_do_add_new();
        }else{
            $data['action'] = 'addnew';

						$data['header']=$this->load->view('template/header.php',NULL,TRUE);
						$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
						$data['script']=$this->load->view('include/script.php',NULL,TRUE);
						$data['style']=$this->load->view('include/style.php',NULL,TRUE);

            $this->load->view('page/produk_kategori_form',$data);
        }
	}

	private function _do_add_new(){
        $this->load->library('form_validation');

        $title = $this->input->post('ProductCategoryName');
        $alias = $this->input->post('Alias');

        $this->form_validation->set_rules('Title', 'ProductCategoryName', 'required');
        $this->form_validation->set_rules('Alias', 'Alias', 'required');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $data['ProductCategoryName'] = $title;
            $data['Alias'] = $alias;

            $this->MetaposModel->add_new_data('TR_ProductCategory',$data);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Product is success added.');

            redirect(site_url('product'));
        }else{
            $this->session->set_flashdata('title', $title);
            $this->session->set_flashdata('alias', $alias);

            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());
            redirect(site_url('product_category/add_data'));
        }
    }

	public function edit_data($id=""){
    if(isset($_POST['_do_edit'])){
        $this->_do_edit($id);
    }else{
				$data['header']=$this->load->view('template/header.php',NULL,TRUE);
				$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
				$data['script']=$this->load->view('include/script.php',NULL,TRUE);
				$data['style']=$this->load->view('include/style.php',NULL,TRUE);
        $where['ProductCategoryId'] = $id;

				$info_data = $this->MetaposModel->get_info('*','TR_ProductCategory',$where);
        $data['action'] = 'editdata';

				if($info_data != false){
					$data['info_data'] = $info_data->row();
				}else{
					redirect(base_url('produkkategori'));
				}

        $this->load->view('page/produk_kategori_form',$data);
    }
	}

	public function _do_edit($id){
        $this->load->library('form_validation');

       $title = $this->input->post('ProductCategoryName');
        $alias = $this->input->post('Alias');

        $this->form_validation->set_rules('Title', 'Title', 'required');
        $this->form_validation->set_rules('Alias', 'Alias', 'required');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $where['ProductCategoryId'] = $id;

            $data['ProductCategoryName'] = $title;
            $data['Alias'] = $alias;
            
            $this->MetaposModel->edit_data('TR_ProductCategory',$data,$where);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Edit Product Category is Success.');
            $this->session->set_flashdata('msgtype', 'success');

            redirect(site_url('produkkategori'));
        }else{
            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());

            redirect(site_url('product_category/edit_data/'.$id));
        }
    }

	public function check_username($productname,$product_id=''){
      $where['ProductCategoryName'] = $productname;
      $where['Status'] = 1;
      if($product_id == ''){
          $infodata = $this->MetaposModel->get_info('ProductCategoryName', 'TR_ProductCategory', $where);
      }else{
          $infodata = $this->MetaposModel->get_info_where_and_where_not_in('ProductCategoryId', 'TR_ProductCategory', $where,'ProductCategoryId',$product);
      }

      if($infodata != false){
          $this->form_validation->set_message('check_check_username','Product Category Name is already at our data.');
          return false;
      }else{
          return true;
      }
  }
}
