<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct(){

            parent::__construct();
            $this->load->model('MetaposModel');

       }

	public function index(){
		$data['header']=$this->load->view('template/header.php',NULL,TRUE);
		$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
		$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		$data['style']=$this->load->view('include/style.php',NULL,TRUE);

		$data['data_list'] = $this->MetaposModel->get_info('*','TT_Contactus');

		$this->load->view('page/contactus',$data);
	}

	public function add_data(){
		if(isset($_POST['_do_add_new'])){
            $this->_do_add_new();
        }else{
            $data['action'] = 'addnew';

						$data['header']=$this->load->view('template/header.php',NULL,TRUE);
						$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
						$data['script']=$this->load->view('include/script.php',NULL,TRUE);
						$data['style']=$this->load->view('include/style.php',NULL,TRUE);

            $this->load->view('page/contact_form',$data);
        }
	}

	private function _do_add_new(){
        $this->load->library('form_validation');

       $subject = $this->input->post('Subject');
       $name = $this->input->post('Name');
       $phone = $this->input->post('Phone');
       $email = $this->input->post('Email');
       $message = $this->input->post('Message');

        $this->form_validation->set_rules('Subject', 'Subject', 'required');
        $this->form_validation->set_rules('Name', 'Name', 'required');
        $this->form_validation->set_rules('Phone', 'Phone', 'required');
        $this->form_validation->set_rules('Email', 'Email', 'required');
        $this->form_validation->set_rules('Message', 'Message', 'required');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $data['Subject'] = $subject;
            $data['Name'] = $name;
            $data['Phone'] = $phone;
            $data['Email'] = $email;
            $data['Message'] = $message;

            $this->MetaposModel->add_new_data('TT_Contactus',$data);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Contact is success added.');

            redirect(site_url('contact'));
        }else{
            $this->session->set_flashdata('Subject', $subject);
            $this->session->set_flashdata('Name', $Name);
            $this->session->set_flashdata('Phone', $phone);
            $this->session->set_flashdata('Email', $email);
            $this->session->set_flashdata('Message', $message);

            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());
            redirect(site_url('contact/add_data'));
        }
    }

	public function edit_data($id=""){
    if(isset($_POST['_do_edit'])){
        $this->_do_edit($id);
    }else{
				$data['header']=$this->load->view('template/header.php',NULL,TRUE);
				$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
				$data['script']=$this->load->view('include/script.php',NULL,TRUE);
				$data['style']=$this->load->view('include/style.php',NULL,TRUE);
        $where['ContactUsId'] = $id;

				$info_data = $this->MetaposModel->get_info('*','TT_Contactus',$where);
        $data['action'] = 'editdata';

				if($info_data != false){
					$data['info_data'] = $info_data->row();
				}else{
					redirect(base_url('contactus'));
				}

        $this->load->view('page/contact_form',$data);
    }
	}

	public function _do_edit($id){
        $this->load->library('form_validation');

       $subject = $this->input->post('Subject');
       $name = $this->input->post('Name');
       $phone = $this->input->post('Phone');
       $email = $this->input->post('Email');
       $message = $this->input->post('Message');

        $this->form_validation->set_rules('Subject', 'Subject', 'required');
        $this->form_validation->set_rules('Name', 'Name', 'required');
        $this->form_validation->set_rules('Phone', 'Phone', 'required');
        $this->form_validation->set_rules('Email', 'Email', 'required');
        $this->form_validation->set_rules('Message', 'Message', 'required');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $where['ContactUsId'] = $id;

            $data['Subject'] = $subject;
            $data['Name'] = $name;
            $data['Phone'] = $phone;
            $data['Email'] = $email;
            $data['Message'] = $message;
            
            $this->MetaposModel->edit_data('TT_Contactus',$data,$where);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Edit Contact is Success.');
            $this->session->set_flashdata('msgtype', 'success');

            redirect(site_url('contact'));
        }else{
            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());

            redirect(site_url('contact/edit_data/'.$id));
        }
    }

	public function check_username($name,$contactus_id=''){
      $where['Name'] = $name;
      $where['Status'] = 1;
      if($subscribe_id == ''){
          $infodata = $this->MetaposModel->get_info('Email', 'TT_Contactus', $where);
      }else{
          $infodata = $this->MetaposModel->get_info_where_and_where_not_in('ContactUsId', 'TT_Contactus', $where,'ContactUsId',$subscribe_id);
      }

      if($infodata != false){
          $this->form_validation->set_message('check_check_username','Name is already at our data.');
          return false;
      }else{
          return true;
      }
  }
}
