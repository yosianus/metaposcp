<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscribe extends CI_Controller {

	public function __construct(){

            parent::__construct();
            $this->load->model('MetaposModel');

       }

	public function index(){
		$data['header']=$this->load->view('template/header.php',NULL,TRUE);
		$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
		$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		$data['style']=$this->load->view('include/style.php',NULL,TRUE);

		$data['data_list'] = $this->MetaposModel->get_info('*','TT_Subscription');

		$this->load->view('page/subs',$data);
	}

	public function add_data(){
		if(isset($_POST['_do_add_new'])){
            $this->_do_add_new();
        }else{
            $data['action'] = 'addnew';

						$data['header']=$this->load->view('template/header.php',NULL,TRUE);
						$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
						$data['script']=$this->load->view('include/script.php',NULL,TRUE);
						$data['style']=$this->load->view('include/style.php',NULL,TRUE);

            $this->load->view('page/subs_form',$data);
        }
	}

	private function _do_add_new(){
        $this->load->library('form_validation');

        $email = $this->input->post('Email');

        $this->form_validation->set_rules('Email', 'Email', 'required');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $data['Email'] = $email;

            $this->MetaposModel->add_new_data('TT_Subscription',$data);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Subscription is success added.');

            redirect(site_url('subscibe'));
        }else{
            $this->session->set_flashdata('email', $email);

            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());
            redirect(site_url('subscribe/add_data'));
        }
    }

	public function edit_data($id=""){
    if(isset($_POST['_do_edit'])){
        $this->_do_edit($id);
    }else{
				$data['header']=$this->load->view('template/header.php',NULL,TRUE);
				$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
				$data['script']=$this->load->view('include/script.php',NULL,TRUE);
				$data['style']=$this->load->view('include/style.php',NULL,TRUE);
        $where['SubscriptionId'] = $id;

				$info_data = $this->MetaposModel->get_info('*','TT_Subscription',$where);
        $data['action'] = 'editdata';

				if($info_data != false){
					$data['info_data'] = $info_data->row();
				}else{
					redirect(base_url('subscibe'));
				}

        $this->load->view('page/subs_form',$data);
    }
	}

	public function _do_edit($id){
        $this->load->library('form_validation');

       $email = $this->input->post('Email');

        $this->form_validation->set_rules('Email', 'Email', 'required');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $where['SubscriptionId'] = $id;

            $data['Email'] = $email;
            
            $this->MetaposModel->edit_data('TT_Subscription',$data,$where);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Edit Subscribe is Success.');
            $this->session->set_flashdata('msgtype', 'success');

            redirect(site_url('subscibe'));
        }else{
            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());

            redirect(site_url('subscribe/edit_data/'.$id));
        }
    }

	public function check_username($email,$subscribe_id=''){
      $where['Email'] = $email;
      $where['Status'] = 1;
      if($subscribe_id == ''){
          $infodata = $this->MetaposModel->get_info('Email', 'TT_Subscription', $where);
      }else{
          $infodata = $this->MetaposModel->get_info_where_and_where_not_in('SubscriptionId', 'TT_Subscription', $where,'SubscriptionId',$subscribe_id);
      }

      if($infodata != false){
          $this->form_validation->set_message('check_check_username','Email is already at our data.');
          return false;
      }else{
          return true;
      }
  }
}
