<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MetaposController extends CI_Controller {


	 public function __construct(){

            parent::__construct();
			$this->load->model('MetaposModel');

       }

	//Buat tampilin halaman SignIn
	public function index(){
	 	 $data['header']=$this->load->view('template/header.php',NULL,TRUE);
	 	 $data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
	 	 $data['script']=$this->load->view('include/script.php',NULL,TRUE);
	 	 $data['style']=$this->load->view('include/style.php',NULL,TRUE);
	 	$this->load->view('page/login.php', $data);
	}

    public function login(){
        $this->load->library('form_validation');
        $this->load->library('upload');

        $this->form_validation->set_rules('username', 'username', 'valid_email',
            array(
                'valid_email'=>'You must enter a valid username!'
            )
        );

         $this->form_validation->set_rules('password','password','min_length[6]',
            array(
                'min_length'=>'Please input minimal 6 character!'
            )
        );

        if ($this->form_validation->run()==FALSE) {
            $this->session->set_flashdata('msg', "login_failed");
            $this->index();
        }
        else{
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $error = false;
            $result = $this->MetaposModel->get_login($username);

            if(sizeof($result) > 0){
                $hash = $result[0]['password'];

                if(password_verify($password, $hash)){
                    $this->session->set_userdata('login_user', $result[0]['username']);
                    $this->session->set_userdata('login_userid', $result[0]['userid']);

                    $this->session->set_flashdata('msg', "login_success");
                    redirect(base_url().'MetaposController/home');
                }
                else{
                    $this->session->set_flashdata('msg', "login_wrong");
                    $this->index();
                }
            }
            else{
                $this->session->set_flashdata('msg', "login_wrong");
                $this->index();
            }
        }
    }

	//Buat tampilin home
	public function home(){
		$data['header']=$this->load->view('template/header.php',NULL,TRUE);
		$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
		$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		$data['style']=$this->load->view('include/style.php',NULL,TRUE);
		$this->load->view('page/home.php', $data);
	}

    public function about(){
        $data['header']=$this->load->view('template/header.php',NULL,TRUE);
        $data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
        $data['script']=$this->load->view('include/script.php',NULL,TRUE);
        $data['style']=$this->load->view('include/style.php',NULL,TRUE);
        $this->load->view('page/about.php', $data);
    }

    public function contact(){
        $data['header']=$this->load->view('template/header.php',NULL,TRUE);
        $data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
        $data['script']=$this->load->view('include/script.php',NULL,TRUE);
        $data['style']=$this->load->view('include/style.php',NULL,TRUE);
        $this->load->view('page/kontak.php', $data);
    }

    public function pricing(){
        $data['header']=$this->load->view('template/header.php',NULL,TRUE);
        $data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
        $data['script']=$this->load->view('include/script.php',NULL,TRUE);
        $data['style']=$this->load->view('include/style.php',NULL,TRUE);
        $this->load->view('page/pricing.php', $data);
    }

}
