<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {


	 public function __construct(){

            parent::__construct();
			$this->load->model('MetaposModel');

       }

	//Buat tampilin halaman Home
	public function index(){
	 	 $data['header']=$this->load->view('template/header.php',NULL,TRUE);
	 	 $data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
	 	 $data['script']=$this->load->view('include/script.php',NULL,TRUE);
	 	 $data['style']=$this->load->view('include/style.php',NULL,TRUE);
	 	$this->load->view('page/home.php', $data);
	}
}
