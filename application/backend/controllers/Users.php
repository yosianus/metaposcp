<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct(){

            parent::__construct();
            $this->load->model('MetaposModel');

       }

	public function index(){
		$data['header']=$this->load->view('template/header.php',NULL,TRUE);
		$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
		$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		$data['style']=$this->load->view('include/style.php',NULL,TRUE);

		$data['data_list'] = $this->MetaposModel->get_info('*','TR_User');

		$this->load->view('page/users',$data);
	}

	public function add_data(){
		if(isset($_POST['_do_add_new'])){
            $this->_do_add_new();
        }else{
            $data['action'] = 'addnew';

						$data['header']=$this->load->view('template/header.php',NULL,TRUE);
						$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
						$data['script']=$this->load->view('include/script.php',NULL,TRUE);
						$data['style']=$this->load->view('include/style.php',NULL,TRUE);

            $this->load->view('page/users_form',$data);
        }
	}

	private function _do_add_new(){
        $this->load->library('form_validation');

        $username = $this->input->post('UserName');
        $firstname = $this->input->post('FirstName');
        $lastname = $this->input->post('LastName');
        $password = $this->input->post('Password');
        $repassword = $this->input->post('RePassword');

        $this->form_validation->set_rules('UserName', 'UserName', 'required|callback_check_username');
        $this->form_validation->set_rules('FirstName', 'FirstName', 'required');
        $this->form_validation->set_rules('Password', 'Password', 'required');
        $this->form_validation->set_rules('RePassword', 'Re-Password', 'required|matches[Password]');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $data['UserName'] = $username;
            $data['FirstName'] = $firstname;
            $data['LastName'] = $lastname;
            $data['Password'] = sha1(md5($password));

            $this->MetaposModel->add_new_data('TR_User',$data);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Users is success added.');

            redirect(site_url('users'));
        }else{
            $this->session->set_flashdata('username', $username);
            $this->session->set_flashdata('firstname', $firstname);
            $this->session->set_flashdata('lastname', $lastname);

            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());
            redirect(site_url('users/add_data'));
        }
    }

	public function edit_data($id=""){
    if(isset($_POST['_do_edit'])){
        $this->_do_edit($id);
    }else{
				$data['header']=$this->load->view('template/header.php',NULL,TRUE);
				$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
				$data['script']=$this->load->view('include/script.php',NULL,TRUE);
				$data['style']=$this->load->view('include/style.php',NULL,TRUE);
        $where['UserId'] = $id;

				$info_data = $this->MetaposModel->get_info('*','TR_User',$where);
        $data['action'] = 'editdata';

				if($info_data != false){
					$data['info_data'] = $info_data->row();
				}else{
					redirect(base_url('users'));
				}

        $this->load->view('page/users_form',$data);
    }
	}

	public function _do_edit($id){
        $this->load->library('form_validation');

        $username = $this->input->post('UserName');
        $firstname = $this->input->post('FirstName');
        $lastname = $this->input->post('LastName');
        $password = $this->input->post('Password');
        $repassword = $this->input->post('RePassword');

        $this->form_validation->set_rules('UserName', 'UserName', 'required|callback_check_username['.$id.']');
        $this->form_validation->set_rules('FirstName', 'FirstName', 'required');
        if($password != '' || $repassword != ''){
            $this->form_validation->set_rules('Password', 'Password', 'required');
            $this->form_validation->set_rules('RePassword', 'Re-Password', 'required|matches[Password]');
        }

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $where['UserId'] = $id;

            $data['UserName'] = $username;
            $data['FirstName'] = $firstname;
            $data['LastName'] = $lastname;
            if($password != ''){
                $data['Password'] = sha1(md5($password));
            }
            $this->MetaposModel->edit_data('TR_User',$data,$where);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Edit User is Success.');
            $this->session->set_flashdata('msgtype', 'success');

            redirect(site_url('users'));
        }else{
            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());

            redirect(site_url('users/edit_data/'.$id));
        }
    }

	public function check_username($username,$user_id=''){
      $where['UserName'] = $username;
      $where['Status'] = 1;
      if($user_id == ''){
          $infodata = $this->MetaposModel->get_info('UserName', 'TR_User', $where);
      }else{
          $infodata = $this->MetaposModel->get_info_where_and_where_not_in('UserId', 'TR_User', $where,'UserId',$user_id);
      }

      if($infodata != false){
          $this->form_validation->set_message('check_check_username','UserName is already at our data.');
          return false;
      }else{
          return true;
      }
  }
}
