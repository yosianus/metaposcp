<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonial extends CI_Controller {

	public function __construct(){

            parent::__construct();
            $this->load->model('MetaposModel');

       }

	public function index(){
		$data['header']=$this->load->view('template/header.php',NULL,TRUE);
		$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
		$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		$data['style']=$this->load->view('include/style.php',NULL,TRUE);

		$data['data_list'] = $this->MetaposModel->get_info('*','TT_Testimonial');

		$this->load->view('page/testimoni',$data);
	}

	public function add_data(){
		if(isset($_POST['_do_add_new'])){
            $this->_do_add_new();
        }else{
            $data['action'] = 'addnew';

						$data['header']=$this->load->view('template/header.php',NULL,TRUE);
						$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
						$data['script']=$this->load->view('include/script.php',NULL,TRUE);
						$data['style']=$this->load->view('include/style.php',NULL,TRUE);

            $this->load->view('page/testimoni_form',$data);
        }
	}

	private function _do_add_new(){
        $this->load->library('form_validation');

        $title = $this->input->post('ClientName');
        $logo = $this->input->post('ClientLogo');
        $testi = $this->input->post('Testimonial');

        $this->form_validation->set_rules('ClientName', 'ClientName', 'required');
        $this->form_validation->set_rules('ClientLogo', 'ClientLogo', 'required');
        $this->form_validation->set_rules('Testimonial', 'Testimonial', 'required');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $data['ClientName'] = $title;
            $data['ClientLogo'] = $logo;
            $data['Testimonial'] = $testi;

            $this->MetaposModel->add_new_data('TT_Testimonial',$data);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Testimonial is success added.');

            redirect(site_url('testimoni'));
        }else{
            $this->session->set_flashdata('clientname', $title);
            $this->session->set_flashdata('clientlogo', $logo);
            $this->session->set_flashdata('testimonial', $testi);

            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());
            redirect(site_url('testimoni/add_data'));
        }
    }

	public function edit_data($id=""){
    if(isset($_POST['_do_edit'])){
        $this->_do_edit($id);
    }else{
				$data['header']=$this->load->view('template/header.php',NULL,TRUE);
				$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
				$data['script']=$this->load->view('include/script.php',NULL,TRUE);
				$data['style']=$this->load->view('include/style.php',NULL,TRUE);
        $where['TestimonialId'] = $id;

				$info_data = $this->MetaposModel->get_info('*','TT_Testimonial',$where);
        $data['action'] = 'editdata';

				if($info_data != false){
					$data['info_data'] = $info_data->row();
				}else{
					redirect(base_url('testimoni'));
				}

        $this->load->view('page/testimoni_form',$data);
    }
	}

	public function _do_edit($id){
        $this->load->library('form_validation');

       $title = $this->input->post('ClientName');
        $logo = $this->input->post('ClientLogo');
        $testi = $this->input->post('Testimonial');

        $this->form_validation->set_rules('ClientName', 'ClientName', 'required');
        $this->form_validation->set_rules('ClientLogo', 'ClientLogo', 'required');
        $this->form_validation->set_rules('Testimonial', 'Testimonial', 'required');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $where['TestimonialId'] = $id;

            $data['ClientName'] = $title;
            $data['ClientLogo'] = $logo;
            $data['Testimonial'] = $testi;
            
            $this->MetaposModel->edit_data('TT_Testimonial',$data,$where);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Edit Testimonial is Success.');
            $this->session->set_flashdata('msgtype', 'success');

            redirect(site_url('testimoni'));
        }else{
            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());

            redirect(site_url('testimoni/edit_data/'.$id));
        }
    }

	public function check_username($clientname,$client_id=''){
      $where['ClientName'] = $clientname;
      $where['Status'] = 1;
      if($client_id == ''){
          $infodata = $this->MetaposModel->get_info('ClientName', 'TT_Testimonial', $where);
      }else{
          $infodata = $this->MetaposModel->get_info_where_and_where_not_in('TestimonialId', 'TT_Testimonial', $where,'TestimonialId',$testimonial);
      }

      if($infodata != false){
          $this->form_validation->set_message('check_check_username','UserName is already at our data.');
          return false;
      }else{
          return true;
      }
  }
}
