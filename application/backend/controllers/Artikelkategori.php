<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikelkategori extends CI_Controller {

	public function __construct(){

            parent::__construct();
            $this->load->model('MetaposModel');

       }

	public function index(){
		$data['header']=$this->load->view('template/header.php',NULL,TRUE);
		$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
		$data['script']=$this->load->view('include/script.php',NULL,TRUE);
		$data['style']=$this->load->view('include/style.php',NULL,TRUE);

		$data['data_list'] = $this->MetaposModel->get_info('*','TR_ArticleCategory');

		$this->load->view('page/artikel_kategori',$data);
	}

	public function add_data(){
		if(isset($_POST['_do_add_new'])){
            $this->_do_add_new();
        }else{
            $data['action'] = 'addnew';

						$data['header']=$this->load->view('template/header.php',NULL,TRUE);
						$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
						$data['script']=$this->load->view('include/script.php',NULL,TRUE);
						$data['style']=$this->load->view('include/style.php',NULL,TRUE);

            $this->load->view('page/artikel_kategori_form',$data);
        }
	}

	private function _do_add_new(){
        $this->load->library('form_validation');

        $title = $this->input->post('ArticleCategoryName');
        $alias = $this->input->post('Alias');

        $this->form_validation->set_rules('ArticleCategoryName', 'ArticleCategoryName', 'required');
        $this->form_validation->set_rules('Alias', 'Alias', 'required');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $data['ArticleCategoryName'] = $title;
            $data['Alias'] = $alias;

            $this->MetaposModel->add_new_data('TR_ArticleCategory',$data);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Article Category is success added.');

            redirect(site_url('artikelkategori'));
        }else{
            $this->session->set_flashdata('articlecategoryname', $title);
            $this->session->set_flashdata('alias', $alias);

            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());
            redirect(site_url('artikelkategori/add_data'));
        }
    }

	public function edit_data($id=""){
    if(isset($_POST['_do_edit'])){
        $this->_do_edit($id);
    }else{
				$data['header']=$this->load->view('template/header.php',NULL,TRUE);
				$data['footer']=$this->load->view('template/footer.php',NULL,TRUE);
				$data['script']=$this->load->view('include/script.php',NULL,TRUE);
				$data['style']=$this->load->view('include/style.php',NULL,TRUE);
        $where['ArticleCategoryId'] = $id;

				$info_data = $this->MetaposModel->get_info('*','TR_ArticleCategory',$where);
        $data['action'] = 'editdata';

				if($info_data != false){
					$data['info_data'] = $info_data->row();
				}else{
					redirect(base_url('artikelkategori'));
				}

        $this->load->view('page/artikel_kategori_form',$data);
    }
	}

	public function _do_edit($id){
        $this->load->library('form_validation');

       $title = $this->input->post('ArticleCategoryName');
        $alias = $this->input->post('Alias');

        $this->form_validation->set_rules('ArticleCategoryName', 'ArticleCategoryName', 'required');
        $this->form_validation->set_rules('Alias', 'Alias', 'required');

        if($this->form_validation->run() == true) {
            $this->db->trans_start();

            $where['ArticleCategoryId'] = $id;

            $data['ArticleCategoryName'] = $title;
            $data['Alias'] = $alias;
            
            $this->MetaposModel->edit_data('TR_ArticleCategory',$data,$where);

            $this->db->trans_complete();

            $this->session->set_flashdata('msg', 'Edit Article Category is Success.');
            $this->session->set_flashdata('msgtype', 'success');

            redirect(site_url('artikelkategori'));
        }else{
            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg', validation_errors());

            redirect(site_url('artikelkategori/edit_data/'.$id));
        }
    }

	public function check_username($articlename,$article_id=''){
      $where['Title'] = $articlename;
      $where['Status'] = 1;
      if($artikel_id == ''){
          $infodata = $this->MetaposModel->get_info('Titile', 'TR_Article', $where);
      }else{
          $infodata = $this->MetaposModel->get_info_where_and_where_not_in('ArticleCategoryId', 'TR_ArticleCategory', $where,'ArticleCategoryId',$artikel_id);
      }

      if($infodata != false){
          $this->form_validation->set_message('check_check_username','UserName is already at our data.');
          return false;
      }else{
          return true;
      }
  }
}
