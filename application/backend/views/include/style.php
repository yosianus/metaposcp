<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<link href="<?php echo base_url();?>../assets/css/animate.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php echo base_url();?>../assets/css/animate-velocity.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php echo base_url();?>../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../assets/css/bootstrap-table.css">

	<link href="<?php echo base_url();?>../assets/css/engage.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php echo base_url();?>../assets/css/filter.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php echo base_url();?>../assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php echo base_url();?>../assets/css/fontawesome-all.min.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php echo base_url();?>../assets/css/footable.bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php echo base_url();?>../assets/css/intlTelInput.css" rel="stylesheet" type="text/css" media="all" />

	<!-- Data table CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../assets/css/jquery.dataTables.min.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../assets/css/jquery.datetimepicker.min.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../assets/css/jquery.fancybox.min.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../assets/css/jquery.toast.min.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../assets/css/jquery-ui.min.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../assets/css/jquery-ui.structure.min.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../assets/css/jquery-ui.theme.min.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../assets/css/jssocial.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../assets/css/jssocial-theme-flat.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../assets/css/linea-icon.css">

	<link href="<?php echo base_url();?>../assets/css/material-design-iconic-font.min.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php echo base_url();?>../assets/css/pe-icon-7-stroke.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php echo base_url();?>../assets/css/pe-icon-7-styles.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php echo base_url();?>../assets/css/rnt_css.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php echo base_url();?>../assets/css/simple-line-icons.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php echo base_url();?>../assets/css/slick.css" rel="stylesheet" type="text/css" media="all" />

	<!-- Custom CSS -->
	<link href="<?php echo base_url();?>../assets/css/style.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php echo base_url();?>../assets/css/sweetalert.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php echo base_url();?>../assets/css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />

