	<!-- JavaScript -->
	<script src="<?php echo base_url();?>../assets/js/animate-velocity.js"></script>

	<script src="<?php echo base_url();?>../assets/js/animate-wow.js"></script>
    <!-- jQuery -->
    <script src="<?php echo base_url();?>../assets/js/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>../assets/js/bootstrap.min.js"></script>

	<!-- Data table JavaScript -->
	<script src="<?php echo base_url('../assets/js/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?php echo base_url('../assets/js/dataTables-data.js'); ?>"></script>
	<script src="<?php echo base_url('../assets/js/bootstrap-table.min.js'); ?>"></script>
	<script src="<?php echo base_url('../assets/js/bootstrap-table-data.js'); ?>"></script>

	<!-- Slimscroll JavaScript -->
	<script src="<?php echo base_url('../assets/js/jquery.slimscroll.js'); ?>"></script>

	<!-- Progressbar Animation JavaScript -->
	<script src="<?php echo base_url();?>../assets/js/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url();?>../assets/js/jquery.counterup.min.js"></script>

	<!-- Owl JavaScript -->
	<script src="<?php echo base_url();?>../assets/js/owl.carousel.min.js"></script>

	<!-- Switchery JavaScript -->
	<script src="<?php echo base_url();?>../assets/js/switchery.min.js"></script>

	<!-- Toast JavaScript -->
	<script src="<?php echo base_url();?>../assets/js/jquery.toast.min.js"></script>
