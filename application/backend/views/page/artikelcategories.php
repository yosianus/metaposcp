<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<title>MetaPOS</title>
	

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	
	<?php
		echo $style;
		echo $script;
	?>

<style>
	body {
   	background-color: white;
	}
table {
    border: 1px solid black;
    padding: 5px;
}
table {
    border-spacing: 15px;
}

</style>
</head>

<body>
<div class="wrap-body">
	<?php 
		echo $header;
	?>
				<!-- Main Content -->
				<div class="container">

				<<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-8 col-md-9 col-sm-9 col-xs-18">
						<br>
						<br>
						<br>
						<br>
					</div>

					<!-- Breadcrumb -->
					<div class="col-lg-12 col-sm-11 col-md-11 col-xs-15">
					  <ol class="breadcrumb">
						<li><a href="<?php echo site_url("metaposController/home");?>">Dashboard</a></li>
						<li><a href="<?php echo site_url("metaposController/home");?>"><span>Article Management</span></a></li>
						<li class="active"><span>Article Categories</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->

				</div>
				<!-- /Title -->

				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="pull-left">
									<h5 class="panel-heading txt-dark">Article Data</h5>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<!--button add sama delete -->
											<body>
												<div class="w3-container" align="right">

  													<!-- button add
  													-->

  													<a href="<?php echo site_url("metaposController/add");?>" class="btn btn-default" role="button">Add</a>
  													<!-- button add -->

  													<!-- button delete -->
  													<button type="button" class="btn btn-default" id="myBtn">Delete</button>
  														<!-- Modal -->
  														<div class="modal fade" id="myModal" role="dialog">
   			 												<div class="modal-dialog">
      														<!-- Modal content-->
      															<div class="modal-content">
        															<div class="modal-header">
          																<button type="button" class="close" data-dismiss="modal">&times;</button>
         																	<h4 align="center" class="modal-title">Remove Data</h4>
        															</div>
        															<div class="modal-body">
          																<p align="center">Are you sure?</p>
        															</div>
        															<div class="modal-footer">
        																<button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
          																<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        															</div>
      															</div>      
    														</div>
  														</div> 
														<script>
															$(document).ready(function(){
    															$("#myBtn").click(function(){
        															$("#myModal").modal({show: true});
    															});
															});
														</script>
														<!-- button delete -->
												</div>
											</body>
											<!--button add sama delete -->

											<table id="datable_1" class="table table-hover display  pb-30" class="table table-bordered" data-click-to-select="true">
												<thead>
													<tr>
														<th>Nama Artikel</th>
														<th>Alias</th>
														<th>Status</th>
														<th>Edit</th>
													</tr>
												</thead>
												<tbody>
													<?php
													if($from ="artikelcategories"){
          													foreach ($artikel_kategori as $a) {
            													$id_artikel = $a['ArticleCategoryId'];
            													$nama_artikel = $a['ArticleCategoryName'];
            													$alias = $a['Alias'];
            													$status = $a['Status'];

            													echo "<tr>";
            													echo "<td>".$nama_artikel."</td>";
            													echo "<td>".$alias."</td>";
            													echo "<td>".$status."</td>";

            													
            													echo "</tr>";
          													} 
          												}
        												 
    												?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->
			</div>
		</div>
        <!-- /Main Content -->
<!-- footer -->
	<div class="footer">
		<div class="footer-top">
			<div class="container">
						<p>2018 &copy; MetaPOS </p>
				</div>
			</div>
		</div>
	</div>
<!-- //footer -->
    </div>
    <!-- /#wrapper -->


</body>

</html>
