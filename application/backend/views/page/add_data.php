<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<title>MetaPOS</title>


	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<?php
		echo $style;
		echo $script;
	?>

<style>
	body {
   	background-color: white;
	}
/* Add padding to containers */
.container {
    padding: 10px;
    background-color: white;
}

/* Full-width input fields */
input[type=text], input[type=password] {
    width: 100%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
    background-color: #ddd;
    outline: none;
}

/* Overwrite default styles of hr */
hr {
    border: 1px solid #f1f1f1;
    margin-bottom: 25px;
}

/* Set a style for the submit button */
.registerbtn {
    background-color: #C0C0C0;
    color: black;
    padding: 10px 15px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
}

.registerbtn:hover {
    opacity: 10;
}

/* Set a style for the submit button */
.cancelbtn {
    background-color: #C0C0C0;
    color: black;
    padding: 10px 15px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
}

.cancelbtn:hover {
    opacity: 10;
}

</style>
</head>

<body>

	<?php
		echo $header;
	?>
				<!-- Main Content -->

				<div class="container">


				<<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-8 col-md-9 col-sm-9 col-xs-18">
						<br>
						<br>
						<br>
						<br>

					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-12 col-sm-11 col-md-11 col-xs-15">
					  <ul class="breadcrumb">
						<li><a href="<?php echo site_url("metaposController/home");?>">Dashboard</a></li>
						<li><a href="#"><span>Article Management</span></a></li>
						<li><a href="<?php echo site_url("metaposController/article_category");?>"><span>Article Categories</span></a></li>
						<li class="#"><span>Add Data</span></li>
					  </ul>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="pull-left">
								<h3 class="panel-heading txt-dark">Add Data</h3>
							</div>
							<div class="clearfix"></div>
						</div>
							<div class="panel-body">

								<form role="form" action="<?php echo base_url();?>index.php/metaposController/register" method="post" enctype="multipart/form-data">
									<hr>
									<label for="name">Name</label>
									<input type="text" placeholder="Enter Article Name" name="name" required="">
									<span><?php echo form_error('name', '<div class="alert-danger">', '</div>');?></span>

									<label for="alias"><b>Alias</b></label>
									<input type="text" placeholder=" " name="alias" required="">
									<span><?php echo form_error('alias', '<div class="alert-danger">', '</div>');?></span>

									<label for="status"><b>Status</b></label>
									<input type="text" placeholder="Enter Article Status" name="status" required="">
									<span><?php echo form_error('status', '<div class="alert-danger">', '</div>');?></span>

									<input type="submit" value="Submit" />

									<!-- <button type="button" class="btn btn-default" data-dismiss="modal" >Submit</button>
									<button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button> -->
							</div>
					</div>
				</div>
				<!-- /Row -->
			</div>
		</div>
        <!-- /Main Content -->
<!-- footer -->
	<div class="footer">
		<div class="footer-top">
			<div class="container">
						<p>2018 &copy; MetaPOS </p>
				</div>
			</div>
		</div>
	</div>
<!-- //footer -->
    </div>
    <!-- /#wrapper -->


</body>

</html>
