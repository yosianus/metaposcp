<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<title>MetaPOS</title>
	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<?=$style.$script;?>

	<style>
	  body {
		    background-color: white;
		    }
		table {
		    border: 1px solid black;
		    padding: 5px;
		}
		table {
		    border-spacing: 15px;
		}
		.page-wrapper{
			background-color: #ffffff !important;
		}
	</style>
</head>

<body>
	<?=$header;?>
	<?php
	if($action == 'addnew'){
	    $col_username = $this->session->flashdata('username');
	    $col_firstname = $this->session->flashdata('firstname');
	    $col_lastname = $this->session->flashdata('lastname');
	    $buttonname = "_do_add_new";
	    $savesite = base_url('users/add_data');
	    $langaction = "Add New";
	}else{
	    $col_username = $info_data->UserName;
	    $col_firstname = $info_data->FirstName;
	    $col_lastname = $info_data->LastName;
	    $buttonname = "_do_edit";
	    $savesite = site_url('users/edit_data/'.$info_data->UserId);
	    $langaction = "Edit";
	}
	?>
	<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h5 class="txt-dark">Users</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
						<ul class="breadcrumb">
							<li><a href="<?=base_url();?>">Dashboard</a></li>
								<li><a href="<?=base_url("users");?>">Users</a></li>
							<li class="active"><span><?=$langaction?></span></li>
						</ul>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<div class="row">
						<div class="col-sm-12">
							<form class="" role="form" method="POST" action="<?=$savesite?>">
								<div class="panel panel-primary">
										<div class="panel-heading">
												<div class="pull-left">
														<h5 class="panel-heading txt-light">Users Form</h5>
												</div>
												<div class="clearfix"></div>
										</div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label mb-10 text-left">UserName</label>
														<input type="text" class="form-control" name="UserName" value="<?=$col_username?>" required>
													</div>
													<div class="form-group">
														<label class="control-label mb-10 text-left">FirstName</label>
														<input type="text" class="form-control" name="FirstName" value="<?=$col_firstname?>" required>
													</div>
													<div class="form-group">
														<label class="control-label mb-10 text-left">LastName</label>
														<input type="text" class="form-control" name="LastName" value="<?=$col_lastname?>">
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label mb-10 text-left">Password</label>
														<input type="password" class="form-control" name="Password" value="" >
													</div>
													<div class="form-group">
														<label class="control-label mb-10 text-left">Re-Password</label>
														<input type="password" class="form-control" name="RePassword" value="" >
													</div>
												</div>
											</div>
												<div class="row">
													<div class="col-md-12">
														<button class="btn btn-primary" type="submit" name="<?=$buttonname?>">Save</button>
	                          <a href="<?=base_url("users")?>" class="btn btn-default"><i class="fa fa-left"></i> Cancel</a>
													</div>
												</div>
										</div>
								</div>
							</form>
						</div>
				</div>
		</div>
	</div>
	<!-- footer -->
    <div class="footer">
        <div class="footer-top">
            <div class="container">
            	<p>2018 &copy; MetaPOS </p>
            </div>
        </div>
    </div>
</body>
</html>
