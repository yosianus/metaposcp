<!-- isian untuk contact number, admin email, dan sosial media -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>Sosial Media | MetaPOS</title>
	<meta name="description" content="Jetson is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Jetson Admin, Jetsonadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<?php
		echo $style;
		echo $script;
	?>
<style>
body {
    background-color: white;
}

body {font-family: Arial, Helvetica, sans-serif;}

input[type=text], select, textarea {
    width: 100%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    margin-top: 6px;
    margin-bottom: 16px;
    resize: vertical;
}

* {
    box-sizing: border-box;
}

/* Create two equal columns that floats next to each other */
.column {
    float: left;
    width: 40%;
    padding: 10px;
    height: 280px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}


</style>
</head>

<body>

	<?php
		echo $header;
	?>

	<!-- Main Content -->

			<div class="container">

				<<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-8 col-md-9 col-sm-9 col-xs-18">
						<br>
						<br>
						<br>
						<br>

					</div>

				</div>
				<!-- /Title -->

				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="pull-left">
								<!-- <h4 class="panel-heading txt-dark">About Us</h4> -->
							</div>
							<div class="clearfix"></div>
						</div>
							<div class="panel-body">
								<div class="row">
  									<!-- <div class="column" style="background-color:#ffffff;">
    									<h4>Contact Number</h4>
   											<p><input type="text" name="FirstName" value="021-55777677"></p>
   											<br>
   										<h4>Admin E-mail</h4>
   											<p><input type="text" name="FirstName" value="blabla@gmail.com"></p>
  									</div>
  									<div class="column" style="background-color:#ffffff;">
    									<h4>Social Media</h4>
    										<p>Facebook <input type="text" name="facebook" value="021-55777677"></p>
    										<br>
    										<p>Instagram <input type="text" name="instagram" value="021-55777677"></p><br>
    										<p>Twitter <input type="text" name="twitter" value="021-55777677"></p>
									</div> -->
									<div class="column" style="background-color:#ffffff;">
    									<label for="socialmedia"><h5>Social Media</h5></label>
    									<br>
    									<p>Instagram <input type="text" id="instagram" name="instagram" placeholder="@adminadmin"></p>
    									<p>Facebook <input type="text" id="facebook" name="facebook" placeholder="Admin"></p>
    									<p>Twitter <input type="text" id="twitter" name="twitter" placeholder="@adminadmin"></p>
    									<br>
    								</div>

									<div class="column" style="background-color:#ffffff;">
									<label for="contact"><h5>Contact Number</h5></label>
    									<input type="text" id="contact" name="contactnumber" placeholder="021-55777678">
    								<br>
    								<label for="email"><h5>Admin E-mail</h5></label>
    									<input type="text" id="admin" name="adminemail" placeholder="admin@gmail.com">
    								</div>
    							</div>
    							<br>
    							<br>
    							<br>
							<button type="button" class="btn btn-default" data-dismiss="modal" >Edit</button>
							</div>
					</div>
					</div>
				</div>
			</div>
			<br>
			<br>

</script>
	<?php
		echo $footer;
	?>
</body>

</html>
