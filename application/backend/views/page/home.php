<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<title>MetaPOS</title>

	<meta name="keywords" content="Welcome to MetaPos">
	<meta name="description" content="Welcome to MetaPos">
	<meta property="og:title" content="Welcome to MetaPos">
	<meta property="og:image" content="assets/image/metaposss.png">
	<meta property="og:site_name" content="MetaPos">
	<meta property="og:description" content="Welcome to MetaPos">
	<meta name="author" content="hencework"/>

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/image/metaposss.png" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<?php
		echo $style;
		echo $script;
	?>
<style>
body {
    background-color: white;
}
</style>
</head>

<body>

	<?php
		echo $header;
	?>

	<!-- Main Content -->

			<div class="container">


				<<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-8 col-md-9 col-sm-9 col-xs-18">
						<br>
						<br>
						<br>
						<br>

					</div>

				</div>
				<!-- /Title -->

				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">

							<div class="pull-left">
								<h6 class="panel-heading txt-dark">Home Page</h6>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>

</script>
	<?php
		echo $footer;
	?>
</body>

</html>
