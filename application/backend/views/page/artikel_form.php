<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<title>MetaPOS</title>
	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<?=$style.$script;?>

	<style>
	  body {
		    background-color: white;
		    }
		table {
		    border: 1px solid black;
		    padding: 5px;
		}
		table {
		    border-spacing: 15px;
		}
		.page-wrapper{
			background-color: #ffffff !important;
		}
	</style>
</head>

<body>
	<?=$header;?>
	<?php
	if($action == 'addnew'){
	    $col_title = $this->session->flashdata('Title');
	    $col_des = $this->session->flashdata('ContentText');
	    $col_alias = $this->session->flashdata('Alias');
	    $col_thumbnail = $this->session->flashdata('Thumbnail');
	    $buttonname = "_do_add_new";
	    $savesite = base_url('artikel/add_data');
	    $langaction = "Add New";
	}else{
	    $col_title = $info_data->Title;
	    $col_des = $info_data->ContentText;
	    $col_alias = $info_data->Alias;
	    $col_thumbnail = $info_data->Thumbnail;
	    $buttonname = "_do_edit";
	    $savesite = site_url('artikel/edit_data/'.$info_data->ArticleId);
	    $langaction = "Edit";
	}
	?>
	<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h5 class="txt-dark">Articles</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
						<ul class="breadcrumb">
							<li><a href="<?=base_url();?>">Dashboard</a></li>
								<li><a href="<?=base_url("artikel");?>">Articles</a></li>
							<li class="active"><span><?=$langaction?></span></li>
						</ul>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<div class="row">
						<div class="col-sm-12">
							<form class="" role="form" method="POST" action="<?=$savesite?>">
								<div class="panel panel-primary">
										<div class="panel-heading">
												<div class="pull-left">
														<h5 class="panel-heading txt-light">Article Form</h5>
												</div>
												<div class="clearfix"></div>
										</div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label mb-10 text-left">Title</label>
														<input type="text" class="form-control" name="Title" value="<?=$col_title?>" required>
													</div>
													<div class="form-group">
														<label class="control-label mb-10 text-left">Content Text</label>
														<input type="text" class="form-control" name="ContentText" value="<?=$col_des?>" required>
													</div>
													<div class="form-group">
														<label class="control-label mb-10 text-left">Alias</label>
														<input type="text" class="form-control" name="Alias" value="<?=$col_alias?>">
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label mb-10 text-left">Thumbnail</label>
														<input type="text" class="form-control" name="Thumbnail" value="<?=$col_thumbnail?>" >
													</div>
												</div>
											</div>
												<div class="row">
													<div class="col-md-12">
														<button class="btn btn-primary" type="submit" name="<?=$buttonname?>">Save</button>

	                          							<a href="<?=base_url("artikel")?>" class="btn btn-default"><i class="fa fa-left"></i> Cancel</a>
													</div>
												</div>
										</div>
								</div>
							</form>

						</div>
				</div>
		</div>
	</div>
	<!-- footer -->
    <div class="footer">
        <div class="footer-top">
            <div class="container">
            	<p>2018 &copy; MetaPOS </p>
            </div>
        </div>
    </div>
</body>
</html>

<!-- <script>
function myFunction() {
    alert("The form was submitted");
}
</script> -->
