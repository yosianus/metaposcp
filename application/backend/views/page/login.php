<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<title>MetaPOS</title>

	

<?php
	echo $style;
	echo $script;
?>

<style>
	body {
   	 background-color: white;
}


</style>
</head>
<body>
	

	<div class="wrapper pa-0">
		<header class="sp-header">
			<div class="sp-logo-wrap pull-left">
				<a href="index.html">
					<img style="width: 180px; height: 50px" class="brand-img mr-10" src="<?php echo base_url();?>assets/image/metaposss.png" alt="MetaPos"/>
						<!-- <span class="brand-text">MetaPOS</span> -->
				</a>
			</div>
			<div class="clearfix"></div>
		</header>
		<!-- Main Content -->
		<div class="page-wrapper pa-0 ma-0 auth-page">
			<div class="container-fluid">
			<!-- Row -->
				<div class="table-struct full-width full-height">
					<div class="table-cell vertical-align-middle auth-form-wrap">
						<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10">Sign in to MetaPOS</h3>
										</div>
										<div class="form-wrap">
											<form action="<?php echo base_url();?>home" method="post">
												<!-- <div class="modal-body">
            										<label>Username</label>
            											<input type="username" name="username" class="form-control" placeholder="Enter you username" value="<?=$col_username?>" required>
            												<span><?php echo form_error('username', '<div class="alert-danger">', '</div>');?></span>
         										</div>
          										<div class="modal-body">
            										<label>Password</label>
            											<a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="forgot-password.html">forgot password?</a>
															<div class="clearfix"></div>
            													<input type="password" name="password" class="form-control" placeholder="Enter your password" value="<?=$col_password?>" required>
            														<span><?php echo form_error('password', '<div class="alert-danger">', '</div>');?></span>
          										</div> -->

          										<div class="modal-body">
            										<label>Email</label>
            											<input type="username" name="username" class="form-control" placeholder="Enter you username" required="">
            												<span><?php echo form_error('username', '<div class="alert-danger">', '</div>');?></span>
          										</div>
         										<div class="modal-body">
            										<label>Password</label>
            											<input type="password" name="password" class="form-control" placeholder="Masukkan Password" required="">
            												<span><?php echo form_error('password', '<div class="alert-danger">', '</div>');?></span>
          										</div>

													<div class="clearfix"></div>
												</div>
												<div class="form-group text-center">
													<button type="submit" name="btnSignIn" class="btn btn-primary">Sign In</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->
				</div>
			</div>
			<!-- /Main Content -->

		<?php
			echo $footer;
		?>
		</div>
		<!-- /#wrapper -->
	</body>
</html>
