<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>MetaPOS</title>


    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <?php
        echo $style;
        echo $script;
    ?>

<style>
    body {
    background-color: white;
    }

/* Add padding to containers */
.container {
    padding: 10px;
    background-color: white;
}

/* Full-width input fields */
input[type=text], input[type=password] {
    width: 100%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
    background-color: #ddd;
    outline: none;
}

/* Overwrite default styles of hr */
hr {
    border: 1px solid #f1f1f1;
    margin-bottom: 25px;
}

/* Set a style for the submit button */
.registerbtn {
    background-color: #C0C0C0;
    color: black;
    padding: 10px 15px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
}

.registerbtn:hover {
    opacity: 10;
}

/* Set a style for the submit button */
.cancelbtn {
    background-color: #C0C0C0;
    color: black;
    padding: 10px 15px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
}

.cancelbtn:hover {
    opacity: 10;
}

</style>
</head>

<body>

    <?php
        echo $header;
    ?>
                <!-- Main Content
                <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container-fluid">-->
          <!-- Row -->
          <div class="table-struct full-width full-height">
            <div class="table-cell vertical-align-middle auth-form-wrap">

                <div class="row">
                  <div class="col-sm-12 col-xs-12">

                    <div class="container">
                        <h2 align="center">New Account</h2>
                        <p>Please fill in this form to create a new account.</p>
                        <form role="form" action="<?php echo base_url();?>index.php/metaposController/register" method="post" enctype="multipart/form-data">
                        <hr>

                        <label for="fname">First Name</label>
                        <input type="text" placeholder="Enter your first name" name="fname" required="">
                        <span><?php echo form_error('fname', '<div class="alert-danger">', '</div>');?></span>

                        <label for="lname"><b>Last Name</b></label>
                        <input type="text" placeholder="Enter your last name" name="lname" required="">
                        <span><?php echo form_error('lname', '<div class="alert-danger">', '</div>');?></span>

                        <label for="username"><b>Username</b></label>
                        <input type="text" placeholder="Enter your username" name="username" required="">
                        <span><?php echo form_error('username', '<div class="alert-danger">', '</div>');?></span>

                        <label for="email"><b>Email</b></label>
                        <input type="text" placeholder="Enter your email" name="email" required="">
                        <span><?php echo form_error('email', '<div class="alert-danger">', '</div>');?></span>

                        <label for="psw"><b>Password</b></label>
                        <input type="password" placeholder="Enter your password" name="psw" required="">
                        <span><?php echo form_error('psw', '<div class="alert-danger">', '</div>');?></span>

                        <label for="psw-repeat"><b>Comfrim Password</b></label>
                        <input type="password" placeholder="Comfrim your password" name="psw-repeat" required="">
                        <span><?php echo form_error('psw-repeat', '<div class="alert-danger">', '</div>');?></span>

                        <!--<button type="button" class="btn btn-default" data-dismiss="modal" onclick="alert('Register success!')">Register</button>-->

                        <button type="button" class="btn btn-default" data-dismiss="modal" >Register</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>

                    </div>

  <!--<div class="container signin">
    <p>Already have an account? <a href="#">Sign in</a>.</p>
  </div>

                      <h2 align="left">Registation new user</h3>
                    </div>
                    <div class="form-wrap">
                      <form action="<?php echo base_url();?>index.php/metaposController/login" method="post">
                        <div class="modal-body">
                              <span>First Name :              </span>
                              <input type="text" name="name" class="Name" placeholder=" Enter Your First Name" required=""/>
                              <span><?php echo form_error('name', '<div class="alert-danger">', '</div>');?></span>
                        </div>
                        <div class="modal-body">
                            <span>Last Name :              </span>
                            <input type="text" name="name" class="Name" placeholder=" Enter Your Last Name" required=""/>
                            <span><?php echo form_error('name', '<div class="alert-danger">', '</div>');?></span>
                        </div>
                        <div class="modal-body">
                            <span>Username :                 </span>
                            <input type="text" name="username" class="Username" placeholder=" Enter Your Username" required="">
                            <span><?php echo form_error('username', '<div class="alert-danger">', '</div>');?></span>
                        </div>
                        <div class="modal-body">
                            <span>Email :            </span>
                            <input type="email" name="email" class="email" placeholder=" Enter Your Email" required=""/>
                            <span><?php echo form_error('email', '<div class="alert-danger">', '</div>');?></span>
                        </div>
                        <div class="modal-body">
                            <span>Password :            </span>
                            <input type="password" name="password" class="password" placeholder=" Enter Your Password" required=""/>
                            <span><?php echo form_error('password', '<div class="alert-danger">', '</div>');?></span>
                        </div>
                        <div class="modal-body">
                            <span>Confirm Password :            </span>
                            <input type="password" name="confirm_password" class="password" placeholder=" Confirm Your Password" required=""/>
                            <span><?php echo form_error('confirm_password', '<div class="alert-danger">', '</div>');?></span>
                        </div>

                        <div class="form-group text-center">
                          <button type="submit" name="btnSignIn" class="btn btn-primary">add</button>
                          <button type="submit" name="btnSignIn" class="btn btn-primary">cancel</button>
                        </div>
                      </form>
                    </div>-->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /Row -->
        </div>
      </div>

        <!-- /Main Content -->
<!-- footer -->
    <div class="footer">
        <div class="footer-top">
            <div class="container">
                        <p>2018 &copy; MetaPOS </p>
                </div>
            </div>
        </div>
    </div>
<!-- //footer -->
    </div>
    <!-- /#wrapper -->


</body>

</html>
