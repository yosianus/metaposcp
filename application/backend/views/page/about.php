<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>MetaPOS</title>
	<meta name="description" content="Jetson is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Jetson Admin, Jetsonadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<link rel="stylesheet" href="ckeditor/contents.css" type="text/css"/>

		<!-- panggil ckeditor.js -->
    	<script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>
    	<!-- panggil adapter jquery ckeditor -->
    	<script type="text/javascript" src="assets/ckeditor/adapters/jquery.js"></script>
    	<!-- setup selector -->
    	<script type="text/javascript">
       		$('textarea.texteditor').ckeditor();
    	</script>
	<?php
		echo $style;
		echo $script;
	?>
<style>
body {
    background-color: white;
}
</style>
</head>

<body>

	<?php
		echo $header;
	?>

	<!-- Main Content -->

			<div class="container">


				<<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-8 col-md-9 col-sm-9 col-xs-18">
						<br>
						<br>
						<br>
						<br>

					</div>

				</div>
				<!-- /Title -->

				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="pull-left">
								<h4 class="panel-heading txt-dark">About Us</h4s>
							</div>
							<div class="clearfix"></div>
						</div>
						<form>
        					<p><textarea name="about" class="ckeditor"></textarea></p>
        					<p><button type="submit">SIMPAN</button></p>
    					</form>
					</div>
				</div>
			</div>

	<?php
		echo $footer;
	?>
</body>

</html>
