<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <title>MetaPOS</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <?=$style.$script;?>

    <style>
      body {
            background-color: white;
            }
        table {
            border: 1px solid black;
            padding: 5px;
        }
        table {
            border-spacing: 15px;
        }
        .page-wrapper{
            background-color: #ffffff !important;
        }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"><!-- 
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script> -->
</head>

<body>
    <?=$header;?>
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Users</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=base_url();?>">Dashboard</a></li>
                        <li class="breadcrumb-item active"><span>Users</span></li>
                    </ul>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h5 class="panel-heading txt-light">Users Data</h5>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <!--button add sama delete -->
                                <body>
                                    <div class="w3-container" align="right">
                                        <a href="<?=base_url('users/add_data/')?>" class="btn btn-success" role="button">Add</a>
                                        <!-- button add -->

                                        <!-- button delete -->
                                        <button type="button" class="btn btn-danger" id="myBtn">Delete</button>
                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 align="center" class="modal-title">Remove Data</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p align="center">Are you sure?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <script>
                                                $(document).ready(function(){
                                                    $("#myBtn").click(function(){
                                                        $("#myModal").modal({show: true});
                                                    });
                                                });
                                            </script>
                                            <!-- button delete -->
                                    </div>
                                </body>
                                <!--button add sama delete -->
                                <table id="datable_1" class="table table-hover display  pb-30">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Username</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th style="width:3%;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if($data_list != false){
                                                foreach($data_list->result() as $row){
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="vehicle1" value="<?=$row->UserId?>">
                                            </td>
                                                <td><?=$row->UserName?></td>
                                                <td><?=$row->FirstName?></td>
                                                <td><?=$row->LastName?></td>
                                                <td>
                                                    <a href="<?=base_url('users/edit_data/'.$row->UserId)?>" class="btn btn-warning btn-md"><span class="glyphicon glyphicon-pencil"></span></a>
                                                </td>
                                        </tr>
                                        <?php
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer -->
    <div class="footer">
        <div class="footer-top">
            <div class="container">
                <p>2018 &copy; MetaPOS </p>
            </div>
        </div>
    </div>
</body>
</html>
