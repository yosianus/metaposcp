<div class="la-anim-1"></div>
<div class="wrapper theme-1-active pimary-color-orange">
  <!-- Top Menu Items -->
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="mobile-only-brand pull-left">
      <div class="nav-header pull-left">
        <div class="logo-wrap">
        <!--   <a href="<?php echo base_url("home");?>" title="MetaPos">
          <img class="brand-img" src="<?php echo base_url();?>assets/image/metaposss.png" alt="MetaPos"></a>
              <span class="brand-text">MetaPOS</span> -->
              
              <h1>
                <a class="navbar-brand" href="<?php echo base_url();?>index.php">
                <!-- <span>Edu</span> <i>Academy</i> -->
                <img src="<?php echo base_url();?>assets/images/metaposss.png" alt="" />
                </a>
              </h1>
          <!-- </a> -->
        </div>
      </div>
      <!--<a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>-->
        <a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
          <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>
            <form id="search_form" role="search" class="top-nav-search collapse pull-left">
              <div class="input-group">
                <input type="text" name="example-input1-group2" class="form-control" placeholder="Search">
                  <span class="input-group-btn">
                    <button type="button" class="btn  btn-default"  data-target="#search_form" data-toggle="collapse" aria-label="Close" aria-expanded="true"><i class="zmdi zmdi-search"></i></button>
                  </span>
              </div>
            </form>
      </div>
      <div id="mobile_only_nav" class="mobile-only-nav pull-right">
        <ul class="nav navbar-right top-nav pull-right">
          
          <li class="dropdown app-drp">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="zmdi zmdi-apps top-nav-icon"></i></a>
            <ul class="dropdown-menu app-dropdown" data-dropdown-in="slideInRight" data-dropdown-out="flipOutX">
              <li>
                <div class="app-nicescroll-bar">
                  <ul class="app-icon-wrap pa-10">
                    <li>
                      <a href="weather.html" class="connection-item">
                      <i class="zmdi zmdi-cloud-outline txt-info"></i>
                      <span class="block">weather</span>
                      </a>
                    </li>
                    <li>
                      <a href="inbox.html" class="connection-item">
                      <i class="zmdi zmdi-email-open txt-success"></i>
                      <span class="block">e-mail</span>
                      </a>
                    </li>
                    <li>
                      <a href="calendar.html" class="connection-item">
                      <i class="zmdi zmdi-calendar-check txt-primary"></i>
                      <span class="block">calendar</span>
                      </a>
                    </li>
                    <li>
                      <a href="vector-map.html" class="connection-item">
                      <i class="zmdi zmdi-map txt-danger"></i>
                      <span class="block">map</span>
                      </a>
                    </li>
                    <li>
                      <a href="chats.html" class="connection-item">
                      <i class="zmdi zmdi-comment-outline txt-warning"></i>
                      <span class="block">chat</span>
                      </a>
                    </li>
                    <li>
                      <a href="contact-card.html" class="connection-item">
                      <i class="zmdi zmdi-assignment-account"></i>
                      <span class="block">contact</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <li>
                <div class="app-box-bottom-wrap">
                  <hr class="light-grey-hr ma-0"/>
                  <a class="block text-center read-all" href="javascript:void(0)"> more </a>
                </div>
              </li>
            </ul>
          </li>
          <li class="dropdown full-width-drp">
            
            <ul class="dropdown-menu mega-menu pa-0" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
              <li class="product-nicescroll-bar row">
                <ul class="pa-20">
                  
                  <li class="col-md-3 col-xs-6 col-menu-list">
                    <a href="javascript:void(0);"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">User Management</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
                    <hr class="light-grey-hr ma-0"/>
                    <ul>
                      <a href="<?php echo base_url("users");?>">Users</a>
                    </ul>
                  </li>
                  <li class="col-md-3 col-xs-6 col-menu-list">
                    <a href="javascript:void(0);"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Article Management</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
                    <hr class="light-grey-hr ma-0"/>
                    <ul>
                      <li>
                        <a href="<?php echo site_url("Artikelkategori");?>">Article Categories</a>
                      </li>
                      <li>
                        <a href="<?php echo site_url("Artikel");?>">Articles</a>
                      </li>
                    </ul>
                  </li>
                  <li class="col-md-3 col-xs-6 col-menu-list">
                    <a href="javascript:void(0);"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Products</span></div><div class="pull-right"></i></div><div class="clearfix"></div></a>
                    <hr class="light-grey-hr ma-0"/>
                    <ul>
                      <li>
                        <!--<a href="#">Product Categories</a>-->
                        <a href="<?php echo site_url("Produkkategori");?>">Product Categories</a>
                      </li>
                      <li>
                        <a href="<?php echo site_url("Produk");?>">Products</a>
                      </li>
                    </ul>
                  </li>
                  <li class="col-md-3 col-xs-6 col-menu-list">
                    <a href="javascript:void(0);"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Members</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
                    <hr class="light-grey-hr ma-0"/>
                    <ul>
                      <li>
                        <a href="<?php echo site_url("testimonial");?>">Testimonial</a>
                      </li>
                    </ul>
                  </li>
                  <li class="col-md-3 col-xs-6 col-menu-list">
                    <a href="javascript:void(0);"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Subscribe</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
                    <hr class="light-grey-hr ma-0"/>
                    <ul>
                      
                    </ul>
                  </li>
                  <li class="col-md-3 col-xs-6 col-menu-list">
                    <a href="javascript:void(0);"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Settings</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
                    <hr class="light-grey-hr ma-0"/>
                    <ul>
                      <li>
                        <a href="<?php echo site_url("metaposController/about");?>">About Us</a>
                      </li>
                      <li>
                        <a href="<?php echo site_url("metaposController/pricing");?>">Pricing</a>
                      </li>
                      <li>
                        <a href="<?php echo site_url("metaposController/contact");?>">Contact Number</a>
                      </li>
                      <li>
                        <a href="<?php echo site_url("metaposController/contact");?>">Social Media</a>
                      </li>
                      <li>
                        <a href="<?php echo site_url("metaposController/contact");?>">Admin Email</a>
                      </li>
                    </ul>
                  </li>
              </li>
            </ul>
          </li>
        </ul>
        <li class="dropdown auth-drp">
            <a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown"><img src="assets/image/user1.png" alt="user_auth" class="user-auth-img img-circle"/><span class="user-online-status"></span></a>
            <ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
              <li>
                <a href="profile.html"><i class="zmdi zmdi-account"></i><span>Profile</span></a>
              </li>
              <li>
                <a href="#"><i class="zmdi zmdi-card"></i><span>my balance</span></a>
              </li>
              <li>
                <a href="inbox.html"><i class="zmdi zmdi-email"></i><span>Inbox</span></a>
              </li>
              <li>
                <a href="#"><i class="zmdi zmdi-settings"></i><span>Settings</span></a>
              </li>
              <li class="divider"></li>
              <li class="sub-menu show-on-hover">
                <a href="#" class="dropdown-toggle pr-0 level-2-drp"><i class="zmdi zmdi-check text-success"></i> available</a>
                <ul class="dropdown-menu open-left-side">
                  <li>
                    <a href="#"><i class="zmdi zmdi-check text-success"></i><span>available</span></a>
                  </li>
                  <li>
                    <a href="#"><i class="zmdi zmdi-circle-o text-warning"></i><span>busy</span></a>
                  </li>
                  <li>
                    <a href="#"><i class="zmdi zmdi-minus-circle-outline text-danger"></i><span>offline</span></a>
                  </li>
                </ul>
              </li>
              <li class="divider"></li>
              <li>
                <a href="#"><i class="zmdi zmdi-power"></i><span>Log Out</span></a>
              </li>
            </ul>
          </li>
      </div>
    </nav>
    <!-- /Top Menu Items -->

    <!-- Left Sidebar Menu -->
    <div class="fixed-sidebar-left">
      <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li class="navigation-header">
          <span>Main</span>
          <i class="zmdi zmdi-more"></i>
        </li>
        <li>
          <a class="active" href="<?php echo site_url("home");?>" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
          <ul id="dashboard_dr" class="collapse collapse-level-1">
          </ul>
        </li>
        <li>
          <a href="javascript:void(0);" data-toggle="collapse" data-target="#pages_dr"><div class="pull-left"><i class="zmdi zmdi-assignment-account mr-20"></i><span class="right-nav-text">User Management</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
          <ul id="pages_dr" class="collapse collapse-level-1 two-col-list">
            <li>
              <a href="<?php echo base_url("users");?>">Users</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="javascript:void(0);" data-toggle="collapse" data-target="#app_dr"><div class="pull-left"><i class="zmdi zmdi-card mr-20"></i><span class="right-nav-text">Article Management </span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
          <ul id="app_dr" class="collapse collapse-level-1">
            <li>
              <!--<a href="article_categories.php">Article Categories</a>
              -->
              <a href="<?php echo site_url("Artikelkategori");?>">Article Categories</a>
            </li>
            <li>
              <a href="<?php echo site_url("Artikel");?>">Articles</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr"><div class="pull-left"><i class="zmdi zmdi-menu mr-20"></i><span class="right-nav-text">Products</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
          <ul id="ui_dr" class="collapse collapse-level-1 two-col-list">
            <li>
              <!--<a href="#">Product Categories</a>-->
              <a href="<?php echo site_url("Produkkategori");?>">Product Categories</a>
            </li>
            <li>
              <a href="<?php echo site_url("produk");?>">Products</a>
            </li>
          </ul>
        </li>
       <li>
          <a href="javascript:void(0);" data-toggle="collapse" data-target="#form_dr"><div class="pull-left"><i class="zmdi zmdi-account mr-20"></i><span class="right-nav-text">Members</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
          <ul id="form_dr" class="collapse collapse-level-1 two-col-list">
            <li>
              <a href="<?php echo site_url("testimonial");?>">Testimonial</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="<?php echo site_url("subscribe");?>" data-toggle="collapse" data-target="#form_dr"><div class="pull-left"><i class="zmdi zmdi-accounts-add mr-20"></i><span class="right-nav-text">Subscibe</span></div><div class="clearfix"></div></a>
          <ul id="form_dr" class="collapse collapse-level-1 two-col-list">
            
          </ul>
        </li>
        <li>
          <a href="<?php echo site_url("contact");?>" data-toggle="collapse" data-target="#form_dr"><div class="pull-left"><i class="zmdi zmdi-accounts-add mr-20"></i><span class="right-nav-text">Contact Us</span></div><div class="clearfix"></div></a>
          <ul id="form_dr" class="collapse collapse-level-1 two-col-list">
            
          </ul>
        </li>
        <li>
          <a href="javascript:void(0);" data-toggle="collapse" data-target="#chart_dr"><div class="pull-left"><i class="zmdi zmdi-settings mr-20"></i><span class="right-nav-text">Setting </span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
          <ul id="chart_dr" class="collapse collapse-level-1 two-col-list">
            <li>
              <a href="<?php echo site_url("metaposController/about");?>">About Us</a>
            </li>
            <!-- <li>
              <a href="<?php echo site_url("metaposController/pricing");?>">Pricing</a>
            </li> -->
            <li>
              <a href="<?php echo site_url("metaposController/contact");?>">Contact Number</a>
            </li>
            <li>
              <a href="<?php echo site_url("metaposController/contact");?>">Social Media</a>
            </li>
            <li>
              <a href="<?php echo site_url("metaposController/contact");?>">Admin Email</a>
            </li>
          </ul>
        </li>
        <li><hr class="light-grey-hr mb-10"/></li>
      </ul>
    </div>
    <!-- /Left Sidebar Menu -->

    <!-- /Right Setting Menu -->

    <!-- Right Sidebar Backdrop -->
    <div class="right-sidebar-backdrop"></div>
    <!-- /Right Sidebar Backdrop -->

        <!-- Main Content-->

        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
